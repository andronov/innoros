# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        request = context['request']

        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='inline',
            draggable=False,
            deletable=False,
            collapsible=False,
            column=2,
            children=[
                [_(u'На главную'), '/'],
                [_(u'Переводы'), '/rosetta'],
                [_('Change password'),reverse('%s:password_change' % site_name)],
                [_('Log out'), reverse('%s:logout' % site_name)],
            ]
        ))
        self.children.append(modules.Group(
            title=_(u"Модуль управления"),
            column=1,
            children=[

                modules.ModelList(_(u"Общее"), ['django.contrib.*', 'accounts.models.*', 'constance.admin.*'], ['django.contrib.sites.*']),

                ]
        ))

        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=8,
            collapsible=False,
            column=3,
        ))


