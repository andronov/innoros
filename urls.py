# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static

admin.autodiscover()

# Устанавливаем свои вьюхи для ошибок 
handler403 = 'views.error403' # доступ запрещен
handler404 = 'views.error404' # страница не существует

urlpatterns = patterns('',
                       (r'^uploadify/', include('uploadify.urls')),
                       (r'^', include('project.urls')),
                       url('', include('registration.urls')),
                       # Calendar
                       url(r'^calendar/', include('django_bootstrap_calendar.urls')),
                       (r'^grappelli/', include('grappelli.urls')),
                       (r'^', include('accounts.urls')),
                       url(r'^admin/', include(admin.site.urls)),

                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.STATIC_ROOT,
                       }),
)

# if settings.DEBUG:
    # urlpatterns += patterns('',
    #                         url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
    #                             {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    # ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
                            url(r'^rosetta/', include('rosetta.urls')),
    )
