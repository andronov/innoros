from django.shortcuts import render_to_response

def error403(request):
    '''
    Default view for error 403: Inadequate permissions.
    '''
    return render_to_response('403.html', {'user': request.user})

def error404(request):
    '''
    Default view for error 404: Inadequate permissions.
    '''
    return render_to_response('404.html', {'user': request.user})

def error500(request):
    '''
    Default view for error 500: Inadequate permissions.
    '''
    return render_to_response('500.html', {'user': request.user})
