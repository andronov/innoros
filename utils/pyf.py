#!/usr/bin/env python
import sys
from pyfg import find_or_grep

#Examples:
#pyg.py 'search term1' 'term2 in same file' -d webdrive/python webdrive/templates
#pyg.py billing -n urls.py webdrive/python
#pyf url* bil* webdrive
if __name__ == "__main__":
    SysArgv = sys.argv + ["f"]
    find_or_grep(SysArgv)

