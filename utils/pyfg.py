#!/usr/bin/env python
import os, sys, codecs

def name_matches(Name, Templates):
    Res = 0
    for S in Templates:
        if S[0] == '*' and S[-1] == '*':
            Res = S[1:-1] in Name
        elif S[0] == '*':
            Res = Name.endswith(S[1:])
        elif S[-1] == '*':
            Res = Name.startswith(S[:-1])
        else:
            Res = (Name == S)
        if Res:
            break
    return Res

def os_walk_with_excluded_dirs(TopDir, ExcludedDirs, ExcludedTemplates):
    for Dir, SubDirs, Files in os.walk(TopDir):
        ResSubDirs = []
        for SubDir in SubDirs:
            Exclude = 0
            if os.path.realpath(os.path.join(Dir, SubDir)) in ExcludedDirs:
                Exclude = 1
            if not Exclude:
                if name_matches(SubDir, ExcludedTemplates):
                    Exclude = 1
            if Exclude:
                SubDirs.remove(SubDir)
            else:
                ResSubDirs.append(SubDir)
        yield Dir, ResSubDirs, Files

def find_in_file(FullName, Words, NameList, FindWholeWord):
    if not os.path.islink(FullName):
        try:
            Size = os.path.getsize(FullName)
            if Size == 0:
                return
        except:
            pass
        for Word in Words:
            Found = 0
            try:
                F = open(FullName, 'r')
            except:
                break
            #try:
            #    EnumeratedLines = enumerate(F)
            #except: #
            #    F.close()
            #    print("File is too big:", FullName)
            #    break
            for LineNum, Line in enumerate(F):
                Pos = Line.find(Word)
                if Pos >= 0:
                    if FindWholeWord:
                        IsWhole = 1
                        if Pos > 0 and (Line[Pos-1].isalnum() or Line[Pos-1] == '_'):
                            IsWhole = 0
                        PosAfter = Pos + len(Word)
                        if PosAfter < len(Line) - 1 and (Line[PosAfter].isalnum() or Line[PosAfter] == '_'):
                            IsWhole = 0
                        if IsWhole:
                            Found = 1
                    else:
                        Found = 1
                    if Found:
                        break
            F.close()
            if not Found:
                break
        if Found:
            NameList += [FullName]

def find_or_grep(SysArgv):
    Opts = ['-d', '-e', '-n', '-w']
    Found = 0
    Fn = SysArgv[-1]
    Args = SysArgv[1:-1]
    TemplateSearch = 0
    OptsInArgs = []
    IsDirOpt = 0
    TopDirs = []
    NameTemplates = []
    for i in range(len(Args)):
        if Args[i] in Opts:
            OptsInArgs.append({'pos': i, 'opt': Args[i]})
            if Args[i] == '-d':
                IsDirOpt = 1
    if not IsDirOpt:
        TopDirs.append(Args[-1])
        Args = Args[:-1]
    OptsInArgs.append({'pos': len(Args), 'opt': ''})
    SearchStrs = Args[ : OptsInArgs[0]['pos'] ]
    ExcludedDirs = []
    ExcludedTemplates = []
    FindWholeWord = 0
    for i in range(len(OptsInArgs) - 1):
        ArgsPart = Args[OptsInArgs[i]['pos'] + 1 : OptsInArgs[i+1]['pos']]
        Opt = OptsInArgs[i]['opt']
        if Opt == '-d':
            TopDirs.extend(ArgsPart)
        elif Opt == '-e':
            for Arg in ArgsPart:
                if "*" in Arg:
                    ExcludedTemplates.append(Arg)
                else:
                    ExcludedDirs.append(os.path.realpath(Arg))
        elif Opt == '-n':
            NameTemplates.extend(ArgsPart)
            TemplateSearch = 1
        elif Opt == '-w':
            SearchStrs = ArgsPart #
            FindWholeWord = 1
    for TopDir in TopDirs:
        if not os.path.isdir(TopDir):
            print TopDir + " is not a directory"
            return
        TopDir = os.path.normpath(TopDir)
        NameList = []
        Count = 0
        for Dir, Subdirs, Files in os_walk_with_excluded_dirs(TopDir, ExcludedDirs, ExcludedTemplates):
            Count += 1
            if Fn == "f":
                DirCount = 3000
            elif Fn == "g":
                DirCount = 300
            if Count % DirCount == 0:
                print("Current directory:", Dir)
            #if IsFilter:
            #    Files = fnmatch.filter(subfiles, '*.txt') ##
            ShortDirName = os.path.basename(os.path.normpath(Dir))
            if Fn == "f" and name_matches(ShortDirName, SearchStrs):
                NameList += [Dir]
            for File in Files:
                if not name_matches(File, ExcludedTemplates):
                    FullName = os.path.join(Dir, File)
                    try:
                        if Fn == "f" and name_matches(File, SearchStrs):
                            NameList += [FullName]
                        elif Fn == "g":
                            if TemplateSearch and not name_matches(File, NameTemplates):
                                pass
                            else:
                                find_in_file(FullName, SearchStrs, NameList, FindWholeWord)
                    except:
                        print("Problem with file:", FullName)
        print
        for Elt in NameList:
            print Elt

if __name__ == "__main__":
    find_or_grep() #

