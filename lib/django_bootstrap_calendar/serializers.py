# -*- coding: utf-8 -*-
import datetime
from django.core.serializers.json import DjangoJSONEncoder
from django_bootstrap_calendar.utils import datetime_to_timestamp

import time
from datetime import datetime

__author__ = 'sandlbn'

import json
from django.db.models.query import QuerySet


def event_serializer(events):
    """
    serialize event model
    """
    objects_body = []

    if isinstance(events, QuerySet):
        for event in events:
            a = (event.date_add).strftime("%d.%m.%Y")
            e = (event.date_end).strftime("%d.%m.%Y")
            stro = '2014-09-12'
            birthday = time.strftime('%Y-%m-%d', time.strptime(stro, '%Y-%m-%d'))
            test = {'vasya':1,'pupkin':2}
            field = {
                "id": event.id,
                "title": event.title,
                "author": str(event.author),
                "priority":event.get_priority_display(),
                "number": str(event.number),
                "progress": str(event.progress),
                "deleted": event.deleted,
                "date_add": a,
                "date_end": e,
                "birthday": birthday,
                "test": test,
                #"class": event.css_class,
                "start": datetime_to_timestamp(event.date_add),
                "end": datetime_to_timestamp(event.date_end),
            }
            objects_body.append(field)

    objects_head = {"success": 1}
    objects_head["result"] = objects_body
    #print(json.dumps(objects_head, cls=DjangoJSONEncoder, ensure_ascii=False))
    return json.dumps(objects_head, cls=DjangoJSONEncoder)

