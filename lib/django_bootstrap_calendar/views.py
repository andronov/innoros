# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.views.generic.simple import direct_to_template
from accounts.models import UserProfile

__author__ = 'sandlbn and w3lly'

from django.views.generic import ListView, TemplateView
from models import CalendarEvent
from serializers import event_serializer
from utils import timestamp_to_datetime

import datetime

from project.models import *


class CalendarJsonListView(ListView):

    template_name = 'django_bootstrap_calendar/calendar_events.html'

    def get_queryset(self):
        user_id = self.request.user.id
        queryset = Task.layer_objects.all()




        from_date = self.request.GET.get('from', False)
        to_date = self.request.GET.get('to', False)
        #print(timestamp_to_datetime(from_date))


        if from_date and to_date:
            queryset = queryset.filter(
                date_add__range=(
                    timestamp_to_datetime(from_date) + datetime.timedelta(-30),
                    timestamp_to_datetime(to_date)
                    )
            )
            #print(queryset)
        elif from_date:
            queryset = queryset.filter(
                date_end__gte=timestamp_to_datetime(from_date)
            )
            #print(queryset)
        elif to_date:
            queryset = queryset.filter(
                date_end__lte=timestamp_to_datetime(to_date)
            )
            #print(queryset)
        return event_serializer(queryset)


class CalendarView(TemplateView):

    template_name = 'django_bootstrap_calendar/calendar.html'

"""
@login_required
def calendar(request,  user_id = None, extra_context=None):
    template_name = 'django_bootstrap_calendar/calendar_events.html'
    if not user_id:
        user_id = request.user.id

    user = User.objects.get(pk = user_id)
    # пользователь, дашбоард которого смотрим
    request.viewed_user = user
    print(user)
    context = {
            'user': user,
        }
    #return event_serializer(user)
    return direct_to_template(request, template_name, context)
"""