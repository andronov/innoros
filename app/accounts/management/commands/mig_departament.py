from django.core.management.base import BaseCommand
from accounts.models import TreeDepartment, UserProfile
class Command(BaseCommand):
    help = "Migration deportament for tree"

    def handle(self, *args, **options):
        for u in UserProfile.objects.all():
            try:
                dep = u.department.title
                select = TreeDepartment.objects.get(title = dep)
                u.tree = select
                u.save()
                print u"%s:ok" % u.user.username
            except:
                print u"%s:fail" % u.user.username