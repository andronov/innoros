# -*- coding: utf-8 -*-
import os
import datetime
import calendar

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models import signals
from django.db.models import Q

import logging
from django.utils.encoding import smart_unicode
from django.db.models import Min
from mptt.models import MPTTModel, TreeForeignKey
from dateutil.rrule import DAILY, WDAYMASK, MONTHLY, rrule, WEEKLY
from datetime import date, timedelta as td
from accounts.signals import create_profile
from project.models import *
from utils import *
from helper.models import GetVerboseNamesMixin


def user_unicode(self):
    try:
        return unicode("%s %s" % (self.last_name or '', self.first_name or ''))
    except:
        return unicode(self.username)


User.__unicode__ = user_unicode
User._meta.ordering = ['last_name']


# Уровни управления системой профилей и подразделений
class TreeDepartment(MPTTModel):
    title = models.CharField(_(u'Название'), max_length=250)
    nums = models.CharField(_(u'Пункт'), max_length=250, blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    def __unicode__(self):
        return smart_unicode(self.title)

    def get_all_tree_users(self):
        tree = self.get_descendants(include_self=True)
        return UserProfile.objects.filter(tree__in=[i.id for i in tree]).select_related()

    def get_user_profiles(self):
        return UserProfile.objects.filter(tree=self.id).select_related()

    def get_task_count(self, month=False, year=False, and_where=[]):
        users = self.get_all_tree_users()
        num = 0
        if users:
            if month and year:
                num = Task.objects.filter(author_task__in=[u.user.id for u in users], layer__month=int(month),
                                          layer__year=int(year), *and_where).select_related().values_list(
                    'hash_task').distinct().count()
            else:
                num = Task.objects.filter(author_task__in=[u.user.id for u in users],
                                          *and_where).select_related().values_list('hash_task').distinct().count()
        return num

    def get_task_count_tree(self, month, year, and_where=[]):
        tree = self.get_descendants(include_self=True)
        num = []
        for t in tree:
            for user in UserProfile.objects.filter(tree=t.id):
                if month and year:
                    num.append(
                        Task.objects.filter(author_task=user.user.id, layer__month=int(month), layer__year=int(year),
                                            *and_where).select_related().values_list('hash_task').distinct().count())
                else:
                    num.append(Task.objects.filter(author_task=user.user.id, *and_where).values_list(
                        'hash_task').select_related().distinct().count())
        return num or [0]

    def get_all_task_tree(self, month=None, year=None, and_where=[]):
        tree = self.get_root()
        users = UserProfile.objects.filter(
            tree__in=[i.id for i in tree.get_descendants(include_self=True)]).select_related()
        if users:
            if month and year:
                num = Task.objects.filter(author_task__in=[u.user.id for u in users], layer__month=int(month),
                                          layer__year=int(year), *and_where).select_related().values_list(
                    'hash_task').distinct().count()
            else:
                num = Task.objects.filter(author_task__in=[u.user.id for u in users], *and_where).values_list(
                    'hash_task').select_related().distinct().count()
        return num or 0

    def get_change_task(self, month, year, and_where=[], attrs='date_add'):
        if month and year:
            and_where.extend([('layer__month', int(month)), ('layer__year', int(year))])
        tasks = Task.objects.filter(author_task__in=[u.user.id for u in self.get_all_tree_users()],
                                   layer__month=int(month), layer__year=int(year)).select_related().filter(*and_where)
        num = Task.objects.filter(author_task__in=[u.user.id for u in self.get_all_tree_users()],
                                  *and_where).select_related().values_list('hash_task').distinct().count()
        new_task = {}
        for task in tasks:
            new_task['%s-%s-%s' % (task.__getattribute__(attrs), task.title, task.hash_task)] = task.hash_task
        return abs(num - len(new_task))

    def get_today_avg_task(self, and_where=[], attrs='date_add'):
        task = Task.objects.filter(author_task__in=[u.user.id for u in self.get_all_tree_users()],
                                   layer=datetime.datetime.now().date()).select_related().filter(*and_where)
        new_task = []
        for t in task:
            try:
                new_task.append(int(t.__getattribute__(attrs)))
            except:
                new_task.append(0)
        return sum(new_task) / len(task)

    def get_graph_month_for_user(self):
        min_date = Task.objects.all().order_by('layer')[0].layer
        now_date = datetime.datetime.now().date()
        delta = rrule(MONTHLY, dtstart=min_date, until=now_date)
        get_list = list(delta)
        a = 0
        ar = []
        while a < len(get_list):
            ar.append(0)
            a += 1
        i = 0
        for is2 in get_list:
            if i < len(get_list):
                ar[i] = self.get_task_count(is2.month, is2.year)
                i += 1
        get_date = []
        for dt in get_list:
            get_date.append(u'%s.%s' % (dt.month, dt.year))
        return get_date, ar

    class Meta:
        verbose_name = _(u'уровни управления')
        verbose_name_plural = _(u'уровни управления')

    class MPTTMeta:
        order_insertion_by = ['nums']


class UserProfileManager(models.Manager):
    def get_query_set(self):
        return super(UserProfileManager, self).get_query_set().filter(dismissed=False, tree__isnull=False)


class UserProfile(models.Model, GetVerboseNamesMixin):
    class Meta():
        verbose_name = _(u'профиль')
        verbose_name_plural = _(u'профили')
        ordering = ['-role', 'user__last_name']
        permissions = (
            ('view_query_9', 'View Query 9'),
            ('view_query_0', 'View Query 0'),
            ('view_query_1', 'View Query 1'),
            ('view_query_2', 'View Query 2'),
            ('view_query_3', 'View Query 3'),
            ('view_query_4', 'View Query 4'),
            ('view_query_5', 'View Query 5'),
            ('view_query_6', 'View Query 6'),
            ('view_query_7', 'View Query 7'),
            ('view_query_8', 'View Query 8'),
            ('view_query_14', 'View Query 14'),
            ('view_graph_1', 'View Graph 1'),
            ('view_user_tasks_in_profile', 'Can view user tasks in profile'),
            ('change_task_performer', 'Can change task performer'),
        )

    user          = models.ForeignKey(User, unique=True, verbose_name=_(u'Пользователь'))
    middle_name   = models.CharField(_(u'Отчество'), max_length=200, blank=True, null=True)
    birthday      = models.DateField(_(u'Дата рождения'), blank=True, null=True)
    private_email = models.EmailField(_(u'Личный email'), blank=True, null=True)
    work_phone    = models.CharField(_(u'Рабочий телефон'), max_length=30, blank=True, null=True)
    mobile_phone  = models.CharField(_(u'Мобильный телефон'), max_length=30, blank=True, null=True)
    skype         = models.CharField(_(u'Скайп'), max_length=100, blank=True, null=True)
    location      = models.CharField(_(u'Город'), max_length=140, blank=True, null=True)
    role          = models.BooleanField(_(u'Руководитель уровня'), default=0)
    role2         = models.BooleanField(_(u'Зам.руководителя уровня'), default=0)
    tree          = models.ForeignKey(TreeDepartment, blank=True, null=True, verbose_name=_(u'Уровень'))
    tree_views    = models.ManyToManyField(TreeDepartment, related_name='tree_views', blank=True, null=True,
                                        verbose_name=_(u'Доступ на просмотр уровней (Исключительный параметр)'))
    job             = models.CharField(_(u'Должность'), max_length=140, blank=True, null=True)
    profile_picture = models.ImageField(_(u'Фотография'), upload_to='icon', blank=True, null=True)
    dismissed       = models.BooleanField(_(u'Уволен'), default=0)
    feedback        = models.TextField(_(u'Рациональное предложение'), blank=True, null=True)
    kadr            = models.TextField(_(u"Мнение отдела кадров"), blank=True, null=True)
    send_email      = models.BooleanField(_(u'Присылать оповещения'), default=0)
    experience      = models.TextField(u"Навыки", blank=True, null=True)
    competence      = models.TextField(u"Компетенции", blank=True, null=True)
    boss_review     = models.TextField(u"Мнение руководителя", blank=True, null=True)

    objects       = models.Manager()
    staff_objects = UserProfileManager()

    def __unicode__(self):
        return u'%s' % self.get_names()


    def get_names(self):
        try:
            return u"%s %s %s" % (self.user.last_name or '', self.user.first_name or '', self.middle_name or '')
        except:
            return self.user.username

    def get_user_profiles(self):
        try:
            return UserProfile.objects.filter(
                tree__in=[i.id for i in self.tree.get_descendants(include_self=True)]).select_related()
        except:
            return UserProfile.objects.filter(id=self.id).select_related()

    def get_task_count(self, month=None, year=None, and_where=[]):
        if month and year:
            return Task.objects.filter(author_task=self.user, layer__month=int(month), layer__year=int(year),
                                       *and_where).select_related().values_list('hash_task').distinct().count()
        else:
            return Task.objects.filter(author_task=self.user, *and_where).values_list(
                'hash_task').select_related().distinct().count()

    def get_task_count_avg(self, month=None, year=None, and_where=[]):
        if month and year:
            tree = self.tree.get_task_count(month, year)
            task = self.get_task_count(month, year)
        else:
            task = self.get_task_count(and_where=and_where)
            tree = self.tree.get_task_count(and_where=and_where)
        if is_sep():
            logging.debug('tree count {0}'.format(tree))
            logging.debug('task avg pro {0}'.format(task))
        if task:
            return float(task) / tree * 100
        else:
            return 0

    def get_change_task(self, month, year, and_where=[], attrs='date_add'):
        if month and year:
            and_where.extend([('layer__month', int(month)), ('layer__year', int(year))])
        task = Task.objects.filter(author_task=self.user, layer__month=int(month),
                                   layer__year=int(year)).select_related().filter(*and_where)
        num = self.get_task_count(month, year, and_where)
        new_task = {}
        for t in task:
            new_task['%s-%s-%s' % (t.__getattribute__(attrs), t.title, t.hash_task)] = t.hash_task
        return abs(num - len(new_task))

    def get_graph_task_for_user(self, year, and_where=[]):
        _list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        ar = []
        for is2 in _list:
            ar += str(Task.objects.filter(layer__month=is2, layer__year=int(year), author_task=self.user,
                                          *and_where).values_list('hash_task').distinct().count())
        return ar

    def get_graph_AVG_task_for_user(self, and_where=[]):
        hashes = Task.objects.filter(author_task=self.user, *and_where).values_list(
            'hash_task').select_related().distinct()
        new_task = []
        for get in hashes:
            tasks = Task.objects.filter(hash_task__in=get, author_task=self.user).select_related()
            progress_array = []
            for task in tasks:
                progress_array.append(task.progress or 0)
            new_task.append(progress_array)
        lens_array = []
        for lens in new_task:
            lens_array.append(len(lens))
        if not lens_array:
            return 0, [0]
        max_lens = max(lens_array)
        output_array = list(range(1, max_lens + 1))
        param_array = []
        output2 = {}
        output3 = {}
        i = 0
        for params in new_task:
            max_ry = len(params)
            i2 = 1
            for line in output_array:
                try:
                    try:
                        output2[i2] += int(params[int(line)])
                    except KeyError:
                        output2[i2] = int(params[int(line)])
                except IndexError:
                    try:
                        output2[i2] += 100
                    except KeyError:
                        output2[i2] = 100

                i2 += 1
            i += 1

        i3 = 1
        for key, param in output2.items():
            try:
                output3[i3] += int(param)
            except:
                output3[i3] = int(param)
            i3 += 1

        for key, out in output3.items():
            param_array.append(out / i)
        return output_array, param_array

    def get_today_avg_task(self, and_where=[], attrs='date_add'):
        task = Task.objects.filter(author_task=self.user, layer=datetime.datetime.now().date()).select_related().filter(
            *and_where)
        new_task = []
        for t in task:
            try:
                new_task.append(int(t.__getattribute__(attrs)))
            except:
                new_task.append(0)
        return sum(new_task) / len(task)

    def get_open_task_on_everyday(self):
        min_date = Task.objects.aggregate(Min('layer')).values()[0]
        current_date = datetime.datetime.now().date()
        task_dict = {}
        while min_date <= current_date:
            tuple_date = current_date.timetuple()
            timestamp_utc = calendar.timegm(tuple_date)
            task_dict[timestamp_utc * 1000] = Task.objects.filter(author_task=self.user, layer=current_date).exclude(
                progress=100).values_list('hash_task').distinct().count()
            current_date -= datetime.timedelta(days=1)
        return task_dict

    def get_timeliness_and_task_count(self, month=None, year=None, and_where=[]):
        if month and year:
            items = Task.objects.filter(author_task=self.user, layer__month=int(month), layer__year=int(year), *and_where).order_by('number', 'layer')
        else:
            items = Task.objects.filter(author_task=self.user, *and_where).order_by('number', 'layer')
        number = -1
        timeliness_list = []
        progress = 0
        temp = 0
        for i in range(len(items)):
            item = items[i]
            date_end = item.date_end
            prev_progress = progress
            progress = item.progress
            if progress is None:
                progress = 0
            if item.number != number or i == len(items) - 1: # next task
                temp += 1
                if number != -1: # not first task
                    planned_duration_list = []
                    summa = 0
                    count = len(duration_list)
                    if count > 0:
                        partial_duration = (last_date - date_add).days
                        for j in range(count):
                            planned_duration_list.append(partial_duration * progress_list[j] / 100)
                            summa += abs(planned_duration_list[j] - duration_list[j])
                        timeliness_list.append(summa/count)
                progress_list = []
                duration_list = []
                number = item.number
                date_add = item.date_add
            if progress != prev_progress:
                last_date = item.layer
                for j in range(int(prev_progress/10), int(progress/10)):
                    duration_list.append( (item.layer - item.date_add).days )
                    progress_list.append((j+1)*10)
        if len(timeliness_list) > 0:
            timeliness = sum(timeliness_list) / float(len(timeliness_list))
        else:
            timeliness = -1 ##
        return timeliness, len(timeliness_list)

    @classmethod
    def get_all_bosses(cls, user):
        '''
        Возвращает список всех начальников выше по иеррархии
        '''
        user_profile    = cls.objects.get(user__id=user.id)
        user_tree_level = user_profile.tree
        # ancestors       = user_profile.tree.get_ancestors(ascending=False, include_self=False);

        # проверяем является ли пользователь начальником своего отдела или зам. начальника
        is_departament_boss        = user_profile.role
        is_departament_deputy_boss = user_profile.role2

        all_bosses = []
        # for node in ancestors:
        #     all_bosses += list(cls.objects.filter(tree = node, role = True))
        all_bosses += list(cls.objects.filter(
                tree_views = user_tree_level
            ).exclude(
                tree = user_tree_level,
                role = True,
            ).exclude(
                tree  = user_tree_level,
                role2 = True
            ).exclude(pk = user_profile.pk))

        # нам нужны лишь сотрудники отделов выше по иеррархии
        # если пользователь не начальник отдела, то нам еще нужен начальник данного отдела
        if not is_departament_boss:
            all_bosses += list(cls.objects.filter(tree = user_profile.tree, role = True))
            # если пользователь не зам.начальника отдела, то в массив боссов нужно добавить и зам.начальника
            if not is_departament_deputy_boss:
                all_bosses += list(cls.objects.filter(tree = user_profile.tree, role2 = True))

        return all_bosses

    def is_boss_of_user(self, user):
        '''
        Является ли пользователь профиля self начальником указанного пользователя.
        '''
        all_bosses = UserProfile.get_all_bosses(user)
        all_bosses_profile_ids = [obj.pk for obj in all_bosses]
        if self.id in all_bosses_profile_ids:
            return True
        else:
            return False


    def has_access_to_user_tasks(self, dest_user_id):
        '''
        Проверяет, можно ли данному пользователю смотреть поручения указанного пользователя.

        dest_user_id - идентификатор пользователя, к поручениям которого проверяется доступ
        '''
        # Все начальники данного пользователя
        all_bosses = UserProfile.get_all_bosses(User.objects.get(id = self.user.id))
        all_bosses_profile_ids = [obj.pk for obj in all_bosses]

        dest_profile = UserProfile.objects.get(user__id = dest_user_id)
        if dest_profile.id in all_bosses_profile_ids:
            return True
        return False

    def get_employers(self):
        employers = []
        if(self.user.is_superuser):
            employers = UserProfile.objects.all()
            return employers

        # проверяем является ли пользователь начальником своего отдела или зам. начальника
        is_departament_boss        = self.role
        is_departament_deputy_boss = self.role2

        tree = TreeDepartment.objects.get(id = self.tree.id)
        # descendants = [i.id for i in tree.get_descendants(include_self = False)]
        # получаем все подчиненные отделы (отдел, в котором числится данный сотрудник не включаем пока)
        descendants = self.tree_views.exclude(id = self.tree.id)

        employers += list(UserProfile.objects.filter(tree__in = descendants))

        # если пользователь начальник отдела, то нам еще нужен зам.начальник данного отдела
        if is_departament_deputy_boss or is_departament_boss:
            employers += list(UserProfile.objects.filter(tree = self.tree, role = False, role2 = False))
        if is_departament_boss:
            employers += list(UserProfile.objects.filter(tree = self.tree, role2 = True))

        # assert False

        return set(employers)

        # try:
        #     if(self.user.is_superuser):
        #         employers = UserProfile.objects.all()
        #     else:
        #         tree = TreeDepartment.objects.get(id=self.tree.id)
        #         employers = UserProfile.objects.filter(
        #             Q(tree__in=[i.id for i in tree.get_descendants(include_self=True)]) |
        #             Q(tree__in=[i.id for i in self.tree_views.all()]),
        #             user__is_active=True, dismissed=False).select_related().order_by('user__last_name')
        # except:
        #     employers = []
        # return employers

    def can_view_review_fields(self, request_user):
        '''
        Можно ли просматривать поля "мнение отдела кадров" и "мнение руководителя" указанному пользователю.
        '''
        # профиль авторизованного пользователя
        request_user_profile = UserProfile.objects.get(user__pk = request_user.pk)
        if request_user_profile.is_boss_of_user(self.user) or self.user.id == request_user.id:
            return True
        return False
    
    def get_last_attended_date(self):
        """Возвращает дату последнего посещения СКАИП"""
        last_attended = UsersAttendDays.objects.filter(user=self.user).order_by('-date')[:1]
        if last_attended.count():
            return last_attended[0].date
        else:
            return None
    def get_opened_tasks(self, date_start, date_end):
        return Task.objects.filter(
            author_task  = self.user,
            # layer__range = (date_start, date_end),
            layer = datetime.datetime.now(),
            deleted      = False,
        ).exclude(progress = 100).distinct()

    def get_closed_tasks(self, date_start, date_end):
         return Task.objects.filter(
             progress     = 100,
             author_task  = self.user,
             layer__range = (date_start, date_end),
        ).distinct()

    def get_deleted_tasks(self, date_start, date_end):
        return Task.objects.filter(
            author_task  = self.user,
            deleted      = True,
            layer__range = (date_start, date_end),
        ).distinct()


class UsersOnline(models.Model):
    user = models.ForeignKey(User)
    time = models.DateTimeField()
    time_in_history = models.DateTimeField()

    def __unicode__(self):
        return self.user


class AttentionOnline(models.Model):
    user = models.ForeignKey(User)
    time = models.DateTimeField()
    time_in_history = models.IntegerField()

    def __unicode__(self):
        return self.user


signals.post_save.connect(create_profile, sender=User)


class UsersAttendDays(models.Model):
    """Вспомогательная модель, сохраняющая, в какие дни пользователи заходили в систему.
    Используется для упрощения формирования отчета "Список малоактивных сотрудников"
    Новые записи создаются по сигналу post_save на модели UsersOnline."""
    user = models.ForeignKey(User)
    date = models.DateField()

    class Meta:
        unique_together = ('user', 'date')


def create_user_attend_record(sender, instance, created, **kwargs):
    date = instance.time.date()
    user = instance.user
    UsersAttendDays.objects.get_or_create(user=user, date=date)

signals.post_save.connect(create_user_attend_record, sender=UsersOnline)