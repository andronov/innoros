


def create_profile(sender, instance, signal, created, **kwargs):
    from accounts.models import UserProfile
    if created:
        UserProfile(user = instance).save()
