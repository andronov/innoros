from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^ajax_edit/$', 'accounts.views.ajax_edit', name="ajax_edit"),

    url(r'^edit/$', 'accounts.views.edit_account', name="edit_account"),

    url(r'^profile-with-tasks/(?P<username>\w+)/$', 'accounts.views.view_account_with_tasks', name="view_account_with_tasks"),
    url(r'^profile/(?P<username>\w+)/$', 'accounts.views.view_account', name="view_account"),
    url(r'^users/(?P<username>\w+)/$', 'accounts.views.view_account', name="view_account_user"),
    url(r'^list/$', 'accounts.views.accounts_list', name="accounts_list"),

    url(r'^employers/$', 'accounts.views.edit_employers', name="edit_employers"),
)

