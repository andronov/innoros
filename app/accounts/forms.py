#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from accounts.models import UserProfile
from models import *
from django.utils.translation import ugettext_lazy as _
from mptt.forms import TreeNodeChoiceField
from utils import *
from helper.forms import PeriodFilters

class UserFullName(User):
    class Meta:
        proxy = True

    def __unicode__(self):
        try:
          return self.get_profile().get_names()
        except :
          return self.username

class UserProfileForm(forms.ModelForm):
    location =  forms.CharField(label=_(u'Город'), required=False)
    class Meta():
        exclude = ['user', 'role', 'role2', 'dismissed', 'tree_views']
        model = UserProfile

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['middle_name'].required = True
        self.fields['job'].required = True
        self.fields['tree'].required = True

class UserForm(forms.ModelForm):
    class Meta():
        model = User
        fields = [ "email", "first_name", "last_name"]

    def __init__(self, *args, **kw):
        super(forms.ModelForm, self).__init__(*args, **kw)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        new_order = self.fields.keyOrder[:-3]
        new_order.insert(0, 'email')
        new_order.insert(1, 'last_name')
        new_order.insert(2, 'first_name')
        self.fields.keyOrder = new_order

class ProfileTasksFiltersForm(PeriodFilters):
    pass
