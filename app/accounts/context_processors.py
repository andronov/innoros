# -*- coding: utf-8 -*-
import re
import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from accounts.models import *
from dateutil.relativedelta import *
from django.db.models import Q
import itertools
import operator
from django.views.generic.simple import direct_to_template


def get_access_user(request):
    if request.user.is_authenticated():
        try:
            tree = TreeDepartment.objects.get(id=request.user.get_profile().tree.id)
            return UserProfile.objects.filter(
                Q(tree__in=[i.id for i in tree.get_descendants(include_self=True)]) |
                Q(tree__in=[i.id for i in request.user.get_profile().tree_views.all()]),
                user__is_active=True, dismissed=False).select_related().order_by('user__last_name')
        except:
            return {}
    return {}


def get_employers(request):
    if request.user.is_authenticated():
        access_m = True
        get_profile = UserProfile.objects.get(user=request.user)
        try:
            employers = get_profile.get_employers()
            # tree = TreeDepartment.objects.get(id=get_profile.tree.id)
            # employers = UserProfile.objects.filter(
            #     Q(tree__in=[i.id for i in tree.get_descendants(include_self=True)]) |
            #     Q(tree__in=[i.id for i in request.user.get_profile().tree_views.all()]),
            #     user__is_active=True, dismissed=False).select_related().order_by('user__last_name')
        except:
            employers = []
        return {'ACC_EMPLOYERS': employers, 'ACC_MANAGER': access_m}
    return {}


def getAccess(request):
    # пришлось вместое GET-параметра get_user помещать id пользователя непосредственно в url,
    # поэтому получаем user_id из url
    user_id = None

    pattern = re.compile('^/user-dashboard/(\d+)/')
    matches = pattern.search(request.path)
    try:
        user_id = int(matches.groups()[0])
    except:
        pass

    if not user_id:
        pattern = re.compile('^/history/(\d+)/')
        matches = pattern.search(request.path)
        try:
            user_id = int(matches.groups()[0])
        except:
            pass


    if user_id:
        p = UserProfile.objects.select_related().get(user = user_id)
        get_access = get_access_user(request)
        if not request.user.is_superuser:
            if not p in get_access:
                raise PermissionDenied
        return {
            'USER_TASKS': p,
            # 'URI_USER': '?get_user=%s' % user_id
        }
    return {}

def getOnline(request):
    if request.path.startswith("/history") or request.path.startswith("/?get_user="):
        time_in_history = datetime.datetime.now()
    else:
        time_in_history = datetime.date(0001, 01, 01)
    try:
        UsersOnline.objects.get_or_create(
            user=User.objects.get(id=request.user.id),
            time__range=((datetime.datetime.now() + relativedelta(minutes=-1)), (datetime.datetime.now() + relativedelta(minutes=+1))),
            time_in_history=time_in_history,
            defaults={'time': datetime.datetime.now()}
        )
    except:
          pass
    return {}

