from django.template.defaultfilters import stringfilter
from django import template
from accounts.models import UserProfile


register = template.Library()

@stringfilter
@register.simple_tag
def int_to_string(value):
    return value

@register.simple_tag
def get_full_fio(user):
    profile = UserProfile.objects.filter(user__pk = user.pk)[0]
    return profile.get_names()
