# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Leaders'
        db.delete_table('accounts_leaders')

        # Removing M2M table for field dep on 'Leaders'
        db.delete_table('accounts_leaders_dep')

        # Removing M2M table for field manager on 'Leaders'
        db.delete_table('accounts_leaders_manager')

        # Deleting model 'Department'
        db.delete_table('accounts_department')

        # Removing M2M table for field manager on 'Department'
        db.delete_table('accounts_department_manager')

        # Deleting model 'Operator'
        db.delete_table('accounts_operator')

        # Removing M2M table for field dep on 'Operator'
        db.delete_table('accounts_operator_dep')

        # Removing M2M table for field manager on 'Operator'
        db.delete_table('accounts_operator_manager')

        # Deleting field 'UserProfile.department'
        db.delete_column('accounts_userprofile', 'department_id')


    def backwards(self, orm):
        # Adding model 'Leaders'
        db.create_table('accounts_leaders', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=140)),
        ))
        db.send_create_signal('accounts', ['Leaders'])

        # Adding M2M table for field dep on 'Leaders'
        db.create_table('accounts_leaders_dep', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('leaders', models.ForeignKey(orm['accounts.leaders'], null=False)),
            ('department', models.ForeignKey(orm['accounts.department'], null=False))
        ))
        db.create_unique('accounts_leaders_dep', ['leaders_id', 'department_id'])

        # Adding M2M table for field manager on 'Leaders'
        db.create_table('accounts_leaders_manager', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('leaders', models.ForeignKey(orm['accounts.leaders'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('accounts_leaders_manager', ['leaders_id', 'user_id'])

        # Adding model 'Department'
        db.create_table('accounts_department', (
            ('dep', self.gf('django.db.models.fields.related.ForeignKey')(related_name='dep', null=True, to=orm['accounts.UserProfile'], blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=140)),
        ))
        db.send_create_signal('accounts', ['Department'])

        # Adding M2M table for field manager on 'Department'
        db.create_table('accounts_department_manager', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('department', models.ForeignKey(orm['accounts.department'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('accounts_department_manager', ['department_id', 'user_id'])

        # Adding model 'Operator'
        db.create_table('accounts_operator', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=140)),
        ))
        db.send_create_signal('accounts', ['Operator'])

        # Adding M2M table for field dep on 'Operator'
        db.create_table('accounts_operator_dep', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('operator', models.ForeignKey(orm['accounts.operator'], null=False)),
            ('department', models.ForeignKey(orm['accounts.department'], null=False))
        ))
        db.create_unique('accounts_operator_dep', ['operator_id', 'department_id'])

        # Adding M2M table for field manager on 'Operator'
        db.create_table('accounts_operator_manager', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('operator', models.ForeignKey(orm['accounts.operator'], null=False)),
            ('user', models.ForeignKey(orm['auth.user'], null=False))
        ))
        db.create_unique('accounts_operator_manager', ['operator_id', 'user_id'])

        # Adding field 'UserProfile.department'
        db.add_column('accounts_userprofile', 'department',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.Department'], null=True, blank=True),
                      keep_default=False)


    models = {
        'accounts.invitation': {
            'Meta': {'object_name': 'Invitation'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'accounts.treedepartment': {
            'Meta': {'object_name': 'TreeDepartment'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'nums': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['accounts.TreeDepartment']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'accounts.userprofile': {
            'Meta': {'ordering': "['-role', 'user__last_name']", 'object_name': 'UserProfile'},
            'dismissed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'feedback': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job': ('django.db.models.fields.CharField', [], {'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'kadr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'profile_picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tree': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounts.TreeDepartment']", 'null': 'True', 'blank': 'True'}),
            'tree_views': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'tree_views'", 'null': 'True', 'to': "orm['accounts.TreeDepartment']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'accounts.usersonline': {
            'Meta': {'object_name': 'UsersOnline'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['accounts']