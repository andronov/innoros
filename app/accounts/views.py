# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.views.generic.simple import direct_to_template
from django.contrib.auth.decorators import login_required
from django.db.models import Max
from accounts.models import *
from accounts.forms import *
from django.conf import settings
from constance import config
from registration.backends import get_backend
from registration.forms import RegistrationForm
from django.http import HttpResponse
from utils import *
from django.contrib import messages


@login_required
def edit_account(request):
    UserSet = UserForm
    UserProfileSet = UserProfileForm
    Profile = UserProfile.objects.get(user = request.user.id)
    if request.method == 'POST':
        uform = UserSet(request.POST, request.FILES, prefix="user", instance=request.user)
        if uform.is_valid():
            uform.save()
            pform = UserProfileSet(request.POST, request.FILES, prefix="profile", instance=Profile)
            if pform.is_valid():
                pform.save()
            else:
                messages.add_message(request, messages.INFO, pform.errors)
        else:
            messages.add_message(request, messages.INFO, uform.errors)
    return direct_to_template(request, 'accounts/edit_account.html', {
           'uform': UserSet(instance=request.user, prefix="user"),
           'pform': UserProfileSet(instance=Profile, prefix="profile"),
           'is_test': is_test,
        })


@login_required
def accounts_list(request):
    node = TreeDepartment.objects.all()
    return direct_to_template(request, 'accounts/accounts_list.html', {
        'node': node,
        'is_test': is_test(),
    })

@login_required
def view_account(request, username):
    user = User.objects.get(username = username)
    request_user_profile = UserProfile.objects.get(user__id=request.user.id)
    # пользователь, дашбоард которого смотрим
    request.viewed_user = user
    profile = UserProfile.objects.get(user = user.id)
    can_view_review_fields = profile.can_view_review_fields(request.user)
    return direct_to_template(request, 'accounts/view_account.html', {
        'profile': profile,
        'request_user': request_user_profile,
        'is_test': is_test(),
        'can_view_review_fields': can_view_review_fields,
    })


@login_required
def view_account_with_tasks(request, username):
    user = User.objects.get(username = username)
    request_user_profile = UserProfile.objects.get(user__id=request.user.id)
    # пользователь, дашбоард которого смотрим
    request.viewed_user = user
    viewed_profile = UserProfile.objects.get(user__id=user.id)

    # tasks = Task.objects.filter(author_task=user).order_by('date_add')

    # получаем всех начальников выше по иеррархии
    all_bosses = UserProfile.get_all_bosses(user)

    all_bosses_profile_ids = [obj.pk for obj in all_bosses]

    # Фильтры
    date_start = datetime.datetime(1900, 1, 1)
    date_end   = datetime.datetime(2900, 1, 1)
    filters_form = ProfileTasksFiltersForm(initial = {
        'period': 'all_time',
    })
    if request.GET.get('period', None):
        filters_form = ProfileTasksFiltersForm(request.GET)
        if filters_form.is_valid():
            date_start = filters_form.start
            date_end   = filters_form.end

    # независимо от уровня иеррархии пользователя, который смотрит профиль, 
    # показываем те задачи, в которых он является постановщиком задачи (выбран в поле "кем дано"). 
    from_request_user_tasks = Task.layer_objects.filter(
        # author_task = user,
        author = request.user,
        date_add__lte =  date_end,
        date_end__gte =  date_start,
    )
    from_request_user_tasks_count = from_request_user_tasks.count()

    # может ли текущий пользователь просматривать блок с поручениями
    can_view_tasks  = False

    open_tasks     = []
    complete_tasks = []
    deleted_tasks  = []
    overdue_tasks  = []

    all_tasks_count  = Task.objects.filter(
        author       = request.user,
        date_add__lte =  date_end,
        date_end__gte =  date_start,
    ).values('hash_task').annotate(max_id = Max('id')).count()

    # если профиль просматривает кто-то выше по иеррархии, то покажем все остальные задачи, 
    if request_user_profile.pk in all_bosses_profile_ids or request.user.pk == user.pk:
        can_view_tasks = True

        open_tasks = list(Task.layer_objects.filter(
            author_task = user,
            date_add__lte =  date_end,
            date_end__gte =  date_start,
        ).exclude(progress = 100, deleted = True))

        complete_tasks_ids = Task.objects.filter(
            author_task  = user,
            date_add__lte =  date_end,
            date_end__gte =  date_start,
            progress     = 100,
        ).values('hash_task').annotate(max_id = Max('id'))
        complete_tasks_ids = [r['max_id'] for r in complete_tasks_ids]
        complete_tasks     += list(Task.objects.filter(pk__in = complete_tasks_ids))


        deleted_tasks_ids  = Task.objects.filter(
            author_task  = user,
            date_add__lte =  date_end,
            date_end__gte =  date_start,
            deleted      = True,
        ).values('hash_task').annotate(max_id = Max('id'))
        deleted_tasks_ids = [r['max_id'] for r in deleted_tasks_ids]
        deleted_tasks     += list(Task.objects.filter(pk__in = deleted_tasks_ids))


        overdue_tasks_ids  = Task.objects.filter(
            author_task  = user,
            date_end__lt = datetime.datetime.now().date(),
            # layer        = datetime.datetime.now().date()
            date_add__lte =  date_end,
            date_end__gte =  date_start,
        ).exclude(progress = 100).values('hash_task').annotate(max_id = Max('id'))
        overdue_tasks_ids = [r['max_id'] for r in overdue_tasks_ids]
        overdue_tasks     += list(Task.objects.filter(pk__in = overdue_tasks_ids))

        # all_tasks_count = Task.objects.filter(author_task = u.user, date_add__range=(date_start, date_end)).values_list('hash_task').distinct().count()
        all_tasks_count = Task.objects.filter(
            Q(author = request.user) |
            Q(author_task  = user)
        ).filter(
            date_add__lte =  date_end,
            date_end__gte =  date_start,
        ).values('hash_task').annotate(max_id = Max('id')).count()


    open_tasks_count     = len(open_tasks)
    complete_tasks_count = len(complete_tasks)
    deleted_tasks_count  = len(deleted_tasks)
    overdue_tasks_count  = len(overdue_tasks)

    tasks = []
    if open_tasks:
        tasks = open_tasks
    elif complete_tasks:
        tasks = complete_tasks
    elif deleted_tasks:
        tasks = deleted_tasks
    elif overdue_tasks:
        tasks = overdue_tasks

    can_view_review_fields = viewed_profile.can_view_review_fields(request.user)
    return direct_to_template(request, 'accounts/view_account_with_tasks.html', {
        'can_view_tasks' : can_view_tasks,
        'profile': UserProfile.objects.get(user = user.id),
        'request_user': request_user_profile,
        'today': datetime.datetime.date(datetime.datetime.now()),
        'open_tasks': open_tasks,
        'complete_tasks': complete_tasks,
        'deleted_tasks': deleted_tasks,
        'overdue_tasks': overdue_tasks,
        'from_request_user_tasks' : from_request_user_tasks,
        'tasks_count': all_tasks_count,
        'open_tasks_count': open_tasks_count,
        'complete_tasks_count': complete_tasks_count,
        'deleted_tasks_count': deleted_tasks_count,
        'overdue_tasks_count': overdue_tasks_count,
        'from_request_user_tasks_count' : from_request_user_tasks_count,
        'is_test': is_test(),
        'from': request.GET.get('from'),
        'filters_form' : filters_form,
        'can_view_review_fields' : can_view_review_fields,
    })


@login_required
def edit_employers(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    backend = get_backend('registration.backends.default.DefaultBackend')
    if request.method == "POST":
        reg_form = RegistrationForm(request.POST, request.FILES, prefix="registration")
        if reg_form.is_valid():
            new = backend.register(request, **reg_form.cleaned_data)
            profile = UserProfile.objects.get(user = new.id)
            profile.middle_name = reg_form.cleaned_data['middle_name']
            profile.job = reg_form.cleaned_data['job']
            profile.save()
    users_list = User.objects.all().order_by('last_name')
    reg_form = RegistrationForm(prefix="registration")
    return direct_to_template(request, 'accounts/employers_edit.html', {
        "users_list": users_list,
        "form": reg_form,
        'is_test': is_test(),
    })


def ajax_edit(request):
    if request.method == "GET":
        if request.GET.get('feedback_edit'):
            try:
                profile = UserProfile.objects.get(user = request.user)
                profile.feedback = request.GET.get('feedback_edit')
                profile.save()
                if config.CAN_SEND_MAIL:
                    send_mail(
                        "Создание/Изменение рационального предложения",
                        u"Пользователь %s добавил рациональное предложение:\n %s" % (
                            request.user, request.GET.get('feedback_edit')),
                        u"Автоматический отчет о состоянии рационального предложения в Системе КАИП",
                        ("btrigub@govvrn.ru", "neremina@govvrn.ru", "lpodoprihina@govvrn.ru", "fedorova.air@gmail.com"),
                        fail_silently=False
                    )
            except:
                pass
        if request.GET.get('kadr_edit'):
            try:
                profile = UserProfile.objects.get(user=request.user)
                if profile.tree.id == 22:
                    profile_edit = UserProfile.objects.get(id=request.GET.get('kadr_edit_id'))
                    profile_edit.kadr = request.GET.get('kadr_edit')
                    profile_edit.save()
            except:
                pass
    return HttpResponse('')
