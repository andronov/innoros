from django.contrib import admin
from accounts.models import *
from mptt.admin import MPTTModelAdmin

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'middle_name', 'location', 'job', 'birthday', 'tree', 'role', 'dismissed')

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(TreeDepartment, MPTTModelAdmin)