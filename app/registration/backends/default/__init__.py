from django.conf import settings
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site
from registration import signals
from registration.forms import RegistrationForm, RegistrationFormInvaite
from registration.models import RegistrationProfile


class DefaultBackend(object):
    def register(self, request, **kwargs):
        username, email, password = kwargs['username'], kwargs['email'], kwargs['password1']
        first_name, last_name = kwargs['first_name'], kwargs['last_name']
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        new_user = RegistrationProfile.objects.create_inactive_user(username, email, password, site)
        signals.user_registered.send(sender=self.__class__, user=new_user, request=request)
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.save()
        return new_user

    def activate(self, request, activation_key):
        activated = RegistrationProfile.objects.activate_user(activation_key)
        if activated:
            signals.user_activated.send(sender=self.__class__,
                                        user=activated,
                                        request=request)
        return activated

    def registration_allowed(self, request):
        return getattr(settings, 'REGISTRATION_OPEN', True)

    def get_form_class(self, request):
        return RegistrationFormInvaite

    def post_registration_redirect(self, request, user):
        return ('registration_complete', (), {})

    def post_activation_redirect(self, request, user):
        return ('registration_activation_complete', (), {})
