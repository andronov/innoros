# -*- coding: utf-8 -*-
from datetime import datetime, timedelta, time
from dateutil.relativedelta import relativedelta
from django import forms
import constants

class PeriodFilters(forms.Form):
    '''
    Форма позволяющая выбрать диапазон дат.
    Можно выбирать как предустановленный диапазон (сегодня, последняя неделя ... ),
    так и произвольный.
    '''
    class Media():
        js = ('js/filter_period.js',)

    EMPTY_PERIOD_CHOICE = ('', u'- Выбрать период -')
    period = forms.ChoiceField(
        choices  = (EMPTY_PERIOD_CHOICE,) + constants.PERIOD_CHOICES,
        label    = u'Период',
        required = False,
    )
    start = forms.DateField(
        label    = u'от',
        required = False,
    )
    end = forms.DateField(
        label    = u'до',
        required = False,
    )

    def __init__(self, *args, **kwargs):
        super(PeriodFilters, self).__init__(*args, **kwargs)
        self.fields['period'].widget.attrs = {
            'class': 'form-filters__period',
        }
        self.fields['start'].widget.attrs = {
            'class': 'form-filters__start-date',
        }
        self.fields['end'].widget.attrs = {
            'class': 'form-filters__end-date',
        }

    def clean(self):
        values = super(PeriodFilters, self).clean()
        self.define_date_period()
        return values

    def define_date_period(self):
        values = self.cleaned_data
        today = datetime.today()
        today_start = datetime.combine(today, time.min)
        today_end   = datetime.combine(today, time.max)
        mega_small_date = datetime(1900, 1, 1)
        mega_big_date = datetime(2900, 1, 1)

        if not values['period']:
            # start = today_start
            # end   = today_end
            values['period'] = 'all_time'

        if values['period'] == 'custom':
            start = values['start']
            end   = values['end']
        else:
            end = today_end
            if values['period'] == 'today':
                start = end.replace(hour = 0, minute = 1)
            elif values['period'] == 'last_week':
                start = end + relativedelta(days = -7)
            elif values['period'] == 'last_month':
                start = end + relativedelta(months = -1)
            elif values['period'] == 'last_quart':
                start = end + relativedelta(months = -3)
            elif values['period'] == 'last_year':
                start = end + relativedelta(years = -1)
            elif values['period'] == 'cur_month':
                start = end.replace(day = 1)
            elif values['period'] == 'all_time':
                start = mega_small_date
                end = mega_big_date

        self.start = start
        self.end   = end
