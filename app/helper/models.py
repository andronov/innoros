# -*- coding: utf-8 -*-
from django.db import models

class GetVerboseNamesMixin(object):
    def get_verbose_names(self):
        labels = {}
        for field in self._meta.fields:
            labels[field.name] = field.verbose_name
        return labels
