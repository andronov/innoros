# -*- coding: utf-8 -*-

# Пустое значение для choice-типов полей
EMPTY_CHOICE_VALUE = ''
EMPTY_CHOICE = (EMPTY_CHOICE_VALUE, '- Выбрать -')

# choices для поля "период", используемое в формах фильтров
PERIOD_CHOICES = (
    ('today', u'Сегодня'),
    ('cur_month', u'Текущий месяц'),
    ('last_week', u'Последняя неделя'),
    ('last_month', u'Последний месяц'),
    ('last_quart', u'Последний квартал'),
    ('last_year', u'Последний год'),
    ('all_time', u'За все время'),
    ('custom', u'Произвольный период'),
)

# choices для поля "вид", используемое в формах фильтров
DISPLAY_TYPE_CHOICES = (
    ('table', u'Таблица'),
    ('bar', u'Столбцы'),
    ('pie', u'Круги'),
)

# Типы совершаемых пользователем действий
ACTION_TYPE_CHOICES = (
    ('create', u'Создание'),
    ('change', u'Изменение'),
    ('delete', u'Удаление'),
    ('complete', u'Исполнение'),
    ('visit', u'Посещение'),
)

# Возможные значения для списков инициаторов изменений полей
FIELD_EDITOR_PERFORMER_CHOICE = 'performer'
FIELD_EDITOR_AUTHOR_CHOICE = 'author'
FIELD_EDITOR_CHOICES = (
    (FIELD_EDITOR_PERFORMER_CHOICE, u'Исполнитель'),
    (FIELD_EDITOR_AUTHOR_CHOICE, u'Постановщик'),
)
