#!/usr/bin/python
# -*- coding: utf-8 -*-
#import datetime
#from django.conf.urls.defaults import patterns
#from django.contrib import admin
#from django.core.exceptions import PermissionDenied
#from django.shortcuts import get_object_or_404
#from models import *
#from guardian.admin import GuardedModelAdmin
#from guardian.shortcuts import get_objects_for_user, assign
#from django.utils.text import truncate_words
#from django.utils.translation import ungettext, ugettext, ugettext_lazy as _
#import reversion
#from django.core import urlresolvers
#from action import export_as_xls, export_as_csv_action, export_as_pdf
#from django.contrib.contenttypes import generic
#
#
#class FilesInline(generic.GenericTabularInline):
#    model = FilesItem

#class TaskAdmin(GuardedModelAdmin, reversion.VersionAdmin):
#    list_display = ('id', 'title', 'introtask', 'check_file', 'priority', 'author','performer','date_add', 'date_end')
#    list_filter = ('priority', 'author','performer',)
#    date_hierarchy = 'date_add'
#    search_fields = ['title']
#    exclude = ['progress',]
#    actions = [export_as_csv_action("CSV Export"),export_as_xls, export_as_pdf]
#    list_editable =[]
#    readonly_fields = []
#    inlines = [
#        FilesInline,
#        ]
#    def introtask(self, obj):
#        return ("%s") % truncate_words(obj.text, 10)
#    introtask.short_description = _('Itrotext')
#
#    def link_history(self, obj):
#        return u'<a href="%shistory" class="org">%s</a>' % ( urlresolvers.reverse('admin:project_task_change', args=(obj.id,)),_('History'))
#    link_history.short_description = _('History object')
#    link_history.allow_tags = True
#
#    def link_access(self, obj):
#        return u'<a href="%s" class="org">%s</a>' % (urlresolvers.reverse('admin:project_task_permissions', args=(obj.id,)),_('Access'))
#    link_access.short_description = _('Access')
#    link_access.allow_tags = True
#
#
#    def progress_status(self, obj):
#        now = datetime.datetime.now()
#        if now > obj.date_end or int((now - obj.date_end).days) == -1:
#            return u"100%"
#        a = int((obj.date_end - now).days)
#        i = 100 - (float(a) * 100 / (obj.date_end - obj.date_add).days)
#        return u"%s" % int(i) + u"%"
#    progress_status.short_description = _('Bar')
#
#    def check_file(self, obj):
#        if obj.files.all():
#            return _("Yes file")
#        return _("No")
#    check_file.allow_tags = True
#    check_file.short_description = _('File')


#    def queryset(self, request):
#        qs = super(GuardedModelAdmin, self).queryset(request)
#        if not request.user.is_superuser:
#            qs = get_objects_for_user(request.user, "view_task", Task.objects.all())
#        return qs
#
#    def save_model(self, request, obj, form, change):
#        if obj.id is None or request.user.has_perm('add_task', obj) == True:
#           if not request.user.is_superuser and not obj.id:
#              obj.author = request.user
#              obj.last_modified_by = request.user
#             obj.save()
#              assign('view_task', request.user, obj)
#              assign('view_access', request.user, obj)
#              assign('change_task', request.user, obj)
#              assign('add_task', request.user, obj)
#              assign('delete_task', request.user, obj)
#              assign('view_history', request.user, obj)
#              try:
#                   for g in request.user.groups.all():
#                       assign('view_task', g, obj)
#                       assign('change_task',g, obj)
#              except:
#                        pass
#           else:
#               obj.save()
#        if request.user.has_perm('change_task', obj):
#            if not request.user.is_superuser and not obj.id:
#                obj.author = request.user
#                obj.last_modified_by = request.user
#            obj.save()
#        if request.user.is_superuser:
#            obj.save()


#    def add_view(self, request):
#        if not request.user.is_superuser:
#           self.exclude = self.exclude + ['author']
#        else:
#           self.exclude = ['progress',]
#        self.exclude = self.exclude + ['evaluation']
#        return super(TaskAdmin, self).add_view(request)
#
  #  def change_view(self, request, object_id):
  #      self.exclude = ['progress',]
  #      obj = get_object_or_404(self.queryset(request), id=object_id)
  #      if not request.user.is_superuser and request.user.has_perm('change_task', obj) != True:
  #          self.readonly_fields = ['title','priority','performer','text','comment','date_add','date_end', 'evaluation']
  #      else:
  #          self.readonly_fields = []
  #      if not request.user.is_superuser:
  #              self.exclude = self.exclude + ['author']
  #      my_object = get_object_or_404(Task, id=object_id)
  #      if not request.user.is_superuser and my_object.author.id != request.user.id:
  #             self.exclude = self.exclude + ['evaluation']
  #      return super(TaskAdmin, self).change_view(request, object_id)

  #  def obj_perms_manage_view(self, request, object_pk):
  #      obj = get_object_or_404(self.queryset(request), pk=object_pk)
  #      if not request.user.has_perm('view_access', obj):
  #          raise PermissionDenied
  #      return super(TaskAdmin, self).obj_perms_manage_view(request, object_pk)

   # def history_view(self, request, object_id):
   #     obj = get_object_or_404(self.queryset(request), id=object_id)
   #     if not request.user.has_perm('view_history', obj):
    #        raise PermissionDenied
    #    return super(TaskAdmin, self).history_view(request, object_id)

    # def delete_view(self, request, object_id):
    #    obj = get_object_or_404(self.queryset(request), id=object_id)
    #    if not request.user.has_perm('delete_task', obj):
    #        raise PermissionDenied
    #    return super(TaskAdmin, self).delete_view(request, object_id)

    #def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #    if not request.user.is_superuser:
    #        if db_field.name == "performer":
    #            kwargs["queryset"] = User.objects.filter(groups=request.user.groups.all())
    #    return super(TaskAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    #def recoverlist_view(self, request):
    #    if not request.user.is_superuser:
    #        raise PermissionDenied
    #    return super(TaskAdmin, self).recoverlist_view(request)

    #def recover_view(self, request, version_id):
    #    if not request.user.is_superuser:
    #        raise PermissionDenied
    #    return super(TaskAdmin, self).recover_view(request, version_id)

    #def revision_view(self, request, object_id, version_id):
    #    obj = get_object_or_404(self.queryset(request), id=object_id)
    #    if not request.user.has_perm('view_history', obj):
    #        raise PermissionDenied
    #    return super(TaskAdmin, self).revision_view(request, object_id, version_id)


#admin.site.register(Task, TaskAdmin)


