import datetime
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand
from project.models import Task, FilesItem
from django.db.models import Q

class Command(BaseCommand):
    help = "Run one day !!!!"

    def handle(self, *args, **options):
      if len(Task.objects.select_related().filter(layer = datetime.datetime.now().date())) == 0:
        for task in Task.objects.select_related().filter(layer = (datetime.datetime.date(datetime.datetime.now()) + relativedelta(days=-1)), deleted = 0, terminated = 0).filter(Q(progress__lt = 100) | Q(progress__isnull = True)):
            files = []
            if task.edit_task != task.create_task:
                task.edit = 1
            else:
                task.edit = 0
            now = datetime.datetime.now()
            new = Task(
                   number = task.number,
                   nomer = task.nomer,
                   author_task = task.author_task,
                   title = task.title,
                   priority = task.priority,
                   author = task.author,
                   performer = task.performer,
                   text = task.text,
                   comment = task.comment,
                   date_add = task.date_add,
                   date_end = task.date_end,
                   progress = task.progress,
                   evaluation = task.evaluation,
                   hash_task = task.hash_task,
                   layer = datetime.datetime.date(datetime.datetime.now()),
                   edit_task = now,
                   create_task = now,
                   edit = task.edit
            )
            new.save()
            try:
              new.files = task.files.all()
              for f in task.files.all():
                r = FilesItem(**f)
                r.save()
                r.task = new.id
                r.save()
                new.files = r.id
              new.save()
            except :
                pass
