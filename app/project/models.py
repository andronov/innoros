#!/usr/bin/python
# -*- coding: utf-8 -*-


import re
import time
import calendar
import datetime

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from dateutil.parser import *
from django.db.models import Min
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from helper import constants


class FilesItem(models.Model):
    task = models.ForeignKey('Task', null=True, blank=True)
    file = models.FileField(verbose_name=_('File'), upload_to=('upload'), blank=True)

    def get_name(self):
        return re.sub('upload/', '', self.file.name)


priority_choices_dict = {
    'High': _(u'Срочный'),
    'Normal': _(u'Обычный'),
    'Low': _(u'Низкий'),
}

PRIORITY_CHOICES = (
    ('High', priority_choices_dict['High']),
    ('Normal', priority_choices_dict['Normal']),
    ('Low', priority_choices_dict['Low']),
)

EVA_CHOICES = [((str(field)), (field)) for field in xrange(1, 11)]

PROGRES_CHOICES = [
    (10, '10%'),
    (20, '20%'),
    (30, '30%'),
    (40, '40%'),
    (50, '50%'),
    (60, '60%'),
    (70, '70%'),
    (80, '80%'),
    (90, '90%'),
    (100, '100%'),
]


class TaskManager(models.Manager):
    """Менеджер для задач по умолчанию. Содержит функции для упрощения получения опеределенных задач"""
    def get_query_set(self):
        return super(TaskManager, self).get_query_set().filter(
            layer__range=(datetime.datetime.date(datetime.datetime.now()),
                          datetime.datetime.date(datetime.datetime.now())),
            deleted=0)

    def assigned_to_user(self, user):
        """Возвращает задачи, назначенные пользователю"""
        qs = self.get_query_set()
        qs = qs.filter(author_task=user).exclude(deleted=True)
        return qs

    def opened(self, user):
        """Возвращает открытые и непросроченные поручения."""
        qs = self.get_query_set()
        qs = qs.filter(
            date_end__gte = datetime.datetime.now(),
        ).exclude(
            progress = 100,
            deleted = True,
        )
        return qs

    def opened_assigned_to_user(self, user):
        """Возвращает открытые и непросроченные поручения, исполнителем в которых стоит указанный пользователь."""
        qs = self.assigned_to_user(user)
        qs = qs.filter(
            date_end__gte = datetime.datetime.now(),
        ).exclude(
            progress = 100,
            deleted = True,
        )
        return qs

    def authored_by_user(self, user):
        """Возвращает поручения, поручающим (поле "Кем дано") в которых стоит указанный пользователь."""
        qs = self.get_query_set()
        qs = qs.filter(
            author = user,
        )
        return qs

    def deleted(self):
        """Возвращает удаленные задачи"""
        qs = self.get_query_set()
        qs = qs.filter(deleted=True)
        return qs

    def deleted_assigned_to_user(self, user):
        """Возвращает удаленные задачи, исполнителем которых является указанный пользователь"""
        qs = self.deleted()
        qs = qs.filter(author_task = user)

    def finished(self):
        """Возвращает завершенные задачи"""
        qs = self.get_query_set()
        qs = qs.filter(progress=100, deleted=False)
        return qs

    def overdue(self):
        """Возвращает просроченные задачи"""
        qs = self.get_query_set()
        qs = qs.filter(
            date_end__lte = datetime.datetime.now(),
        ).exclude(
            progress = 100,
            deleted = True,
        )
        return qs

    def overdue_assigned_to_user(self, user):
        """Возвращает просроченные задачи, исполнителем которых является указанный пользователь"""
        qs = self.overdue()
        qs = qs.filter(author_task = user)


class CurrentRevisionManager(TaskManager):
    """Менеджер для работы с текущими ревизиями. По умолчанию возвращает все задачи(удаленные и завершенные тоже)"""
    def get_query_set(self):
        return Task.objects.filter(is_current_revision=True)


class TaskAbstract(models.Model):
    number = models.IntegerField(max_length=200, null=True, blank=True)
    nomer = models.CharField(max_length=200, blank=True, verbose_name = u'Номер связи')
    hash_task = models.CharField(max_length=200, null=True, blank=True, editable=False)
    # По сути исполнитель задачи. Тот кто заполняет форму задачи (задачи ставит каждый себе сам, но выставляет определенного постановщика задачи)
    author_task = models.ForeignKey(User, related_name='%(app_label)s_%(class)s_post_tasked', editable=False, null=True, verbose_name=u'Автор (исполнитель)')
    title = models.CharField(_("Title"), max_length=200)
    priority = models.CharField(_('Priority'), max_length=20, choices=PRIORITY_CHOICES, blank=True, null=True, default=u'Normal')
    # постановщик задачи, которого выставляет сам исполнитель. В форме создания задачи обозначается "Кем дана"
    author = models.ForeignKey(User, related_name='%(app_label)s_%(class)s_author', blank=True, null=True, verbose_name=u'Кем дано')
    author_look = models.BooleanField(default=False, verbose_name=u'Просмотрена поставщиком ')
    performer   = models.ForeignKey(User, related_name='%(app_label)s_%(class)s_performer', blank=True, null=True)
    text        = models.TextField(_('Text'), null=True, max_length=15000)
    document    = models.CharField(max_length=254, blank = True, null=True, verbose_name = u'Распорядительный документ')
    comment     = models.TextField(_('Comment'), blank=True, null=True, max_length=15000)
    date_add    = models.DateField(_('Date add'))
    date_end    = models.DateField(_('Date end'))
    progress    = models.IntegerField(_('Progress'), blank=True, default=0, choices=PROGRES_CHOICES, null=True )
    # Затраченное время. Пока попросили обозвать его "сделано"
    time_consumed = models.IntegerField(blank = True, null = True, verbose_name = u'Сделано')
    evaluation  = models.CharField(_('Evaluation'), max_length=255, blank=True, choices=EVA_CHOICES)
    layer       = models.DateField(editable=False, auto_now_add=True, blank=True)
    deleted     = models.BooleanField(default=0, blank=True, editable=False)

    files = models.ManyToManyField(FilesItem, related_name="%(app_label)s_%(class)s_file", blank=True, null=True, help_text=u"Внимание! Русские буквы и пробелы в иени загружаемого файла будут заменены.", verbose_name = u'Прикрепленные файлы')

    terminated      = models.BooleanField(_(u'Прекращено'), default=False)
    terminated_text = models.TextField(_(u'Напишите причину'), blank=True, null=True)

    edit = models.IntegerField(default=0, editable=False, blank=True, null=True)
    edit_task = models.DateTimeField(auto_now=True)
    create_task = models.DateTimeField(auto_now_add=True)

    #Указывает на то, что эта запись является текущей версией данной задачи, используется для облегчения запросов
    is_current_revision = models.BooleanField(default=False, db_index=True, verbose_name=_('Is current revision'))

    objects = models.Manager()
    layer_objects = TaskManager()
    task_manager = CurrentRevisionManager()

    class Meta:
        abstract = True
        verbose_name = _(u'Task')
        verbose_name_plural = _(u'Tasks')
        ordering = ['-number']

class Task(TaskAbstract):
    class Meta(TaskAbstract.Meta):
        abstract = False

    def get_task_revisions(self):
        """Возвращает список layer, соответствующих данной задаче"""
        # TODO: Когда будет удален layer, переделать для поддержки ревизий
        return Task.objects.filter(hash_task=self.hash_task)

    def get_days(self):
        if int((self.date_add - self.date_end).days) == -1 or int((self.date_add - self.date_end).days) == 0:
            return 1
        return (self.date_end - self.date_add).days or ''

    def get_lefts(self):
        return int((datetime.datetime.date(parse(str(self.date_end))) - datetime.datetime.date(datetime.datetime.now().today())).days)

    def save(self, *args, **kwargs):
        super(Task, self).save(*args, **kwargs)

    def get_progress(self):
        if self.progress:
            return self.get_progress_display()
        return u"0%"

    def __unicode__(self):
        return self.title

    def editing_task(self):
        if self.edit == 1:
            return True
        return False

    @staticmethod
    def get_tasks_by_month_list(count_month=12):
        task_list = []
        count_month = count_month
        now = time.localtime()
        month_list = [time.localtime(time.mktime([now.tm_year, now.tm_mon - n, 1, 0, 0, 0, 0, 0, 0]))[:2] for n in xrange(count_month)]
        for month in month_list:
            count = Task.objects.filter(
                layer__month=month[1],
                layer__year=month[0]).values_list('hash_task').distinct().count()
            task_list.append((month[1], month[0], count))
        return task_list

    @staticmethod
    def get_tasks_by_month():
        min_date = Task.objects.aggregate(Min('layer')).values()[0]
        current_date = datetime.datetime.now().date()
        task_dict = {}
        while (min_date.year, min_date.month) <= (current_date.year, current_date.month):
            tuple_date = current_date.timetuple()
            timestamp_utc = calendar.timegm(tuple_date)
            task_dict[timestamp_utc * 1000] = Task.objects.filter(
                layer__month=current_date.month,
                layer__year=current_date.year
            ).values_list('hash_task').distinct().count()
            current_date -= datetime.timedelta(days=calendar.monthrange(current_date.year, current_date.month)[1])
        return task_dict

    @staticmethod
    def get_tasks_by_month_from_year(*year_list):
        month_list = xrange(1, 13)
        data = {}
        for month in month_list:
            for year in year_list:
                if month not in data:
                    data[month] = Task.objects.filter(layer__month=month, layer__year=year).values_list('hash_task').distinct().count(),
                else:
                    data[month] += Task.objects.filter(layer__month=month, layer__year=year).values_list('hash_task').distinct().count(),
        return data

    @staticmethod
    def get_all_tasks_for_employees(year):
        user_list = User.objects.all()
        data = {}
        for user in user_list:
        #for year in year_list:
            user_name = str(user)
            tasks = Task.objects.filter(
                author_task=user,
                layer__year=year
            ).values_list('hash_task').distinct().count(),
            if user_name not in data:
                data[user_name] = tasks
            else:
                data[user_name] += tasks
        print data
        return data

    def get_until_date_end_days(self):
        '''
        Осталось дней до окончания задачи
        '''
        today = datetime.date.today()
        date_end = self.date_end
        delta = date_end - today
        return delta.days

@receiver(post_save, sender=Task)
def task_update_is_current_revision(sender, instance, created, *args, **kwargs):
    layers = Task.objects.filter(hash_task=instance.hash_task).order_by('-layer', '-pk')
    top_layer = layers[0]
    if not top_layer.is_current_revision:
        layers.update(is_current_revision=False)
        top_layer.is_current_revision = True
        top_layer.save()

# Действие, совершаемое пользователем с поручнием (создание, удаление, изменение, etc.)
class Action(models.Model):
    action = models.CharField(choices=constants.ACTION_TYPE_CHOICES, max_length=50)
    user = models.ForeignKey(User, verbose_name=u'Пользователь', related_name='action_user')
    task = models.ForeignKey(Task, verbose_name=u'Поручение', related_name='action_task')
    # так как поручение каждый день клонируется и у него меняется id, то нужен hash_task,
    # который будет однозначно идентифицировать поручение во всех его клонах
    hash_task = models.CharField(max_length=200, null=True, blank=True, editable=False)
    date_create = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, action, user, task):
        user = User.objects.get(pk = user.pk)
        action = cls(action=action, user=user, task=task, hash_task = task.hash_task)
        action.save()
        return action

    def __unicode__(self):
        return self.action

    class Meta:
        ordering = ["-date_create"]
