# -*- coding: utf-8 -*-

def find_max_common_denominator(fraction_top, fraction_bottom):
    """Находит максимальный общий делитель дроби. Возвращает 1, если такого числа нет"""
    if type(fraction_top) is float:
        fraction_top = int(fraction_top)
    if fraction_top == 0 or fraction_top == 1:
        return 1
    result = fraction_top
    while result > 1:
        if fraction_top % result == 0 and fraction_bottom % result == 0:
            return result
        result -= 1
    return 1