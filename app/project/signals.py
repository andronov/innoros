#from django.contrib.auth.models import User, Group
#from guardian.shortcuts import assign
#
#def AddPermissions(sender, instance, **kwargs):
#        user = User.objects.get(id = instance.author.id)
#        assign('view_task', user, instance)
#        assign('view_access', user, instance)
#        assign('change_task', user, instance)
#        assign('add_task', user, instance)
#        assign('delete_task', user, instance)
#        assign('view_history', user, instance)
#        try:
#          for g in user.groups.all():
#              assign('view_task', g, instance)
#              assign('change_task',g, instance)
#        except:
#            pass

from project.models import FilesItem
from django.core.signals import request_finished
from django.dispatch import receiver

@receiver(request_finished)
def upload_received_handler(sender, data, **kwargs):
    if file:
        new_media = FilesItem.objects.create(
            file = data,
            new_upload = True,
        )

        new_media.save()
