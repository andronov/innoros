# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import uuid
from django import forms
from django.contrib.auth.models import User
from project.models import Task, FilesItem
from widgets import CalendarWidget
from django.forms.models import modelformset_factory, inlineformset_factory
from django.contrib.admin.widgets import AdminFileWidget
from helper import constants
import logging
from accounts.forms import UserFullName
from project.task_history.models import TaskRevision, ChangedTaskField
from helper.forms import PeriodFilters


class FilesForm(forms.ModelForm):
    class Meta():
        model = FilesItem
    file = forms.FileField(widget = AdminFileWidget)


class TaskModelForm(forms.ModelForm):
    class Meta():
      exclude= ['number', 'hash_task', 'time_consumed']
      model = Task

    class Media():
        css = {
            'all': [
                'project/css/task_form.css',
            ],
        }

    # author = forms.ModelChoiceField(queryset=UserFullName.objects.filter(is_active=True, groups__id__in = (1, 2)).distinct().order_by('last_name'), required=False)
    # performer = forms.ModelChoiceField(queryset=UserFullName.objects.filter(is_active=True, groups__id__in = (1, 2)).distinct().order_by('last_name'), required=False)

    author = forms.ModelChoiceField(
        queryset = UserFullName.objects.filter(is_active = True, groups__id = 1).distinct().order_by('last_name'),
        label = Task._meta.get_field_by_name('author')[0].verbose_name
    )
    performer = forms.ModelChoiceField(
        queryset = UserFullName.objects.filter(is_active = True, groups__id = 1).distinct().order_by('last_name'),
        required = False,
        label = Task._meta.get_field_by_name('performer')[0].verbose_name
    )
    text = forms.CharField(
        widget     = forms.Textarea(attrs = {'maxlength': '15000'}),
        max_length = 15000,
        required   = True,
        label = Task._meta.get_field_by_name('text')[0].verbose_name
    )
    comment = forms.CharField(
        widget     = forms.Textarea(attrs = {'maxlength': '15000'}),
        max_length = 15000,
        required   = False,
        label = Task._meta.get_field_by_name('comment')[0].verbose_name
    )

    #
    # Инициаторы изменения
    #
    # инициатор изменения названия поручения
    title_editor = forms.ChoiceField(
        choices = (constants.EMPTY_CHOICE,) + constants.FIELD_EDITOR_CHOICES,
        required = False,
        label = u'Инициатор изменения (названия)',
    )
    # инициатор изменения описания поручения
    text_editor = forms.ChoiceField(
        choices = (constants.EMPTY_CHOICE,) + constants.FIELD_EDITOR_CHOICES,
        required = False,
        label = u'Инициатор изменения (описания)',
    )
    # инициатор изменения срока окончания поручения
    date_end_editor = forms.ChoiceField(
        choices = (constants.EMPTY_CHOICE,) + constants.FIELD_EDITOR_CHOICES,
        required = False,
        label = u'Инициатор изменения (сроков)',
    )

    # список полей, для которых в форме выбирают инициатора изменений
    FIELDS_WITH_EDITOR_SELECT = [
        'title',
        'text',
        'date_end',
    ]

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(TaskModelForm, self).__init__(*args, **kwargs)

        # Добавляем поле id модели
        self.add_task_id_field()

        # запрещаем редактировать поля, которые данному пользователю нельзя редактировать
        self.disable_forbidden_fields()

        # добавляем поле для ввода дополнительного числа часов затраченного на работу над поручением
        self.add_time_consumed_plus_field()

        self.fields.keyOrder = [
            'object_id',
            'title',
            'title_editor',
            'nomer',
            'author',
            'date_add',
            'date_end',
            'date_end_editor',
            'priority',
            'progress',
            'evaluation',
            'time_consumed_plus',
            'text',
            'text_editor',
            'document',
            'comment',
            'files',
        ]

        # При создании нового поручения поля инициаторов изменения не показываем.
        if not getattr(self.instance, 'id', None):
            self.remove_editor_fields()
        else:
            self.fields['files'].widget.attrs['id'] = 'field-files-{0}'.format(self.instance.id)
            self.fields['title'].widget.attrs['data-initial'] = self.instance.title
            self.fields['text'].widget.attrs['data-initial'] = self.instance.text
            self.fields['date_end'].widget.attrs['data-initial'] = self.instance.date_end.strftime('%d.%m.%Y')
            self.until_date_end_days = self.instance.get_until_date_end_days()

    def disable_forbidden_fields(self):
        '''
        Меняет виджет для полей, которые данному пользователю нельзя редактировать,
        так, чтобы их невозможно было отредактировать
        '''
        try:
            # если текущий пользователь является исполнителем
            if self.request.user.id != self.instance.author.id:
                # деактивируем поле "оценка"
                self.fields['evaluation'].widget.attrs['disabled'] = 'disabled'
        except:
            pass

    def add_task_id_field(self):
        '''
        Добавляем поле id модели
        '''
        task_id = getattr(self.instance, 'pk', '')
        self.fields['object_id'] = forms.CharField(
            initial = task_id,
            widget  = forms.HiddenInput(),
            required = False,
        )

    def add_time_consumed_plus_field(self):
        '''
        Добавляет поле для ввода дополнительного числа часов затраченного на работу над поручением.
        Из этого поля значение приплюсовывается к значению поля "сделано"
        '''
        self.fields['time_consumed_plus'] = forms.IntegerField(min_value = 0, label = Task._meta.get_field_by_name('time_consumed')[0].verbose_name, required = False, help_text = u'ч.')

    @classmethod
    def get_editor_field_name(cls, field_name):
        return '{0}_editor'.format(field_name)

    @classmethod
    def get_editor_fields_names(cls):
        '''
        Возвращает список имен полей инициаторов изменения.
        '''
        return [cls.get_editor_field_name(f) for f in cls.FIELDS_WITH_EDITOR_SELECT]

    def remove_editor_fields(self):
        '''
        Удаляет поля инициаторов изменения.
        '''
        for f in TaskModelForm.get_editor_fields_names():
            del(self.fields[f])

    def as_div(self):
        '''
        Рендерит форму, помещая каждое поле в div.
        '''
        return self._html_output(
            normal_row = u'<div>%(label)s %(field)s%(help_text)s</div>',
            error_row = u'%s',
            row_ender = '',
            help_text_html = u' %s',
            errors_on_separate_row = False,
        )

    def clean_text(self):
        if self.cleaned_data['text'] == self.cleaned_data['title']:
            raise forms.ValidationError(u'В поле подробности не должен содержать только название задачи!')

        return self.cleaned_data['text']

    def clean(self):
        self.cleaned_data = super(TaskModelForm, self).clean()

        # Установим инициаторов изменений полей в виде конкретных пользователей.
        self.prepare_editor_fields()

        if self.cleaned_data['date_add'] > self.cleaned_data['date_end']:
            raise forms.ValidationError('Данные не сохранены. Вы неправильно указали даты')

        return self.cleaned_data

    def prepare_editor_fields(self):
        '''
        В полях "инициатор изменения" указывается тип ("исполнитель", "поручающий"),
        в этой функции мы подставляем нужного пользователя
        исходя из указанного типа.
        '''
        for f in TaskModelForm.get_editor_fields_names():
            if not getattr(self.instance, 'id', None):
                # для нового поручения поля "инициатор" не показываются
                # и инициатором для всех является поручающий из "кем дано"
                self.cleaned_data[f] = self.cleaned_data['author']
                continue

            value = self.cleaned_data[f]
            if value == constants.FIELD_EDITOR_PERFORMER_CHOICE:
                value = self.instance.author_task
            elif value == constants.FIELD_EDITOR_AUTHOR_CHOICE:
                value = self.instance.author
            else:
                value = self.instance.author_task
            self.cleaned_data[f] = value

    def get_editor_by_field(self, field_name):
        '''
        Возвращает пользователя, который считается инициатором изменения указанного поля.
        '''
        if field_name in TaskModelForm.FIELDS_WITH_EDITOR_SELECT:
            # имя поля "инициатор изменения" именно для указанного поля
            editor_field_name = TaskModelForm.get_editor_field_name(field_name)
            return self.cleaned_data[editor_field_name]
        else:
            # На всякий случай, так как в request.user находится объект типа SimpleLazyObject
            # return User.objects.get(pk = self.request.user.pk)
            return User.objects.get(pk = self.request.user.pk)

    def has_changed_fields(self, task):
        '''
        Проверяет изменились ли какие-то редактируемые поля в поручении по сравнению с последней ревизией.
        '''
        task.diff_from_last_revision = TaskRevision.diff_from_task(task)
        if task.diff_from_last_revision:
            return True
        else:
            return False

    def save(self, commit = True):
        if getattr(self.instance, 'id', None):
            is_new = False
        else:
            is_new = True

        task = super(TaskModelForm, self).save(commit = True)

        # прибавляем указанное время работы над поручением к общему времени, которое хранится в моделе
        task.time_consumed = (task.time_consumed or 0) + (self.cleaned_data['time_consumed_plus'] or 0)

        if is_new:
            task.number = Task.objects.filter(author_task=self.request.viewed_user).values_list('hash_task').distinct().count() + 1
            task.author_task = self.request.user
            task.hash_task = uuid.uuid4().hex

        # Сохранение ревизии поручения
        if is_new or self.has_changed_fields(task):
            self.save_revision(task)
        return task

    def save_revision(self, task):
        '''
        Сохранение ревизии поручения
        '''
        revision_fields = {}
        for f in task._meta.fields:
            if f.name != 'id':
                revision_fields[f.name] = getattr(task, f.name)
        revision_fields.update({
            'task': task,
            'revision_author': User.objects.get(pk = self.request.user.pk),
        })
        new_revision = TaskRevision(**revision_fields)
        new_revision.save()
        for f in new_revision.diff_with_last_revision():
            result = ChangedTaskField.objects.get_or_create(
                field_name   = f,
                field_editor = self.get_editor_by_field(f)
            )
            fmodel = result[0]
            new_revision.changed_fields.add(fmodel)
        try:
            new_revision.files = task.files.all()
        except:
            pass

        new_revision.save()

class TasksBulkForm(forms.Form):
    EMPTY_ACTION_CHOICE = ('', u'- Выбрать действие -')
    ACTION_CHOICES = (
        EMPTY_ACTION_CHOICE,
        ('change_task_performer', u'Изменить исполнителя'),
    )

    action = forms.ChoiceField(
        choices  = ACTION_CHOICES,
        label    = u'Действие',
        required = False,
    )
    ids    = forms.CharField(
        max_length = 1000,
        widget     = forms.HiddenInput(),
    )
    # новый исполнитель
    author_task = forms.ModelChoiceField(
        queryset    = UserFullName.objects.filter(is_active = True, groups__id = 1).distinct().order_by('last_name'),
        empty_label = u'- Выбрать исполнителя -',
        label       = u'Исполнитель',
        required    = False,
    )


    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(TasksBulkForm, self).__init__(*args, **kwargs)

        action_choices = [ self.EMPTY_ACTION_CHOICE ]
        try:
            if self.request.user.has_perm('accounts.change_task_performer') or self.request.user.is_superuser:
                action_choices.append(('change_task_performer', u'Изменить исполнителя'))
        except:
            pass
        self.fields['action'].widget.choices = tuple(action_choices)

        if len(self.fields['action'].widget.choices) > 1:
            self.has_actions = True
        else:
            self.has_actions = False

        self.set_css_attributes()

    def set_css_attributes(self):
        self.fields['ids'].widget.attrs = {
            'class': 'tasks-bulk-form__ids',
        }
        self.fields['author_task'].widget.attrs = {
            'class': 'for-action for-change_task_performer',
        }

class HistoryFilters(PeriodFilters):
    pass
    # def __init__(self, *args, **kwargs):
    #     super(HistoryFilters, self).__init__(*args, **kwargs)
    #     # удалим из списка периодов "сегодня"
    #     for choice in self.fields['period'].widget.choices:
    #         if choice[0] == 'today':
    #             i = self.fields['period'].widget.choices.index(choice)
    #             del(self.fields['period'].widget.choices[i])
    #     choices = self.fields['period'].widget.choices

class DashboardFilters(PeriodFilters):
    pass

class UsersStatFilters(PeriodFilters):
    pass

class DatePeriodsDisplayTypeFilters(PeriodFilters):
    EMPTY_DISPLAY_TYPE_CHOICE = ('', u'- Выбрать вид -')
    display_type = forms.ChoiceField(
        label    = u'Вид',
        required = False,
    )
    def __init__(self, custom_choices=constants.DISPLAY_TYPE_CHOICES, *args, **kwargs):
        super(DatePeriodsDisplayTypeFilters, self).__init__(*args, **kwargs)

        logging.debug(custom_choices)

        self.fields['display_type']._set_choices((self.EMPTY_DISPLAY_TYPE_CHOICE,) + custom_choices)

        self.fields['display_type'].widget.attrs = {
            'class': 'form-filters__display_type',
        }


class QueryTaskComplexityDistributionFilterForm(PeriodFilters):
    display_type = forms.CharField(label=u'Отображение', initial='table', widget=forms.Select(
        choices=(
            ('table', u'Таблица'),
            ('bar_chart', u'График'),
        )
    ))


class QueryTasksChangedCompletionTermsFilterForm(PeriodFilters):
    employees = forms.ChoiceField(label=u'Сотрудники', choices=(
        ('division_managers', u'По руководителям управлений'),
        ('department_managers', u'По руководителям отделов'),
        ('all', u'По всему агентству'),
    ), initial='all')
    display_type = forms.ChoiceField(label=u'Вид', choices=(
        ('table', u'Таблица'),
        ('bar_chart', u'Диаграмма - Столбцы'),
        ('pie_chart', u'Диаграмма - Круговая'),
    ), initial='table')


class TaskFulfillmentReportFilterForm(PeriodFilters):
    display_type = forms.CharField(label=u'Отображение', initial='table', widget=forms.Select(
        choices=(
            ('table', u'Таблица'),
            ('bar_chart', u'График'),
        )
    ))


class LowActiveEmployeesFiltersForm(PeriodFilters):
    """user = forms.ModelChoiceField(queryset=User.objects.filter(is_active=True), required=False, label=u'Сотрудник',
                                  empty_label=u'Все')
    job = forms.CharField(label=u'Должность', required=False)"""
    display_type = forms.CharField(label=u'Отображение', initial='table', widget=forms.Select(
        choices=(
            ('table', u'Таблица'),
            ('bar_chart', u'График'),
        )
    ))

TASK_CHANGED_INITIATIVE_CHOICES = (
    ('', u'Все'),
    ('manager', u'По инициативе поручающего'),
    ('performer', u'По инициативе исполнителя'),
)


class QueryTasksChangedStatementFilterForm(PeriodFilters):
    """Фильтр для отчета по задачам с измененной формулировкой"""
    user = forms.ModelChoiceField(queryset=User.objects.filter(is_active=True), label=u'Сотрудник', required=False,
                                  empty_label=u'Все сотрудники')
    change_initiator = forms.ChoiceField(label=u'Инициатор изменения', required=False,
                                         choices=TASK_CHANGED_INITIATIVE_CHOICES)
    display_type = forms.ChoiceField(
        label=u'Тип отчета', initial='users',
        choices=(
            ('users', u'По пользователям'),
                ('departments', u'По подразделениям'),
        )
    )
