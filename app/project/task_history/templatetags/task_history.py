# -*- coding: utf-8 -*-
from django import template
from django.shortcuts import render_to_response, render
from django.template.loader import get_template
from django.template.context import Context
from project.models import Task
from project.task_history.models import TaskRevision, ChangedTaskField
from accounts.models import UserProfile

register = template.Library()

@register.simple_tag
def task_history_block(request, task):
    tpl = get_template('task_history/task_history_block.html')
    filters_form = None
    revisions = TaskRevision.task_revision_manager.get_all_revisions(task)
    history = []
    for i, r in enumerate(revisions):
        # измененные поля
        changed_fields = []
        # типы изменений, такие как "формулировка", "сроки", "комментарии"
        change_types = []
        changed_fields_models = r.changed_fields.all()
        try:
            previous_revision = revisions[i + 1]
        except:
            previous_revision = None
        for f in r.changed_fields.all():
            author = UserProfile.objects.get(user = f.field_editor)
            # TODO remove
            if f.field_name == 'title':
                r.assert_false = True
            else:
                r.assert_false = False
            field_changing = {
                'field' : Task._meta.get_field_by_name(f.field_name)[0].verbose_name,
                'author' : author.get_names(),
                'before' : '',
                'after' : r.render_field_value(f.field_name),
            }

            css_change_type_class = 'change-type-{0}'.format(TaskRevision.get_change_type(f.field_name))
            change_types.append(css_change_type_class);
            # if r.id == 9:
            #     assert False
            if previous_revision:
                field_changing['before'] = previous_revision.render_field_value(f.field_name)
            changed_fields.append(field_changing)
        history.append({
            'revision': r,
            # флаг первой ревизии
            'is_first_revision' : True if i == (revisions.count() - 1) else False,
            'changed_fields': changed_fields,
            'change_types' : ' '.join(set(change_types)),
        })
    context = {
        'filters_form': filters_form,
        'revisions': revisions,
        'history' : history,
    }
    # assert False
    return tpl.render(Context(context))
