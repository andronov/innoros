# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from project.models import TaskAbstract, Task
from accounts.forms import UserFullName
from accounts.models import UserProfile
from project.models import priority_choices_dict

class ChangedTaskFieldManager(models.Manager):
    def get_fields_for_task(self, task_id):
        return self.get_queryset().filter(task_revision__task__id = task_id)

class ChangedTaskField(models.Model):
    '''
    Поля, которые изменились по сравнению с прошлой ревизией.
    Также указывается инициатор изменения (field_editor),
    так как он может быть разный у разных полей в одной редакции.
    '''

    field_name = models.CharField(
        max_length = 254,
        verbose_name = u'Имя поля',
    )

    field_editor = models.ForeignKey(
        User,
        verbose_name = u'Инициатор изменения поля',
        related_name = 'changed_task_field_field_editor',
    )

    def __unicode__(self):
        return self.field_name

    class Meta:
        unique_together = ('field_name', 'field_editor')

class TaskRevisionManager(models.Manager):
    def get_all_revisions(self, task):
        '''
        Возвращает все ревизии указанного поручения, если вообще есть ревизии.
        '''
        return self.get_query_set().filter(task__hash_task = task.hash_task).order_by('-revision_created')

    def get_last_revision(self, task):
        '''
        Возвращает последнюю ревизию указанного поручения, если вообще есть ревизии.
        '''
        try:
            return self.get_query_set().filter(task__hash_task = task.hash_task).latest('revision_created')
        except:
            return None

class TaskRevision(TaskAbstract):
    '''
    Модель ревизий поручения.
    Ревизия создается при каждом измененнии поручения.
    Ревизия содержит значения всех полей поручения на момент изменения поручения,
    а также содержит поля инициаторов изменений для некоторых полей в отдельности.
    '''
    revision_number = models.IntegerField(default = 0, verbose_name = u'Номер ревизии')

    task = models.ForeignKey(
        Task,
        null = True,
        verbose_name = u'Оригинал поручения',
        related_name = 'task_revision_task',
    )

    revision_author = models.ForeignKey(
        User,
        verbose_name = u'Пользователь, изменивший поручение',
        blank = True,
        null = True,
        related_name = 'task_revision_revision_author',
    )

    revision_created = models.DateTimeField(
        auto_now_add = True,
        null = True,
        verbose_name = u'Дата создания ревизии',
    )

    changed_fields = models.ManyToManyField(
        ChangedTaskField,
        blank        = True,
        null         = True,
        related_name = 'task_revision_changed_fields',
        verbose_name = u'Измененные поля',
    )

    task_revision_manager = TaskRevisionManager()

    # TODO доделать
    # CHANGE_TYPES = {
    #     'title_or_text': {
    #         'verbose_name': u'Формулировка',
    #         'fields': ('title', 'text',)
    #     },
    #     'date_end': {
    #         'verbose_name': u'Сроки',
    #         'fields': ('date_end',),
    #     },
    #     'comment': {
    #         'verbose_name': u'Комментарии',
    #         'fields': ('comment',),
    #     },
    #     'progress': {
    #         'verbose_name': u'Процент выполнения',
    #         'fields': ('progress',),
    #     },
    # }

    def __unicode__(self):
        return '{0}: {1}'.format(self.pk, self.title)

    @classmethod
    def get_editable_fields(cls):
        '''
        Возвращает список тех полей, которые изменяются пользователями
        и которые учитываются при ведении истории изменения поручения.
        '''
        return [
            'title',
            'text',
            'author',
            'author_task',
            'comment',
            'progress',
            'evaluation',
            'priority',
            'date_add',
            'date_end',
            'time_consumed',
            'document',
            # 'terminated',
            # 'nomer',
            'number',
            # 'hash_task',
        ]

    def get_previous_revision(self):
        '''
        Возвращает модель предыдущей ревизии.
        '''
        try:
            model = TaskRevision.task_revision_manager.get_all_revisions(self.task).filter(
                revision_created__lt = self.revision_created
            ).latest('revision_created')
            return model
        except:
            return None

    def diff_with_last_revision(self):
        current = self
        previous = self.get_previous_revision()
        diff = {}
        for field in TaskRevision.get_editable_fields():
            current_value = getattr(current, field, '')
            previous_value = getattr(previous, field, '')

            # так как в моделе задачи все поля ForeignField(User) ссылаются на модель UserFullName,
            # а в моделе, ревизии - на модель User, то приведем к одному знаменателю
            if isinstance(current_value, UserFullName):
                current_value = User.objects.get(pk = current_value.pk)

            # Если поля отличаются,
            # а для случая, когда у current данное поле равно None,
            # и ревизий у поручения еще нет, то тоже запишем поле как измененное 
            if current_value != previous_value or not previous:
                diff[field] = {
                    'current': current_value,
                    'previous': previous_value,
                }
        return diff

    @classmethod
    def diff_from_task(cls, task):
        '''
        Возвращает словарь полей, измененных по сравнению с переданным объектом поручения.
        return {
            'field_name1': {
                'original': 'value_in_original_task',
                'revision': 'value_in_this_revision',
            }
        }
        '''
        original = task
        revision = TaskRevision.task_revision_manager.get_last_revision(task)
        diff = {}
        for field in cls.get_editable_fields():
            original_value = getattr(original, field, None)
            revision_value = getattr(revision, field, None)

            # так как в моделе задачи все поля ForeignField(User) ссылаются на модель UserFullName,
            # а в моделе, ревизии - на модель User, то приведем к одному знаменателю
            if isinstance(original_value, UserFullName):
                original_value = User.objects.get(pk = original_value.pk)

            # Если поля отличаются,
            # а для случая, когда у original данное поле равно None,
            # и ревизий у поручения еще нет, то тоже запишем поле как измененное 
            if original_value != revision_value or not revision:
                diff[field] = {
                    'original': original_value,
                    'revision': revision_value,
                }
        return diff

    def render_field_value(self, field_name):
        field_value = getattr(self, field_name, '')
        if isinstance(field_value, User):
            profile = UserProfile.objects.get(user = field_value)
            return profile.get_names()
        # elif isinstance(field_value, str):

        if field_name == 'progress':
            return '{0}%'.format(field_value or 0)
        elif field_name == 'priority':
            return priority_choices_dict[field_value] if field_value else '-'

        if not field_value:
            return '-'

        return field_value

    @classmethod
    def get_change_type(cls, field_name):
        if field_name == 'title' or field_name == 'text':
            return u'title_or_text'
        elif field_name == 'date_end':
            return u'date_end'
        elif field_name == 'comment':
            return u'comment'
        elif field_name == 'progress':
            return u'progress'
        else:
            return u'other'

    def get_last_revision_number(self):
        '''
        Возвращает номер последней ревизии
        '''
        last_revision = TaskRevision.task_revision_manager.get_last_revision(self.task)
        number = getattr(last_revision, 'revision_number', 0)
        if not number:
            number = 0
        return number

    def save(self, *args, **kwargs):
        if self.revision_number == 0:
            self.revision_number = self.get_last_revision_number() + 1
        super(TaskRevision, self).save(*args, **kwargs)
