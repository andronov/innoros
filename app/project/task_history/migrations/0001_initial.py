# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ChangedTaskField'
        db.create_table('task_history_changedtaskfield', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('field_name', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('field_editor', self.gf('django.db.models.fields.related.ForeignKey')(related_name='changed_task_field_field_editor', to=orm['auth.User'])),
        ))
        db.send_create_signal('task_history', ['ChangedTaskField'])

        # Adding model 'TaskRevision'
        db.create_table('task_history_taskrevision', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.IntegerField')(max_length=200, null=True, blank=True)),
            ('nomer', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('hash_task', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('author_task', self.gf('django.db.models.fields.related.ForeignKey')(related_name='task_history_taskrevision_post_tasked', null=True, to=orm['auth.User'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('priority', self.gf('django.db.models.fields.CharField')(default=u'Normal', max_length=20, null=True, blank=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='task_history_taskrevision_author', null=True, to=orm['auth.User'])),
            ('author_look', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('performer', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='task_history_taskrevision_performer', null=True, to=orm['auth.User'])),
            ('text', self.gf('django.db.models.fields.TextField')(max_length=15000, null=True)),
            ('comment', self.gf('django.db.models.fields.TextField')(max_length=15000, null=True, blank=True)),
            ('date_add', self.gf('django.db.models.fields.DateField')()),
            ('date_end', self.gf('django.db.models.fields.DateField')()),
            ('progress', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('evaluation', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('layer', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('terminated', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('terminated_text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('edit', self.gf('django.db.models.fields.IntegerField')(default=0, null=True, blank=True)),
            ('edit_task', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('create_task', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('is_current_revision', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(related_name='task_revision_task', null=True, to=orm['project.Task'])),
            ('revision_author', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='task_revision_revision_author', null=True, to=orm['auth.User'])),
            ('revision_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True)),
        ))
        db.send_create_signal('task_history', ['TaskRevision'])

        # Adding M2M table for field files on 'TaskRevision'
        m2m_table_name = db.shorten_name('task_history_taskrevision_files')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('taskrevision', models.ForeignKey(orm['task_history.taskrevision'], null=False)),
            ('filesitem', models.ForeignKey(orm['project.filesitem'], null=False))
        ))
        db.create_unique(m2m_table_name, ['taskrevision_id', 'filesitem_id'])

        # Adding M2M table for field changed_fields on 'TaskRevision'
        m2m_table_name = db.shorten_name('task_history_taskrevision_changed_fields')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('taskrevision', models.ForeignKey(orm['task_history.taskrevision'], null=False)),
            ('changedtaskfield', models.ForeignKey(orm['task_history.changedtaskfield'], null=False))
        ))
        db.create_unique(m2m_table_name, ['taskrevision_id', 'changedtaskfield_id'])


    def backwards(self, orm):
        # Deleting model 'ChangedTaskField'
        db.delete_table('task_history_changedtaskfield')

        # Deleting model 'TaskRevision'
        db.delete_table('task_history_taskrevision')

        # Removing M2M table for field files on 'TaskRevision'
        db.delete_table(db.shorten_name('task_history_taskrevision_files'))

        # Removing M2M table for field changed_fields on 'TaskRevision'
        db.delete_table(db.shorten_name('task_history_taskrevision_changed_fields'))


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'project.filesitem': {
            'Meta': {'object_name': 'FilesItem'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['project.Task']", 'null': 'True', 'blank': 'True'})
        },
        'project.task': {
            'Meta': {'ordering': "['-number']", 'object_name': 'Task'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'project_task_author'", 'null': 'True', 'to': "orm['auth.User']"}),
            'author_look': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'author_task': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'project_task_post_tasked'", 'null': 'True', 'to': "orm['auth.User']"}),
            'comment': ('django.db.models.fields.TextField', [], {'max_length': '15000', 'null': 'True', 'blank': 'True'}),
            'create_task': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_add': ('django.db.models.fields.DateField', [], {}),
            'date_end': ('django.db.models.fields.DateField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edit': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'edit_task': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'evaluation': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'files': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'project_task_file'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['project.FilesItem']"}),
            'hash_task': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_current_revision': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'layer': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'nomer': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'performer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'project_task_performer'", 'null': 'True', 'to': "orm['auth.User']"}),
            'priority': ('django.db.models.fields.CharField', [], {'default': "u'Normal'", 'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'progress': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'terminated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'terminated_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '15000', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'task_history.changedtaskfield': {
            'Meta': {'object_name': 'ChangedTaskField'},
            'field_editor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'changed_task_field_field_editor'", 'to': "orm['auth.User']"}),
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '254'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'task_history.taskrevision': {
            'Meta': {'ordering': "['-number']", 'object_name': 'TaskRevision'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'task_history_taskrevision_author'", 'null': 'True', 'to': "orm['auth.User']"}),
            'author_look': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'author_task': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'task_history_taskrevision_post_tasked'", 'null': 'True', 'to': "orm['auth.User']"}),
            'changed_fields': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'task_revision_changed_fields'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['task_history.ChangedTaskField']"}),
            'comment': ('django.db.models.fields.TextField', [], {'max_length': '15000', 'null': 'True', 'blank': 'True'}),
            'create_task': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_add': ('django.db.models.fields.DateField', [], {}),
            'date_end': ('django.db.models.fields.DateField', [], {}),
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'edit': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'edit_task': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'evaluation': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'files': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'task_history_taskrevision_file'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['project.FilesItem']"}),
            'hash_task': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_current_revision': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'layer': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'nomer': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'number': ('django.db.models.fields.IntegerField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'performer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'task_history_taskrevision_performer'", 'null': 'True', 'to': "orm['auth.User']"}),
            'priority': ('django.db.models.fields.CharField', [], {'default': "u'Normal'", 'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'progress': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'revision_author': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'task_revision_revision_author'", 'null': 'True', 'to': "orm['auth.User']"}),
            'revision_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'task_revision_task'", 'null': 'True', 'to': "orm['project.Task']"}),
            'terminated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'terminated_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'max_length': '15000', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['task_history']