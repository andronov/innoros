# -*- coding: utf-8 -*-


import xlwt
import uuid
import unidecode
import tempfile
import operator
import math

from datetime import datetime, time, timedelta

import string
from string import printable

from django import template
from django.db.models import Q, Max, F, Count
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.simple import direct_to_template
from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.mail import send_mail
from django.core.exceptions import PermissionDenied
from django.shortcuts import render_to_response, render
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from constance import config
from helper import constants

from accounts.models import *
from accounts.context_processors import get_access_user
from project.action import export_as_xls
from helper.forms import PeriodFilters
from project.forms import TaskModelForm, TasksBulkForm, UsersStatFilters, HistoryFilters, DashboardFilters, \
    DatePeriodsDisplayTypeFilters, QueryTaskComplexityDistributionFilterForm, QueryTasksChangedCompletionTermsFilterForm, \
    TaskFulfillmentReportFilterForm, LowActiveEmployeesFiltersForm, QueryTasksChangedStatementFilterForm
from project.models import *
from utils import is_test, is_sep
from project.task_history.models import TaskRevision, ChangedTaskField
from functions import find_max_common_denominator


register = template.Library()
if is_test():
    host = "http://testing.innoros.ru"
elif is_sep():
    host = "http://sep.innoros.ru"

director_id = 10
g_colors = ['#058DC7', '#f2eb00', '#50B432', '#ED561B', '#0007ff', '#24CBE5', '#FF9655', '#64E572', '#f60459', '#a25df4', '#DDDF00', '#5874A0']
g_colors_line = mark_safe("['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']")
month_names = [u"Январь", u"Февраль", u"Март", u"Апрель", u"Май", u"Июнь", u"Июль", u"Август", u"Сентябрь", u"Октябрь", u"Ноябрь", u"Декабрь"]

def front_page(request):
    return HttpResponseRedirect('/my-dashboard')

def font_dict(print_in_request):
    if print_in_request:
        font_size_9_16 = 16
        font_size_11_16 = 16
        font_size_12_16 = 16
        is_print = True
    else:
        font_size_9_16 = 9
        font_size_11_16 = 11
        font_size_12_16 = 12
        is_print = False
    res = {
        'font_size_9_16': font_size_9_16,
        'font_size_11_16': font_size_11_16,
        'font_size_12_16': font_size_12_16,
        'is_print': is_print,
    }
    return res


@register.filter
def lookup(l, key):
    return l[key]


#TODO: need universal solution
def m_filter(element):
    if element[0] or element[1]:
        return True
    else:
        return False


def m_filter(element):
    if element[0]:
        return True
    else:
        return False


def upload_file(request, *args, **kwargs):
    if request.method == "POST":
        if request.FILES:
            f = re.split(r'\.', request.FILES['Filedata'].name)
            name = re.sub(f[len(f) - 1], '', request.FILES['Filedata'].name)
            request.FILES['Filedata'].name = u"%s.%s" % (re.sub(r'\W+', '-', unidecode.unidecode(name).lower()), f[len(f) - 1])
            if request.FILES['Filedata'].size < 13362414:
                new_media = FilesItem.objects.create(file=request.FILES['Filedata'])
                new_media.save()
                return HttpResponse('<input type="hidden" value="%s" name="files">' % new_media.id)


def delete_file(request):
    f = FilesItem.objects.get(id=request.GET.get('file'))
    f.delete()
    return HttpResponse(' ')


@login_required
def print_page(request, object_id):
    get_user_task = request.GET.get('get_user') or request.user.id
    t = Task.layer_objects.get(id=object_id)
    return direct_to_template(request, 'project/print_page.html', {
        'p': t,
    })


def send_task_mail(task, title):
    if not config.CAN_SEND_MAIL:
        return False

    nl = "\n"
    href = host + "/history_standalone/?obj_id=" + unicode(task.id) + "&get_user=" + unicode(task.author_task.id)
    if task.progress is None:
        progress = u"0"
    else:
        progress = unicode(task.progress)
    priority = unicode(priority_choices_dict[task.priority])
    if task.text == "":
        details = u"Отсутствуют"
    else:
        details = unicode(task.text)
    if task.comment == "":
        comment = u"Отсутствует"
    else:
        comment = unicode(task.comment)
    msg = u"Исполнитель: " + unicode(task.author_task) + nl + u"Название: " + unicode(
        task.title) + nl + u"Процент выполнения: " + progress + "%" + nl + u"Дата поручения: " + unicode(task.date_add) + nl + u"Срок исполнения: " + unicode(
        task.date_end) + nl + u"Приоритет: " + priority + nl + u"Подробности: " + details + nl + u"Комментарий: " + comment + nl + nl + u"Поручение:" + href
    addresses = ["taskinnoros@gmail.com"]
    if task.author.id == director_id:
        addresses += [User.objects.get(id=20).email, User.objects.get(id=10).email, User.objects.get(id=44).email, User.objects.get(id=160).email]
    else:
        addresses += [task.author.email]
    send_mail(title, msg, u"Автоматический отчет о состоянии поручений в Системе КАИП", addresses, fail_silently=False)


def get_mail_title(task):
    if task.terminated:
        mail_title = u"Удалено поручение"
    elif task.progress == 100:
        mail_title = u"Выполнено поручение"
    else:
        mail_title = u"Изменено поручение"
    return mail_title


@login_required
@csrf_exempt
def dashboard(request,  user_id = None):
    if not user_id:
        user_id = request.user.id

    user = User.objects.get(pk = user_id)
    # пользователь, дашбоард которого смотрим
    request.viewed_user = user
    # урл без указания user_id просматриваемого пользователя
    # используется для редиректа при выборе пользователя из низпадающего списка в главном меню
    request.base_url_without_user_id = 'user-dashboard'

    # форма выбора периода дат
    date_start = datetime.datetime(1900, 1, 1)
    date_end   = datetime.datetime(2900, 1, 1)
    filters_form = DashboardFilters(initial = {
        'period': 'all_time',
    })
    if request.GET.get('period', None):
        filters_form = DashboardFilters(request.GET)
        if filters_form.is_valid():
            date_start = filters_form.start
            date_end   = filters_form.end

    tasks = Task.objects.filter(
        layer = datetime.datetime.now(),
        date_add__lte =  date_end,
        date_end__gte =  date_start,
        author_task  = user_id,
        deleted      = 0,
    ).exclude(progress = 100)

    user = User.objects.get(pk = user_id)
    # задачи, у которых данный пользователь является исполнителем
    for_me_tasks = Task.layer_objects.filter(
        author_task = user,
        date_add__lte =  date_end,
        date_end__gte =  date_start,
    ).exclude(progress = 100, deleted = True)
    # задачи, у которых данный пользователь является автором
    my_tasks = Task.layer_objects.filter(
        author = user,
        date_add__lte =  date_end,
        date_end__gte =  date_start,
    ).exclude(progress = 100)

    for_me_tasks_count = for_me_tasks.count()
    my_tasks_count = my_tasks.count()

    request_user_profile = UserProfile.objects.get(user=request.user)
    is_need_count_time = False
    if request_user_profile.role == 1:
        tree_users_list = []
        user_profile = UserProfile.objects.get(user__id=user_id)
        try:
            if request_user_profile.tree.is_leaf_node():
                tree_users_list = UserProfile.objects.filter(
                    tree=request_user_profile.tree).exclude(id=request_user_profile.id)
            else:
                tree_users_list = UserProfile.objects.filter(
                    tree__in=[i.id for i in request_user_profile.tree.get_descendants(include_self=True)]
                ).exclude(id=request_user_profile.id)
                tree_users_list.remove(request_user_profile)
        except Exception, e:
            pass

        if user_profile in tree_users_list:
            #need count time
            is_need_count_time = True

    if request.method == 'GET' and request.GET.get('print'):
        tasks = list(for_me_tasks) + list(my_tasks)
        return direct_to_template(request, 'project/dashboard_print.html', {
            'tasks': tasks,
            'date': datetime.datetime.now(),
            'is_print': True,
            'is_test': is_test(),
        })

    if request.method == 'POST':
        if add_task(request, user_id):
            return HttpResponseRedirect('/')
    data = {
        'for_me_tasks': for_me_tasks,
        'my_tasks': my_tasks,
        'date': datetime.datetime.now(),
        'formset': TaskModelForm(request=request),
        'uinfo': User.objects.get(id=user_id),
        'is_print': False,
        'is_need_count_time': is_need_count_time,
        'is_test': is_test(),
        'filters_form' : filters_form,
    }
    return direct_to_template(request, 'project/dashboard.html', data)

@login_required
@csrf_exempt
def my_dashboard(request):
    return dashboard(request)

@login_required
@csrf_exempt
def user_dashboard(request, user_id):
    return dashboard(request, user_id)

@login_required
def debug(request):
    users = list(User.objects.all().order_by('id'))
    data = []
    for user in users:
        data.append({
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'task_number': Task.objects.filter(author_task=user).values_list('hash_task').distinct().count() + 1
        })
    return render_to_response('debug.html', {'users': data})


def add_task(request, user_id):
    request.POST['comment'] = request.POST['comment'].replace('\n', '').replace('\r\n', '')
    task_form = TaskModelForm(request.POST, request = request)
    if task_form.is_valid():
        new_task = task_form.save(commit=False)
        uploaded_files = FilesItem.objects.filter(id__in=request.POST.getlist('files')) or None
        if uploaded_files:
            new_task.files = uploaded_files
            for uploaded_file in uploaded_files:
                uploaded_file.task = new_task
                uploaded_file.save()

        try:
            relation_task = Task.objects.get(author_task=user_id, number=new_task.nomer)
            if relation_task.nomer == "":
                relation_task.nomer = str(new_task.number)
            else:
                relation_task.nomer += ", " + str(new_task.number)

            relation_task.save()
        except Exception, e:
            #print Exception, e
            pass

        new_task.save()

        action = Action.create("create", request.user, new_task)

        if new_task.author.get_profile().send_email:
            try:
                send_task_mail(new_task, u"Создано поручение")
            except:
                pass
        return True
    else:
        messages.error(request, task_form.errors)
        return False


def save_history_time(request):
    attention_time = request.POST.get('time')
    AttentionOnline.objects.create(user=request.user, time=datetime.datetime.now(), time_in_history=int(attention_time))
    return HttpResponse(request.user)


@login_required
def show_task(request):
    get_user_task = request.GET.get('get_user') or request.user
    task_form = TaskModelForm
    if not request.GET.get('obj_id') is None:
        task = Task.objects.get(id=request.GET.get('obj_id'))

        # Запись действия о посещении задачи
        action = Action.create("visit", request.user, task)

        template = "project/show_task.html"
        return direct_to_template(request, template, {
            'task_form': task_form(instance=task, request = request, prefix='edit'),
            'task': task,
        })
    if request.POST:
        task = Task.objects.get(id=request.POST.get('edit-object_id'))
        str = request.POST['edit-comment']
        request.POST['edit-comment'] = request.POST['edit-comment'].replace('\n', '').replace('\r\n', '')
        task_form = task_form(request.POST, request=request, instance=task, prefix='edit')
        if task_form.is_valid():
            edit = task_form.save(commit=False)
            if request.user.id == task.author_task.id:
                if edit.date_add > edit.date_end:
                    messages.error(request, u"Данные не сохранены. Вы указали неправильно даты")
                    return HttpResponseRedirect('/')
                if len(edit.comment) > 15000 or len(edit.text) > 15000:
                    messages.error(request, u"Данные не сохранены. В полях Подробности и Комментарий действует ограничение в 15000 символов.")
                    return HttpResponseRedirect('/')
                f = FilesItem.objects.filter(id__in=request.POST.getlist('files')) or None
                if f:
                    edit.files = f
                    for i in f:
                        i.task = Task.objects.get(id=task.id)
                        i.save()
                edit.number = task.number
                #edit.performer = request.user
                edit.author_task = task.author_task
                edit.hash_task = task.hash_task
                edit.save()
                if edit.author.get_profile().send_email:
                    send_task_mail(edit, get_mail_title(edit))
            else:
                task.evaluation = request.POST.get('edit-evaluation')
                task.save()

                # Запись действия по изменении задачи
                action = Action.create("change", request.user, task)

                if edit.author.get_profile().send_email:
                    send_task_mail(edit, get_mail_title(task))
            messages.success(request, u'Поручение сохранено')
            if not request.user == task.author_task:
                return HttpResponseRedirect('/?get_user=%s' % task.author_task.id)
        else:
            messages.error(request, task_form.errors)
    return HttpResponseRedirect('/')


@login_required
@require_http_methods(['POST'])
def action_task(request):
    import re

    get = request.GET

    # форма выбора периода дат
    date_start = datetime.datetime(1900, 1, 1)
    date_end   = datetime.datetime(2900, 1, 1)
    filters_form = HistoryFilters(initial = {
        'period': 'all_time',
    })
    if request.GET.get('period', None):
        filters_form = HistoryFilters(request.GET)
        if filters_form.is_valid():
            date_start = filters_form.start
            date_end   = filters_form.end

    list_id = []
    for i in request.POST:
        if re.search('checkbox_', i):
            list_id.append(re.sub('checkbox_', '', i))
    tasks = Task.objects.filter(id__in=list_id)
    if not request.POST.get('delete') is None:
        if len(tasks) > 1:
            return direct_to_template(request, 'project/formset_deleted.html', {
                'task': tasks,
                'is_test': is_test(),
            })
        else:
            for t in tasks:
                t.deleted = 1
                t.save()
                messages.success(request, u'Поручение удалено.')
                return HttpResponseRedirect('/')
    if not request.POST.get('exportxls') is None:
        if list_id == []:
            messages.warning(request, u'Выберите поручения.')
            return HttpResponseRedirect('/')
        else:
            return export_as_xls(request, tasks)
    if not request.POST.get('exportxlsfinished') is None:
        # if not request.user.is_superuser:
        #     users = Task.objects.filter(
        #         author_task=request.user,
        #         deleted=False,
        #         progress=100,
        #     ).filter(
                # date_add__lte =  date_end,
                # date_end__gte =  date_start,
        #     ).order_by('author_task__last_name', 'author_task__first_name')
        #     assert False
        # else:
        unique_tasks_ids = Task.objects.filter(
            date_add__lte =  date_end,
            date_end__gte =  date_start,
        ).filter(
            author_task = request.user.id,
            deleted=False,
            progress=100,
        ).values('hash_task').annotate(max_id = Max('id'))
        ids = [m['max_id'] for m in unique_tasks_ids]

        tasks = Task.objects.filter(id__in = ids).order_by('author_task__last_name', 'author_task__first_name')
        # users = Task.objects.filter(
        #     author_task=request.user,
        #     progress=100,
        # ).filter(
                # date_add__lte =  date_end,
                # date_end__gte =  date_start,
        # ).order_by('author_task__last_name', 'author_task__first_name')

        wb = xlwt.Workbook()
        ws0 = wb.add_sheet('0', cell_overwrite_ok=True)
        col = 0
        borders = xlwt.Borders()
        borders.left = xlwt.Borders.DASHED
        borders.right = xlwt.Borders.DASHED
        borders.top = xlwt.Borders.DASHED
        borders.bottom = xlwt.Borders.DASHED
        borders.left_colour = 0x40
        borders.right_colour = 0x40
        borders.top_colour = 0x40
        borders.bottom_colour = 0x40
        style = xlwt.XFStyle() # Create Style
        style.borders = borders # Add Borders to Style

        ws0.write(0, col, u"Сотрудник", style)
        ws0.write(0, col + 1, u"Должность", style)
        ws0.write(0, col + 2, u"Отдел", style)
        ws0.write(0, col + 3, u"Кем дано", style)
        ws0.write(0, col + 4, u"Название поручения", style)
        ws0.write(0, col + 5, u"Дата поручения", style)
        ws0.write(0, col + 6, u"Срок исполнения", style)
        ws0.write(0, col + 7, u"Процент выполнения", style)
        ws0.col(0).width = 5000
        ws0.col(1).width = 7000
        ws0.col(2).width = 7000
        ws0.col(3).width = 9000
        ws0.col(4).width = 22000
        ws0.col(5).width = 6000
        ws0.col(6).width = 4000
        ws0.col(7).width = 7000
        col = 0
        row = 1
        for obj in tasks:
            ws0.write(row, col, unicode(obj.author_task.get_profile().get_names()), style)
            ws0.write(row, col + 1, unicode(obj.author_task.get_profile().job or _(u'Отсутствует')), style)
            ws0.write(row, col + 2, unicode(_(u'Отсутствует')), style)
            if obj.author:
                ws0.write(row, col + 3, unicode(obj.author.get_profile().get_names()), style)
            else:
                ws0.write(row, col + 3, unicode(_(u'Отсутствует')), style)
            ws0.write(row, col + 4, unicode(obj.title), style)
            ws0.write(row, col + 5, unicode(obj.date_add), style)
            ws0.write(row, col + 6, unicode(obj.date_end), style)
            ws0.write(row, col + 7, unicode(obj.get_progress()), style)
            row = row + 1
        fd, fn = tempfile.mkstemp()
        os.close(fd)
        wb.save(fn)
        fh = open(fn, 'rb')
        resp = fh.read()
        fh.close()
        response = HttpResponse(resp, mimetype='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename=finished.xls'
        return response
    return HttpResponseRedirect('/')


@login_required
@require_http_methods(['POST'])
def delete_task(request):
    list_id = request.POST.getlist('task')
    for t in Task.objects.filter(id__in=list_id):
        #print t
        t.deleted = 1
        t.save()
        messages.success(request, u'Поручение удалено.')
        action = Action.create("delete", request.user, t)
    return HttpResponseRedirect('/')


@login_required
@require_http_methods(['POST'])
def quick_edit(request):
    if not request.POST.get('name') is None and not request.POST.get('value') is None:
        value = request.POST.get('value')
        get_task = Task.objects.get(id=request.POST.get('object_id'))
        get_task.__setattr__(request.POST.get('name'), value)
        get_task.save()
        if get_task.author.get_profile().send_email:
            send_task_mail(get_task, get_mail_title(get_task))
    return HttpResponse('Ok')


@login_required
def history(request, user_id = None, is_standalone=0):
    get_params_string = ''
    for key, value in request.GET.iteritems():
        get_params_string += '{0}={1}&'.format(key, value)

    # форма выбора периода дат
    today = datetime.datetime.today().date()
    date_start = today
    date_end   = today
    filters_form = HistoryFilters(initial = {
        'period': 'today',
    })
    if request.GET.get('period', None):
        filters_form = HistoryFilters(request.GET)
        if filters_form.is_valid():
            date_start = filters_form.start
            date_end   = filters_form.end


    # если id пользователя в адресе отсутствует, то присваиваем id текущего пользователя
    if not user_id:
        user_id = request.user.id
    user = User.objects.get(pk = user_id)

    # пользователь, дашбоард которого смотрим
    request.viewed_user = user
    # урл без указания user_id просматриваемого пользователя
    # используется для редиректа при выборе пользователя из низпадающего списка в главном меню
    request.base_url_without_user_id = 'history'

    # user_id = request.GET.get('get_user')
    tree_users_list = []
    debug_info_list = []
    is_need_count_time = False
    request_user = UserProfile.objects.get(user__id=request.user.id)
    debug_info_list.append(request_user)

    if user_id and request_user.role == 1:
        user_profile = UserProfile.objects.get(user__id=user_id)
        debug_info_list.append(user_profile)
        try:
            if request_user.tree.is_leaf_node():
                debug_info_list.append("tree_users_list1")

                tree_users_list = UserProfile.objects.filter(
                    tree=request_user.tree).exclude(id=request_user.id)
                debug_info_list.append(tree_users_list)

            else:
                tree_users_list = UserProfile.objects.filter(
                    tree__in=[i.id for i in request_user.tree.get_descendants(include_self=True)]).exclude(
                    id=request_user.id)
                tree_users_list.remove(request_user)
                debug_info_list.append("tree_users_list")
                debug_info_list.append(tree_users_list)

        except Exception, e:
            debug_info_list.append("error")

            debug_info_list.append(str(e))
            #tree_users_list = UserProfile.objects.filter(id=request_user.userid).select_related()

        if user_profile in tree_users_list:
            #need count time
            is_need_count_time = True
            debug_info_list.append("is_need_count_time")

            debug_info_list.append(is_need_count_time)

    # таким сложным образом можно получить поручения последней версии изменения,
    # так как благодаря схеме с layer поручения каждый день клонируются,
    # но клонируются лишь незавершенные поручения.
    if not request.GET.get('period', None) or request.GET.get('period', None) == 'today':
        unique_tasks_ids = Task.objects.filter(
            layer = today,
            author_task = user_id,
        ).values('hash_task').annotate(max_id = Max('id'))
    else:
        unique_tasks_ids = Task.objects.filter(
            Q(date_add__lte = date_end,
            date_end__gte = date_start,) |
            ~Q(progress = 100),
        ).filter(
            author_task = user_id,
        ).values('hash_task').annotate(max_id = Max('id'))

    ids = [m['max_id'] for m in unique_tasks_ids]
    tasks = Task.objects.filter(id__in = ids).order_by('date_add')

    template = "project/history-view.html"
    dict = {
        'today': today,
        'project': tasks,
        'is_test': is_test(),
        # 'tree_users_list': request_user,
        'is_need_count_time': is_need_count_time,
        'tree_users_list': debug_info_list,
        'is_standalone': is_standalone,
        'filters_form' : filters_form,
        'user_id' : user_id,
        'get_params_string' : get_params_string,
    }
    if is_standalone:
        dict['get_user'] = user_id
        dict['obj_id'] = request.GET.get('obj_id')
    return direct_to_template(request, template, dict)

@login_required
def my_history(request, is_standalone = 0):
    return history(request, None, is_standalone)

@login_required
def user_history(request, user_id, is_standalone = 0):
    return history(request, user_id, is_standalone)

@require_http_methods(['GET'])
def history_list(request):
    get_user_task = request.GET.get('get_user') or request.user.id
    if not request.GET.get('date') is None:
        return direct_to_template(request, 'project/history-list.html', {
            'today': datetime.datetime.date(datetime.datetime.now()),
            'project': Task.objects.filter(layer=request.GET.get('date'), author_task=get_user_task).order_by('date_add'),
        })
    return Http404


@login_required
def history_task_view(request):
    #     Выбранная задача
    task = Task.objects.get(id=request.GET.get('obj_id'))

    # Действие о просмотре задачи
    action = Action.create("visit", request.user, task)

    # Если пользователь есть потсавщиком этой задачи, ставим галку о просмотре задачи.
    if request.user == task.author:
        task.author_look = True
        task.save()

    return render_to_response("project/history-task-view.html", {
        'task': task,
    })


@login_required
@csrf_exempt
def history_object(request, hash_task):
    assert False

    get_user_task = request.GET.get('get_user') or request.user.id
    return direct_to_template(request, 'project/history-object.html', {
        'project': Task.objects.filter(author_task=get_user_task, hash_task=hash_task),
        'is_test': is_test(),
    })


@login_required
def reports(request):
    return direct_to_template(request, 'project/report.html', {
        'is_test': is_test(),
    })


def get_active_users():
    return User.objects.select_related().filter(is_active=True).order_by('last_name')


def user_is_visible(user):
    res = 1
    try:
        profile = UserProfile.objects.select_related().get(user=user)
        if profile.dismissed or not profile.tree:
            res = 0
    except:
        res = 0
    return res


def get_visible_users(users):
    result = users
    result = result.exclude(Q(userprofile__dismissed=True) | Q(userprofile__tree=None))
    return result


def get_visible_user_profiles():
    return UserProfile.objects.exclude(dismissed=1).exclude(tree__isnull=True).filter(user__is_active=True)


def get_allowed_visible_users(request):
    if not request.user.is_superuser:
        users0 = get_active_users().filter(id__in=[e.user.id for e in get_access_user(request)]).order_by('last_name')
    else:
        users0 = get_active_users()
    users = get_visible_users(users0)
    return users

@login_required
def report_stat(request):
    all_task_count = 0
    all_task_opened = 0
    all_task_closed = 0
    all_task_deleted = 0
    all_online = 0
    all_percent = 0
    users = get_allowed_visible_users(request)
    # new_list = []
    #
    #
    #
    # for u in users:
    #     u.task_opened = Task.objects.filter(author_task = u, layer=(datetime.datetime.date(datetime.datetime.now())), deleted = False).exclude(progress = 100).values_list('hash_task').distinct().count()
    #     u.task_closed = Task.objects.filter(progress = 100, author_task = u).values_list('hash_task').distinct().count()
    #     u.task_deleted = Task.objects.filter(author_task = u, deleted = True).values_list('hash_task').distinct().count()
    #     u.history = Task.objects.filter(progress = 100).values_list('hash_task').distinct().count()
    #     u.online = UsersOnline.objects.filter(user = u).distinct().count()
    #     #u.today_online = UsersOnline.objects.filter(user = u, time__day = int(datetime.datetime.date(datetime.datetime.now()).day)).count()
    #     today = datetime.datetime.today()
    #     u.today_online = UsersOnline.objects.filter(user = u, time__range = [datetime.datetime.combine(today, datetime.time.min), datetime.datetime.combine(today, datetime.time.max)]).count()
    #     u.task_count = u.task_opened + u.task_closed + u.task_deleted
    #     new_list.append(u)
    #     all_task_count += u.task_count
    #     all_task_opened += u.task_opened
    #     all_task_closed += u.task_closed
    #     all_task_deleted += u.task_deleted
    #     all_online += u.online
    #     if u.task_closed:
    #         u.percent = int( float(u.task_closed) * 100 / (int(u.task_opened) + int(u.task_closed)))
    #     else:
    #         u.percent = 0

    date_start = datetime.datetime.now()
    date_end   = date_start
    filters_form = UsersStatFilters(initial = {
        'period': 'today',
        })
    if request.GET.get('period', None):
        filters_form = UsersStatFilters(request.GET)
        if filters_form.is_valid():
            date_start = filters_form.start
            date_end   = filters_form.end

    node = TreeDepartment.objects.all()
    for tree in node:
        tree.task_count = 0
        tree.task_opened = 0
        tree.task_closed = 0
        tree.task_deleted = 0
        tree.online = 0
        tree.percent = 0

        tree.users = []
        for u in tree.get_user_profiles():
            u.task_opened = Task.objects.filter(author_task = u.user, layer__range=(date_start, date_end), deleted = False).exclude(progress = 100).values_list('hash_task').distinct().count()
            u.task_closed = Task.objects.filter(progress = 100, author_task = u.user, layer__range=(date_start, date_end)).values_list('hash_task').distinct().count()
            u.task_deleted = Task.objects.filter(author_task = u.user, deleted = True, layer__range=(date_start, date_end)).values_list('hash_task').distinct().count()
            u.history = Task.objects.filter(progress = 100).values_list('hash_task').distinct().count()
            u.online = UsersOnline.objects.filter(user = u.user).distinct().count()
            today = datetime.datetime.today()
            u.today_online = UsersOnline.objects.filter(user = u.user, time__range = [datetime.datetime.combine(today, datetime.time.min), datetime.datetime.combine(today, datetime.time.max)]).count()
            u.task_count = u.task_opened + u.task_closed + u.task_deleted

            tree.task_count += u.task_count
            tree.task_opened += u.task_opened
            tree.task_closed += u.task_closed
            tree.task_deleted += u.task_deleted
            tree.online += u.online
            # tree.all_percent += 0

            all_task_count += u.task_count
            all_task_opened += u.task_opened
            all_task_closed += u.task_closed
            all_task_deleted += u.task_deleted
            all_online += u.online

            if u.task_closed:
                u.percent = int( float(u.task_closed) * 100 / (int(u.task_opened) + int(u.task_closed)))
            else:
                u.percent = 0
            tree.users.append(u)

    # строка GET-параметров для url-ов
    get_params_string = '&'.join( ['{0}={1}'.format(k, v) for k, v in request.GET.iteritems()] )
    if get_params_string:
        get_params_string = '?{0}&'.format(get_params_string)
    # else:
    #     get_params_string = '?'

    return direct_to_template(request, 'project/stat.html', {
        'filters_form': filters_form,
        'node': node,
        'stat': Task.objects.filter().values_list('hash_task').distinct().count(),
        'all_task_count': all_task_count,
        'all_task_closed': all_task_closed,
        'all_task_opened': all_task_opened,
        'all_task_deleted': all_task_deleted,
        'all_online': all_online,
        'is_test': is_test(),
        'get_params' : get_params_string,
        })

@login_required
def report_stat_opened(request):
    date_start = datetime.datetime.now()
    date_end = date_start
    filters_form = UsersStatFilters(initial={
        'period': 'today',
    })
    # @TODO 5227 раскомментить
    # if request.GET:
    #     filters_form = UsersStatFilters(request.GET)
    #     if filters_form.is_valid():
    #         date_start = filters_form.start
    #         date_end = filters_form.end
    results = get_tree_with_task_statistics(request, date_start, date_end)

    return direct_to_template(request, 'project/stat_opened.html', {
        'filters_form'            : filters_form,
        'node'                    : results['full_tree'],
        'stat'                    : Task.objects.filter().values_list('hash_task').distinct().count(),
        'all_tasks_count'         : results['all_tasks_count'],
        'all_tasks_closed_count'  : results['all_tasks_closed_count'],
        'all_tasks_opened_count'  : results['all_tasks_opened_count'],
        'all_tasks_deleted_count' : results['all_tasks_deleted_count'],
        'all_online'              : results['all_online'],
        'is_test'                 : is_test(),
    })


@login_required
def report_employees(request):
    if not request.user.is_superuser:
        users0 = get_active_users().filter(id__in=[e.user.id for e in get_access_user(request)], groups__id=1)
    else:
        users0 = get_active_users().filter(groups__id=1)
    users = get_visible_users(users0)
    for u in users:
        u.task_opened = Task.objects.filter(author_task=u, layer=(datetime.datetime.date(datetime.datetime.now())), deleted=False).exclude(progress=100).values_list(
            'hash_task').distinct().count()
        u.task_closed = Task.objects.filter(progress=100, author_task=u).values_list('hash_task').distinct().count()
        u.task_deleted = Task.objects.filter(author_task=u, deleted=True).values_list('hash_task').distinct().count()
        u.history = Task.objects.filter(progress=100).values_list('hash_task').distinct().count()
        u.online = UsersOnline.objects.filter(user=u).distinct().count()
        u.today_online = UsersOnline.objects.filter(user=u, time__day=int(datetime.datetime.date(datetime.datetime.now()).day)).count()
        u.task_count = u.task_opened + u.task_closed + u.task_deleted
        if u.task_closed:
            u.percent = int(float(u.task_closed) * 100 / (int(u.task_opened) + int(u.task_closed)))
        else:
            u.percent = 0
    wb = xlwt.Workbook()
    ws0 = wb.add_sheet('0', cell_overwrite_ok=True)
    col = 0
    borders = xlwt.Borders()
    borders.left = xlwt.Borders.DASHED
    borders.right = xlwt.Borders.DASHED
    borders.top = xlwt.Borders.DASHED
    borders.bottom = xlwt.Borders.DASHED
    borders.left_colour = 0x40
    borders.right_colour = 0x40
    borders.top_colour = 0x40
    borders.bottom_colour = 0x40
    style = xlwt.XFStyle() # Create Style
    style.borders = borders # Add Borders to Style

    ws0.write(0, col, u"ID", style)
    ws0.write(0, col + 1, u"Сотрудник", style)
    ws0.write(0, col + 2, u"Должность", style)
    ws0.write(0, col + 3, u"Отдел", style)
    ws0.write(0, col + 4, u"Кол-во поручений (общее)", style)
    ws0.write(0, col + 5, u"Кол-во открытых поручений", style)
    ws0.write(0, col + 6, u"Кол-во закрытых поручений", style)
    ws0.write(0, col + 7, u"Кол-во удаленных поручений", style)
    ws0.write(0, col + 8, u"Средний % выполнения", style)
    ws0.write(0, col + 9, u"Время в онлайне (минут)", style)
    ws0.write(0, col + 10, u"Сегодня в онлайне", style)
    ws0.col(0).width = 2000
    ws0.col(1).width = 3000
    ws0.col(2).width = 9000
    ws0.col(3).width = 10000
    ws0.col(4).width = 6000
    ws0.col(5).width = 4000
    ws0.col(6).width = 4000
    ws0.col(7).width = 4000
    ws0.col(8).width = 4000
    ws0.col(9).width = 4000
    ws0.col(10).width = 4000
    col = 0
    row = 1

    for obj in users:
        ws0.write(row, col, obj.id, style)
        ws0.write(row, col + 1, obj.get_profile().get_names(), style)
        ws0.write(row, col + 2, unicode(obj.get_profile().job or _(u'Отсутствует')), style)
        ws0.write(row, col + 3, unicode(_(u'Отсутствует')), style)
        ws0.write(row, col + 4, unicode(obj.task_count or _(u'Отсутствует')), style)
        ws0.write(row, col + 5, unicode(obj.task_opened), style)
        ws0.write(row, col + 6, unicode(obj.task_closed), style)
        ws0.write(row, col + 7, unicode(obj.task_deleted), style)
        ws0.write(row, col + 8, unicode(obj.percent or _(u'Отсутствует')), style)
        ws0.write(row, col + 9, unicode(obj.online), style)
        ws0.write(row, col + 10, unicode(obj.today_online), style)
        row = row + 1

    fd, fn = tempfile.mkstemp()
    os.close(fd)
    wb.save(fn)
    fh = open(fn, 'rb')
    resp = fh.read()
    fh.close()
    response = HttpResponse(resp, mimetype='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=stats.xls'
    return response


def get_open_tasks(request):
    if not request.user.is_superuser:
        tasks = Task.layer_objects.filter(author_task__in=[e.user.id for e in get_access_user(request)], deleted=False).order_by('author_task__last_name',
                                                                                                                                 'author_task__first_name') # .exclude(progress =
        # 100)
    else:
        tasks = Task.layer_objects.filter(deleted=False).order_by('author_task__last_name', 'author_task__first_name')
    return tasks


def get_open_tasks_by_user(request):
    tasks = get_open_tasks(request)
    res = {}
    for task in tasks:
        key = task.author_task.id
        if not key in res:
            res[key] = []
        res[key].append(task)
    return res


@login_required
def report_open_tasks(request):
    tasks0 = get_open_tasks(request)
    tasks = []
    for task in tasks0:
        if user_is_visible(task.author_task):
            tasks.append(task)
    wb = xlwt.Workbook()
    ws0 = wb.add_sheet('0', cell_overwrite_ok=True)
    col = 0
    borders = xlwt.Borders()
    borders.left = xlwt.Borders.DASHED
    borders.right = xlwt.Borders.DASHED
    borders.top = xlwt.Borders.DASHED
    borders.bottom = xlwt.Borders.DASHED
    borders.left_colour = 0x40
    borders.right_colour = 0x40
    borders.top_colour = 0x40
    borders.bottom_colour = 0x40
    style = xlwt.XFStyle() # Create Style
    style.borders = borders # Add Borders to Style

    ws0.write(0, col, u"Сотрудник", style)
    ws0.write(0, col + 1, u"Должность", style)
    ws0.write(0, col + 2, u"Отдел", style)
    ws0.write(0, col + 3, u"Кем дано", style)
    ws0.write(0, col + 4, u"Название поручения", style)
    ws0.write(0, col + 5, u"Дата поручения", style)
    ws0.write(0, col + 6, u"Срок исполнения", style)
    ws0.write(0, col + 7, u"Процент выполнения", style)
    ws0.col(0).width = 5000
    ws0.col(1).width = 7000
    ws0.col(2).width = 7000
    ws0.col(3).width = 9000
    ws0.col(4).width = 22000
    ws0.col(5).width = 6000
    ws0.col(6).width = 4000
    ws0.col(7).width = 7000
    col = 0
    row = 1
    for obj in tasks:
        ws0.write(row, col, unicode(obj.author_task.get_profile().get_names()), style)
        ws0.write(row, col + 1, unicode(obj.author_task.get_profile().job or _(u'Отсутствует')), style)
        ws0.write(row, col + 2, unicode(_(u'Отсутствует')), style)
        if obj.author:
            ws0.write(row, col + 3, unicode(obj.author.get_profile().get_names()), style)
        else:
            ws0.write(row, col + 3, unicode(_(u'Отсутствует')), style)
        ws0.write(row, col + 4, unicode(obj.title), style)
        ws0.write(row, col + 5, unicode(obj.date_add), style)
        ws0.write(row, col + 6, unicode(obj.date_end), style)
        ws0.write(row, col + 7, unicode(obj.get_progress()), style)
        row = row + 1
    fd, fn = tempfile.mkstemp()
    os.close(fd)
    wb.save(fn)
    fh = open(fn, 'rb')
    resp = fh.read()
    fh.close()
    response = HttpResponse(resp, mimetype='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=stat.xls'
    return response


@login_required
def report_print_open_tasks(request):
    tasks0 = get_open_tasks(request)
    tasks = []
    for task in tasks0:
        user = User.objects.get(id=task.author_task_id)
        if user_is_visible(user):
            tasks.append(task)
    return direct_to_template(request, 'project/print_open_task.html', {
        'query': tasks,
    })


@login_required
def graph(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    chart = {}
    get_user = UserProfile.objects.get(user__id=request.GET.get('otd') or 26)
    chart['title'] = u'Общее число поручений сотрудника'
    chart['desc'] = u'Определяем объем работы сотрудника в текущем периоде'
    chart['db'] = get_user.get_graph_task_for_user(year=tab_year)
    c = []
    a2 = 0
    for i in chart['db']:
        if i > '0':
            a2 += int(i)
    chart['count'] = a2

    if request.GET.get('type'):
        if request.GET.get('type') == '0':
            chart['title'] = u'Общее число поручений сотрудника'
            chart['desc'] = u'Определяем объем работы сотрудника в текущем периоде'
            chart['db'] = get_user.get_graph_task_for_user(year=tab_year)
            a2 = 0
            for i in chart['db']:
                a2 += int(i)
            chart['count'] = a2

        if request.GET.get('type') == '1':
            chart['title'] = u'Средний график выполнения поручения сотрудником'
            chart['desc'] = u'График изменения усредненного процентного состояния поручения у сотрудника'
            chart['db'] = get_user.get_graph_task_for_user(year=tab_year, avg=True)
            chart['count'] = ''

        if request.GET.get('type') == '3':
            chart['title'] = u'Несвоевременное закрытие поручений руководителем'
            chart['desc'] = u'Общий характер взаимодействия и согласованности действий руководителя и подчиненного при завершении выполнения поручения'
            chart['db'] = get_user.get_graph_task_for_user(year=tab_year, and_where=[('progress__lt', 100), ('date_end__gte', datetime.datetime.now().date())])
            a2 = 0
            for i in chart['db']:
                a2 += int(i)
            chart['count'] = a2
        if request.GET.get('type') == '4':
            chart['title'] = u'Число выполненных поручений из общего числа поручений'
            chart['desc'] = u'Степень выполнения сотрудником поставленных ему поручений'
            chart['db'] = get_user.get_graph_task_for_user(year=tab_year, and_where=[('progress', 100)])
            c = []
            a2 = 0
            for i in chart['db']:
                a2 += int(i)
            chart['count'] = a2
        if request.GET.get('type') == '5':
            chart['title'] = u'Число невыполненных поручений из общего числа поручений'
            chart['desc'] = u'Степень невыполнения сотрудником поставленных ему поручений'
            chart['db'] = get_user.get_graph_task_for_user(year=tab_year, and_where=[('progress__lte', 100)])
            c = []
            a2 = 0
            for i in chart['db']:
                a2 += int(i)
            chart['count'] = a2
        if request.GET.get('type') == '6':
            chart['title'] = u'Число отмененных поручений из общего числа поручений'
            chart['desc'] = u'Cтепень холостого хода и неэффективного использования рабочего времени сотрудника'
            chart['db'] = get_user.get_graph_task_for_user(year=tab_year, and_where=[('terminated', True)])
            c = []
            a2 = 0
            for i in chart['db']:
                if int(i) > int('0'):
                    a2 += int(i)
            chart['count'] = a2
        if request.GET.get('type') == '7':
            chart['title'] = u'Число отмененных поручений при определенном проценте их исполнения'
            chart['desc'] = u'Уровень рассогласования действий исполнителя и руководителя и степень неэффективности принятия управленческих решений'
            chart['arr'] = {}
            chart['arr']['a1'] = get_user.get_graph_task_for_user(year=tab_year, and_where=[('progress__lt', 30)])
            chart['arr']['a2'] = get_user.get_graph_task_for_user(year=tab_year, and_where=[('progress__gte', 30), ('progress__lte', 60)])
            chart['arr']['a3'] = get_user.get_graph_task_for_user(year=tab_year, and_where=[('progress__gte', 60)])
            c = []
            a2 = 0
            for i in (chart['arr']['a1'] + chart['arr']['a2'] + chart['arr']['a3']):
                if int(i) > int('0'):
                    a2 += int(i)
            chart['count'] = a2
    return direct_to_template(request, 'project/graph.html', {
        'values': '',
        'year': int(tab_year),
        'chart': chart,
        'otd': get_visible_user_profiles(), # UserProfile.objects.all()
        'get': request.GET,
        'uss': get_user,
        'is_test': is_test(),
    })


@login_required
def graph_1(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_graph_1'):
            raise PermissionDenied
    chart = {}
    colors_line = g_colors_line
    out = {
        'values': '',
        'otd': UserProfile.staff_objects.all(),
        'get': request.GET,
        'colors_line': colors_line,
        'is_test': is_test(),
    }
    if request.GET.get('otd'):
        get_user = UserProfile.objects.get(user__id=request.GET.get('otd'))
        chart['title'] = u'Средний график выполнения поручения сотрудником'
        chart['desc'] = u'График изменения усредненного процентного состояния поручения у сотрудника'
        chart['line'], chart['db'] = get_user.get_graph_AVG_task_for_user()
        chart['count'] = ''
        out = {
            'values': '',
            'chart': chart,
            'otd': UserProfile.staff_objects.all(),
            'get': request.GET,
            'uss': get_user,
            'colors_line': colors_line,
            'is_test': is_test(),
        }
    out.update(font_dict(request.GET.get('otd')))
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/graph/1.html', out)
    else:
        return direct_to_template(request, 'project/graph/1_print.html', out)


@login_required
def graph_2(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_graph_2'):
            raise PermissionDenied
    chart = {}
    if is_test():
        out = {
            'values': '',
            'otd': UserProfile.staff_objects.all(),
            'get': request.GET,
            'is_test': is_test(),
        }
        if request.GET.get('otd'):
            get_user = UserProfile.objects.get(user__id=request.GET.get('otd'))
            chart['title'] = u'Количество открытых поручений на каждый день'
            chart['desc'] = u'График открытых поручений на каждый день у сотрудника'
            task_dict = get_user.get_open_task_on_everyday()
            # chart['line'] , chart['db'] =  get_user.get_graph_AVG_task_for_user()
            chart['count'] = ''
            out = {
                'values': '',
                'chart': chart,
                'otd': UserProfile.staff_objects.all(),
                'get': request.GET,
                'uss': get_user,
                'dict': sorted(task_dict.iteritems()),
                'is_test': is_test(),
            }
    else:
        out = {
            'values': '',
            'get': request.GET,
            'is_test': is_test(),
        }
        node = TreeDepartment.objects.filter(level=0)[0]
        chart['title'] = u'Динамика количества поручений в организации'
        chart['desc'] = u'Общее количество поручений в организации по месяцам.'
        chart['line'], chart['db'] = node.get_graph_month_for_user()
        chart['count'] = ''
        out = {
            'values': '',
            'chart': chart,
            'get': request.GET,
            'is_test': is_test(),
        }
    return direct_to_template(request, 'project/graph/2.html', out)


@login_required
def query_tab_0(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_0'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    node = TreeDepartment.objects.all()
    for tree in node:
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        tree.average = float(tree.task_count) / (len(tree.get_all_tree_users()) or 1)
        tree_user_task = tree.get_task_count_tree(tab_month, tab_year)
        tree.min_tree_task = min(tree_user_task)
        tree.max_tree_task = max(tree_user_task)
        try:
            tree.pro_task = float(tree.task_count) / tree.get_all_task_tree(tab_month, tab_year) * 100
        except:
            tree.pro_task = 0
        tree.users = []
        for user in tree.get_user_profiles():
            user.task_count = user.get_task_count(tab_month, tab_year)
            user.avg_task = user.get_task_count_avg(tab_month, tab_year)
            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/0.html', query_template)
    else:
        return direct_to_template(request, 'project/query/0_print.html', query_template)


@staff_member_required
def graph_3(request):
    chart = {}
    year = ''
    task_list = ''
    if request.GET.get('year'):
        year = int(request.GET.get('year'))
        task_list = Task().get_all_tasks_for_employees(year)
        chart['title'] = u'Динамика количества поручений по сотрудникам'
        chart['desc'] = u'тестовое задание, Кириллов Е.'
    out = {
        'chart': chart,
        'line': ['Сотрудники', ],
        'all_tasks': task_list,
        'year': year
    }
    return direct_to_template(request, 'project/query/10.html', out)


@login_required
def graph_4(request):
    if not request.user.is_superuser:
        if not request.user.has_perm("accounts.view_query_9"):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    node = TreeDepartment.objects.select_related()
    logging.debug('node {0}'.format(node[0].get_descendants()))
    chart2_department_list = ['01.01', '01.02', '01.03', '02.01', '03.01', '03.02', '04', '05', '06', '07', '08', '09']
    pie_list = []
    column_list = []
    chart = {}
    chart2 = {}
    all_task_in_root = node[0].get_root().get_task_count(int(tab_month), int(tab_year))
    if all_task_in_root:
        chart['title'] = 'График распределения нагрузки сотрудников и подразделений'
        for tree in node:
            if tree.parent == node[0]:
                pie_list.append((
                    tree.title, float(tree.get_task_count(int(tab_month), int(tab_year))) / all_task_in_root * 100))
            if tree.nums in chart2_department_list:
                column_list.append((tree.title, tree.get_task_count(int(tab_month), int(tab_year))))
        logging.debug(pie_list)
        chart2['data'] = column_list
        chart2['title'] = 'График распределения нагрузки сотрудников и подразделений'
        logging.debug(len(column_list))
    out = {
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'year': int(tab_year),
        'month': int(tab_month),
        'pie_list': pie_list,
        'chart': chart,
        'chart2': chart2,
    }
    return direct_to_template(request, 'project/graph/9.html', out)


@login_required
def graph_5(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_9'):
            raise PermissionDenied
    chart = {}

    year = request.GET.get('year') or str(datetime.datetime.now().year)

    task_list = Task().get_tasks_by_month_list()
    logging.debug(task_list)
    line = [year]
    if filter(m_filter, task_list):
        chart['title'] = u'Динамика количества поручений в организации'
        chart['desc'] = u'График изменения количества поручений в организации'

    out = {
        'chart': chart,
        'line': line,
        'all_tasks': list(reversed(task_list)),
        'year': year,
    }
    return direct_to_template(request, 'project/graph/5.html', out)


def get_first_day_of_month(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m - 1, 12)
    return date(y + a, m + 1, 1)


def get_last_day_of_month(dt):
    return get_first_day_of_month(dt, 0, 1) + datetime.timedelta(-1)


def get_period_start_date(period):
    if period == "current_month":
        #tab_month = datetime.datetime.now().month
        #tab_year = datetime.datetime.now().year
        #and_where = [(field_name + '__year', int(tab_year)), (field_name + '__month', int(tab_month))]
        start_date = get_first_day_of_month(datetime.date.today())
    else:
        if period == "week":
            days = 7
        elif period == "month":
            days = 31
        elif period == "quarter":
            days = 100
        elif period == "year":
            days = 365
        else:
            raise Exception("unknown period " + str(period))
        start_date = datetime.datetime.now() - datetime.timedelta(days=days)
    return start_date


def get_and_where(period, field_name):
    start_date = get_period_start_date(period)
    and_where = [(field_name + '__range', (start_date, datetime.datetime.now()))]
    return and_where


@login_required
def query_load_distribution(request, tree_id=False, query_name="query_load_distribution"):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_0'):
            raise PermissionDenied
    display_type = request.GET.get('type') or "table"
    period = request.GET.get('period') or "current_month"
    if period != request.session.get(query_name + '__' + 'period', False):
        request.session[query_name + '__' + 'node'] = False
    request.session[query_name + '__' + 'period'] = period
    and_where = get_and_where(period, "layer")
    logging.debug('period {0}'.format(period))
    logging.debug('and_where {0}'.format(and_where))
    node = TreeDepartment.objects.all()
    open_tasks_by_user = get_open_tasks_by_user(request)
    dbg = "" ## str(open_tasks_by_user)
    colors0 = g_colors
    #colors0 = ['#FF0000', '#36c3f7', '#c111be', '#23a323', '#000DFF', '#B7FEFF', '#f99a1d', '#FFFF00', '#FFA0FE', '#A0FFA0', '#878DFE', '#FFFFB2', '#870000', '#007D81',
    # '#81007F', '#008400', '#000787', '#7F7F00']
    colors_by_tree_id = {
        1: '#7F7F00',
        2: colors0[0], 11: colors0[1], 13: colors0[2], 16: colors0[3], 19: colors0[4], 20: colors0[5], 21: colors0[6], 22: colors0[7], 23: colors0[8],
        4: colors0[0], 9: colors0[1], 10: colors0[2],
        12: colors0[0],
        14: colors0[0], 15: colors0[1],
        17: colors0[0], 18: colors0[1],
        5: colors0[0], 6: colors0[1], 7: colors0[2], 8: colors0[3],
    }
    colors_str = "['"
    for color in colors0:
        colors_str += color + "', '"
    colors_str = colors_str[:-3] + "]"
    if query_name == "query_tasks_value":
        colors = colors0
    else:
        colors = []
    dicts = {}
    if not request.session.get(query_name + '__' + 'node', False): # logout and login again to apply changes
        if query_name == "query_tasks_value":
            dicts["value"] = {}
        elif query_name == "query_timeliness":
            dicts["timeliness_sum"] = {}
            dicts["task_count"] = {}
        subtrees = {}
        for tree in node:
            if tree.level != 0:
                if tree.parent.id not in subtrees:
                    subtrees[tree.parent.id] = []
                subtrees[tree.parent.id].append(tree)
            all_task_tree = 0
            all_task_tree = tree.get_all_task_tree(and_where=and_where)
            logging.debug('all task tree {0}'.format(all_task_tree))
            all_tree_user_list = tree.get_all_tree_users()
            tree.user_list = all_tree_user_list
            tree.task_count = tree.get_task_count(and_where=and_where)
            logging.debug(tree.task_count)
            tree.average = float(tree.task_count) / (len(all_tree_user_list) or 1)
            logging.debug('average {0}'.format(tree.average))
            if query_name == "query_load_distribution":
                try:
                    tree.contribution = float(tree.task_count) / all_task_tree * 100
                except:
                    tree.contribution = 0
            tree.users = []
            try:
                dummy = tree.value
            except:
                tree.value = 0
            timeliness_sum = 0
            if query_name == "query_tasks_value":
                if tree.id not in dicts["value"]:
                    dicts["value"][tree.id] = 0
            elif query_name == "query_timeliness":
                if tree.id not in dicts["timeliness_sum"]:
                    dicts["timeliness_sum"][tree.id] = 0
                    dicts["task_count"][tree.id] = 0
            for user_profile in tree.get_user_profiles():
                user_profile.is_user = True
                user_profile.task_count = user_profile.get_task_count(and_where=and_where)
                if query_name == "query_load_distribution":
                    user_profile.contribution = user_profile.get_task_count_avg(and_where=and_where)
                user_profile.value = 0
                if query_name == "query_timeliness":
                    user_profile.timeliness, user_profile.task_count2 = user_profile.get_timeliness_and_task_count(and_where=and_where)
                    dicts["timeliness_sum"][tree.id] += user_profile.timeliness * user_profile.task_count2
                    dicts["task_count"][tree.id] += user_profile.task_count2
                if query_name == "query_tasks_value" and user_profile.user.id in open_tasks_by_user:
                    user_profile.tasks = []
                    for task in open_tasks_by_user[user_profile.user.id]:
                        task.value = (task.date_end - task.date_add).days
                        user_profile.value += task.value
                        user_profile.tasks.append(task)
                if query_name == "query_tasks_value":
                    dicts["value"][tree.id] += user_profile.value
                    tree.value += user_profile.value
                tree.users.append(user_profile)
            cur_tree = tree
            for i in range(tree.level):
                cur_tree = cur_tree.parent
                if query_name == "query_tasks_value":
                    if cur_tree.id in dicts["value"]:
                        dicts["value"][cur_tree.id] += dicts["value"][tree.id]
                    else:
                        dicts["value"][cur_tree.id] = dicts["value"][tree.id]
                    try:
                        cur_tree.value += tree.value
                    except:
                        cur_tree.value = tree.value
                elif query_name == "query_timeliness":
                    if cur_tree.id in dicts["timeliness_sum"]:
                        dicts["timeliness_sum"][cur_tree.id] += dicts["timeliness_sum"][tree.id]
                        dicts["task_count"][cur_tree.id] += dicts["task_count"][tree.id]
                    else:
                        dicts["timeliness_sum"][cur_tree.id] = dicts["timeliness_sum"][tree.id]
                        dicts["task_count"][cur_tree.id] = dicts["task_count"][tree.id]
        index = -3
        for tree in node:
            index += 1
            if tree.id in colors_by_tree_id:
                tree.color = colors_by_tree_id[tree.id]
            else:
                tree.color = colors0[index % len(colors0)]
            if query_name == "query_load_distribution":
                if tree.id in subtrees:
                    if len(tree.users) > 0:
                        subtrees[tree.id].insert(0, tree.users[0])
                    tree.subtrees = subtrees[tree.id]
            elif query_name == "query_tasks_value":
                tree.value = dicts["value"][tree.id]
                try:
                    tree.contribution = float(dicts["value"][tree.id]) / dicts["value"][tree.parent.id] * 100
                except:
                    tree.contribution = 100
            elif query_name == "query_timeliness":
                if dicts["task_count"][tree.id] > 0:
                    tree.timeliness = dicts["timeliness_sum"][tree.id] / dicts["task_count"][tree.id]
                else:
                    tree.timeliness = -2
        request.session[query_name + '__' + 'node'] = node
    if tree_id:
        node = request.session[query_name + '__' + 'node']
        current_node = [n for n in node if n.id == int(tree_id)][0]
        if current_node:
            if request.GET.get('back') or 0:
                current_node = tuple(current_node.get_ancestors())[-1]
                current_node = [n for n in node if n.id == current_node.id][0]
                return HttpResponseRedirect('/query/load_distribution/{0}?period={1}&type=table'.format(current_node.id, period))
            logging.debug('current node {0}'.format(current_node))
            node = [n for n in node if n.is_descendant_of(current_node) and abs(current_node.level - n.level) == 1]
            node.insert(0, current_node)
            logging.debug('node list {0}'.format(node))
            tree_detail = True
    else:
        node = request.session[query_name + '__' + 'node']
    colors_line = mark_safe(colors_str)
    if query_name == "query_load_distribution":
        page_title = u"Распределение нагрузки сотрудников и подразделений"
    elif query_name == "query_tasks_value":
        page_title = u"Ценности поручений"
    elif query_name == "query_timeliness":
        page_title = u"Степень своевременности выполнения поручений"
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'type': display_type,
        'period': period,
        'hides': request.GET.getlist('hide') or [],
        'colors_line': colors_line,
        'page_title': page_title,
        'query_name': query_name,
        'is_test': is_test(),
        'dbg': str(dbg), ##
    }
    query_template.update(font_dict(request.GET.get('print')))
    if 'tree_detail' in locals():
        query_template['tree_detail'] = True
    if display_type in ["bar", "pie"]:
        query_template['chart_type'] = display_type
        if not request.GET.get('print'):
            return direct_to_template(request, 'project/graph/load_distribution.html', query_template)
        else:
            return direct_to_template(request, 'project/graph/load_distribution_print.html', query_template)
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/load_distribution.html', query_template)
    else:
        return direct_to_template(request, 'project/query/load_distribution_print.html', query_template)


@login_required
def query_tasks_value(request, tree_id=False):
    return query_load_distribution(request, tree_id, query_name="query_tasks_value")


@login_required
def query_timeliness(request, tree_id=False):
    return query_load_distribution(request, tree_id, query_name="query_timeliness")


@login_required
def query_canceled_tasks(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_1'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    node = TreeDepartment.objects.all()

    for tree in node:
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        tree.deleted_30_task = tree.get_task_count(tab_month, tab_year, and_where=[('terminated', True), ('progress__lt', 30), ('progress__gte', 0)])
        tree.deleted_60_task = tree.get_task_count(tab_month, tab_year, and_where=[('terminated', True), ('progress__gte', 30), ('progress__lte', 60)])
        tree.deleted_100_task = tree.get_task_count(tab_month, tab_year, and_where=[('terminated', True), ('progress__gt', 60)])
        tree.deleted_task = tree.deleted_30_task + tree.deleted_60_task + tree.deleted_100_task
        try:
            tree.deleted_task_prc = float(tree.deleted_task) / tree.task_count * 100
        except:
            tree.deleted_task_prc = 0
            # Add user deleted task for list
        tree.users = []
        for user in tree.get_user_profiles():
            user.task_count = user.get_task_count(tab_month, tab_year)
            user.deleted_30_task = user.get_task_count(tab_month, tab_year, and_where=[('terminated', True), ('progress__lt', 30), ('progress__gte', 0)])
            user.deleted_60_task = user.get_task_count(tab_month, tab_year, and_where=[('terminated', True), ('progress__gte', 30), ('progress__lte', 60)])
            user.deleted_100_task = user.get_task_count(tab_month, tab_year, and_where=[('terminated', True), ('progress__gt', 60)])
            user.deleted_task = user.deleted_30_task + user.deleted_60_task + user.deleted_100_task
            try:
                user.deleted_task_prc = float(user.deleted_task) / user.task_count * 100
            except:
                user.deleted_task_prc = 0
            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/canceled_tasks.html', query_template)
    else:
        return direct_to_template(request, 'project/query/canceled_tasks_print.html', query_template)


@login_required
def query_deleted_tasks(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_2'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    node = TreeDepartment.objects.all()
    for tree in node:
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        tree.deleted_30_task = tree.get_task_count(tab_month, tab_year, and_where=[('deleted', True), ('progress__lt', 30), ('progress__gte', 0)])
        tree.deleted_60_task = tree.get_task_count(tab_month, tab_year, and_where=[('deleted', True), ('progress__gte', 30), ('progress__lte', 60)])
        tree.deleted_100_task = tree.get_task_count(tab_month, tab_year, and_where=[('deleted', True), ('progress__gt', 60)])
        tree.deleted_task = tree.deleted_30_task + tree.deleted_60_task + tree.deleted_100_task
        try:
            tree.deleted_task_prc = float(tree.deleted_task) / tree.task_count * 100
        except:
            tree.deleted_task_prc = 0
            # Add user deleted task for list
        tree.users = []
        for user in tree.get_user_profiles():
            user.task_count = user.get_task_count(tab_month, tab_year)
            user.deleted_30_task = user.get_task_count(tab_month, tab_year, and_where=[('deleted', True), ('progress__lt', 30), ('progress__gte', 0)])
            user.deleted_60_task = user.get_task_count(tab_month, tab_year, and_where=[('deleted', True), ('progress__gte', 30), ('progress__lte', 60)])
            user.deleted_100_task = user.get_task_count(tab_month, tab_year, and_where=[('deleted', True), ('progress__gt', 60)])
            user.deleted_task = user.deleted_30_task + user.deleted_60_task + user.deleted_100_task
            try:
                user.deleted_task_prc = float(user.deleted_task) / user.task_count * 100
            except:
                user.deleted_task_prc = 0
            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/deleted_tasks.html', query_template)
    else:
        return direct_to_template(request, 'project/query/deleted_tasks_print.html', query_template)


@login_required
def query_start_date_changes(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_3'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    node = TreeDepartment.objects.all()
    for tree in node:
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        tree.change_30_task = tree.get_change_task(tab_month, tab_year, and_where=[('progress__lt', '30')])
        tree.change_60_task = tree.get_change_task(tab_month, tab_year, and_where=[('progress__gte', '30'), ('progress__lte', '60')])
        tree.change_100_task = tree.get_change_task(tab_month, tab_year, and_where=[('progress__gt', '60')])
        tree.change_task = tree.change_30_task + tree.change_60_task + tree.change_100_task

        tree.users = []
        for user in tree.get_user_profiles():
            user.task_count = user.get_task_count(tab_month, tab_year)
            user.change_30_task = user.get_change_task(tab_month, tab_year, and_where=[('progress__lt', '30')])
            user.change_60_task = user.get_change_task(tab_month, tab_year, and_where=[('progress__gte', '30'), ('progress__lte', '60')])
            user.change_100_task = user.get_change_task(tab_month, tab_year, and_where=[('progress__gt', '60')])
            user.change_task = user.change_30_task + user.change_60_task + user.change_100_task
            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/start_date_changes.html', query_template)
    else:
        return direct_to_template(request, 'project/query/start_date_changes_print.html', query_template)


@login_required
def query_completion_date_changes(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_4'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    node = TreeDepartment.objects.all()
    for tree in node:
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        tree.change_30_task = tree.get_change_task(tab_month, tab_year, and_where=[('progress__lt', '30')], attrs='date_end')
        tree.change_60_task = tree.get_change_task(tab_month, tab_year, and_where=[('progress__gte', '30'), ('progress__lte', '60')], attrs='date_end')
        tree.change_100_task = tree.get_change_task(tab_month, tab_year, and_where=[('progress__gt', '60')], attrs='date_end')
        tree.change_task = tree.change_30_task + tree.change_60_task + tree.change_100_task
        tree.users = []
        for user in tree.get_user_profiles():
            user.task_count = user.get_task_count(tab_month, tab_year)
            user.change_30_task = user.get_change_task(tab_month, tab_year, and_where=[('progress__lt', '30')], attrs='date_end')
            user.change_60_task = user.get_change_task(tab_month, tab_year, and_where=[('progress__gte', '30'), ('progress__lte', '60')], attrs='date_end')
            user.change_100_task = user.get_change_task(tab_month, tab_year, and_where=[('progress__gt', '60')], attrs='date_end')
            user.change_task = user.change_30_task + user.change_60_task + user.change_100_task
            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/completion_date_changes.html', query_template)
    else:
        return direct_to_template(request, 'project/query/completion_date_changes_print.html', query_template)


@login_required
def query_tasks_status_review(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_5'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    if request.GET.get('year'):
        today_day = datetime.datetime(int(tab_year), int(tab_month), calendar.monthrange(int(tab_year), int(tab_month))[1]).date()
    else:
        today_day = datetime.datetime.now().date()
    node = TreeDepartment.objects.all()
    for tree in node:
        tree.count_open_task = tree.get_task_count(tab_month, tab_year, and_where=[('layer', today_day)])
        tree.count_closed_task = tree.get_task_count(tab_month, tab_year, and_where=[('progress', 100)])
        tree.count_deleted_task = tree.get_task_count(tab_month, tab_year, and_where=[('deleted', True)]) or 0
        tree.count_prosc_task = tree.get_task_count(tab_month, tab_year, and_where=[('layer', today_day), ('date_end__lt', today_day)])
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        try:
            tree.task_count_prc = tree.get_today_avg_task(and_where=[('layer', today_day)], attrs='progress')
        except:
            tree.task_count_prc = 0
        tree.users = []
        for user in tree.get_user_profiles():
            user.count_open_task = user.get_task_count(tab_month, tab_year, and_where=[('layer', today_day)], )
            user.count_closed_task = user.get_task_count(tab_month, tab_year, and_where=[('progress', 100)], )
            user.count_deleted_task = user.get_task_count(tab_month, tab_year, and_where=[('deleted', True)], )
            user.count_prosc_task = user.get_task_count(tab_month, tab_year, and_where=[('layer', today_day), ('date_end__lt', today_day)])
            user.task_count = user.get_task_count(tab_month, tab_year)
            try:
                user.task_count_prc = user.get_today_avg_task(and_where=[('layer', today_day)], attrs='progress')
            except:
                user.task_count_prc = 0
            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/tasks_status_review.html', query_template)
    else:
        return direct_to_template(request, 'project/query/tasks_status_review_print.html', query_template)


@login_required
def query_tasks_delegation_level(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_6'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    node = TreeDepartment.objects.all()
    for tree in node:
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        tree.ath_20_task = tree.get_task_count(tab_month, tab_year, and_where=[('author__id', '20')])
        tree.ath_15_task = tree.get_task_count(tab_month, tab_year, and_where=[('author__id', '15')])
        tree.ath_6_task = tree.get_task_count(tab_month, tab_year, and_where=[('author__id', '6')])
        tree.users = []
        for user in tree.get_user_profiles():
            user.task_count = user.get_task_count(tab_month, tab_year)
            user.ath_20_task = user.get_task_count(tab_month, tab_year, and_where=[('author__id', '20')])
            user.ath_15_task = user.get_task_count(tab_month, tab_year, and_where=[('author__id', '15')])
            user.ath_6_task = user.get_task_count(tab_month, tab_year, and_where=[('author__id', '6')])
            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/tasks_delegation_level.html', query_template)
    else:
        return direct_to_template(request, 'project/query/tasks_delegation_level_print.html', query_template)


@login_required
def query_closing_tasks(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_7'):
            raise PermissionDenied
    query_template = {
        'date': datetime.datetime.now().date(),
        'otd': UserProfile.staff_objects.all(),
        'is_test': is_test(),
    }
    if request.GET.get('date') is None:
        return direct_to_template(request, 'project/query/closing_tasks.html', query_template)
    else:
        task = Task.objects.filter(date_end=request.GET.get('date'))
        new_task = {}
        array = []
        for t in task:
            new_task['%s' % t.hash_task] = t.id
        get_task = Task.objects.filter(id__in=[val for key, val in new_task.items()]).order_by('author_task__last_name')
        return direct_to_template(request, 'project/query/closing_tasks_list.html', {
            'project': get_task,
        })


@login_required
def query_overdue_tasks(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_8'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    if request.GET.get('year'):
        today_day = datetime.datetime(int(tab_year), int(tab_month), calendar.monthrange(int(tab_year), int(tab_month))[1]).date()
    else:
        today_day = datetime.datetime.now().date()
    node = TreeDepartment.objects.all()
    for tree in node:
        tree.task_count = tree.get_task_count(tab_month, tab_year)
        tree.deleted_30_task = tree.get_task_count(tab_month, tab_year, and_where=[('progress__lt', 30), ('progress__gte', 0), ('layer', today_day), ('date_end__lt', today_day)])
        tree.deleted_60_task = tree.get_task_count(tab_month, tab_year, and_where=[('progress__gte', 30), ('progress__lte', 60), ('layer', today_day), ('date_end__lt', today_day)])
        tree.deleted_100_task = tree.get_task_count(tab_month, tab_year, and_where=[('progress__gt', 60), ('layer', today_day), ('date_end__lt', today_day)])
        tree.deleted_task = tree.deleted_30_task + tree.deleted_60_task + tree.deleted_100_task
        try:
            tree.deleted_task_prc = float(tree.deleted_task) / tree.task_count * 100
        except:
            tree.deleted_task_prc = 0
            # Add user deleted task for list
        tree.users = []
        for user in tree.get_user_profiles():
            user.task_count = user.get_task_count(tab_month, tab_year)
            user.deleted_30_task = user.get_task_count(tab_month, tab_year,
                                                       and_where=[('progress__lt', 30), ('progress__gte', 0), ('layer', today_day), ('date_end__lt', today_day)])
            user.deleted_60_task = user.get_task_count(tab_month, tab_year,
                                                       and_where=[('progress__gte', 30), ('progress__lte', 60), ('layer', today_day), ('date_end__lt', today_day)])
            user.deleted_100_task = user.get_task_count(tab_month, tab_year, and_where=[('progress__gt', 60), ('layer', today_day), ('date_end__lt', today_day)])
            user.deleted_task = user.deleted_30_task + user.deleted_60_task + user.deleted_100_task
            try:
                user.deleted_task_prc = float(user.deleted_task) / user.task_count * 100
            except:
                user.deleted_task_prc = 0

            tree.users.append(user)
    query_template = {
        'date': datetime.datetime.now().date(),
        'node': node,
        'year': int(tab_year),
        'month': int(tab_month),
        'hides': request.GET.getlist('hide') or [],
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/overdue_tasks.html', query_template)
    else:
        return direct_to_template(request, 'project/query/overdue_tasks_print.html', query_template)


def query_overdue_tasks_view(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_8'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    if request.GET.get('year'):
        today_day = datetime.datetime(int(tab_year), int(tab_month), calendar.monthrange(int(tab_year), int(tab_month))[1]).date()
    else:
        today_day = datetime.datetime.now().date()
    out = '<ul>'
    get_user = UserProfile.objects.get(id=request.GET.get('user')).user
    tasks = Task.objects.filter(author_task=get_user, date_end__lt=today_day, layer=today_day).select_related()
    for t in tasks:
        out += '<li>%s</li>' % t.title
    out += '</ul>'
    return HttpResponse('%s' % out)


@login_required
def query_task_count_dynamics(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_9'):
            raise PermissionDenied
    chart = {}
    range_year = request.GET.get('year') or '2012-2013'
    start_year = range_year[:4]
    end_year = range_year[-4:]
    task_list = Task().get_tasks_by_month_from_year(start_year, end_year)
    line = [start_year, end_year]
    d = task_list.values()
    if filter(m_filter, d):
        chart['title'] = u'Динамика количества поручений в организации'
        chart['desc'] = u'График изменения количества поручений в организации'
    colors_line = mark_safe("['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#f2eb00', '#55c99e', '#a25df4', '#0007ff', '#f60459']")
    out = {
        'chart': chart,
        'line': line,
        'all_tasks': task_list,
        'year': start_year + '-' + end_year,
        'colors_line': colors_line,
        'is_test': is_test(),
    }
    out.update(font_dict(request.GET.get('print')))
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/task_count_dynamics.html', out)
    else:
        return direct_to_template(request, 'project/query/task_count_dynamics_print.html', out)


@login_required
def query_most_least_variable_task(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_9'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    user_names = {}
    for user in get_visible_user_profiles().exclude(job='').select_related():
        user_names[user.user.id] = user.user, user.user.id

    logging.debug(user_names)
    task_by_month = Task.objects.filter(layer__month=int(tab_month), layer__year=int(tab_year)).values('id', 'edit', 'title', 'hash_task', 'author_task')
    task_dictionary = {}
    for task in task_by_month:
        if task['edit'] != 0 and task['hash_task'] not in task_dictionary:
            try:
                task_dictionary[task['hash_task']] = [0, task['title'], user_names[task['author_task']], task['id']]
            except KeyError:
                pass
        elif task['edit'] != 0 and task['hash_task'] in task_dictionary:
            task_dictionary[task['hash_task']][0] += 1
    task_dictionary = sorted(task_dictionary.values(), key=operator.itemgetter(0), reverse=True)
    logging.debug(task_dictionary)

    out = {
        'task_top': task_dictionary[:10],
        'task_low': task_dictionary[-10:],
        'year': int(tab_year),
        'month': int(tab_month),
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/most_least_variable_task.html', out)
    else:
        return direct_to_template(request, 'project/query/most_least_variable_task_print.html', out)


@login_required
def query_planning_errors(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_9'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    if request.GET.get('year'):
        # checking if tab is current date
        if int(tab_month) == datetime.datetime.now().month and int(tab_year) == datetime.datetime.now().year:
            today_day = datetime.datetime.now().date()
        else:
            today_day = datetime.datetime(int(tab_year), int(tab_month),
                                          calendar.monthrange(int(tab_year), int(tab_month))[1]).date()
    else:
        today_day = datetime.datetime.now().date()
    task_user = {}
    task_list = []
    for task in Task.objects.filter(layer__month=int(tab_month), layer__year=int(tab_year)).values('author_task_id', 'layer', 'date_end', 'hash_task'):
        task_list.append(task)

    if task_list:
        for user in get_visible_user_profiles().exclude(job='').select_related():
            rescheduled_task_dictionary = {}
            hash_list_changed = []
            expired_task_list = []
            all_task = []

            for task in task_list:
                if task['author_task_id'] == user.user.id:
                    if task['hash_task'] not in rescheduled_task_dictionary:
                        rescheduled_task_dictionary[task['hash_task']] = [task['date_end']]
                    else:
                        rescheduled_task_dictionary[task['hash_task']].append(task['date_end'])
                    if task['hash_task'] not in all_task:
                        all_task.append(task['hash_task'])
                    if task['layer'] == today_day and task['date_end'] < today_day:
                        expired_task_list.append(task['hash_task'])

            for k, v in rescheduled_task_dictionary.items():
                if len(set(v)) > 1:
                    hash_list_changed.append(k)

            expired_task_list.extend(hash_list_changed)
            incorrect_planning_task = len(set(expired_task_list))
            all_task = len(all_task)
            try:
                user.all_task = all_task
                user.incorrect_planning_task = incorrect_planning_task
                task_user[user] = round(incorrect_planning_task / float(all_task) * 100, 1)
            except ZeroDivisionError:
                task_user[user] = 0

        top_10_list = sorted(task_user.items(), key=lambda (k, v): (v, k), reverse=True)[:10]
    else:
        top_10_list = ''
    out = {
        'top_10_list': top_10_list,
        'year': int(tab_year),
        'month': int(tab_month),
        'tab_date': datetime.datetime(int(tab_year), int(tab_month), 1),
        'month_names': month_names,
        'is_test': is_test(),
    }
    if not request.GET.get('print'):
        return direct_to_template(request, 'project/query/planning_errors.html', out)
    else:
        return direct_to_template(request, 'project/query/planning_errors_print.html', out)


@login_required
def query_tab_13(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_9'):
            raise PermissionDenied
    tab_month = request.GET.get('type') or datetime.datetime.now().month
    tab_year = request.GET.get('year') or datetime.datetime.now().year
    user_list = get_visible_user_profiles().exclude(job='').select_related()
    task_list = Task.objects.filter(layer__month=int(tab_month), layer__year=int(tab_year)).values('hash_task', 'author_task_id', 'nomer', 'number', 'author')
    all_task = Task.objects.values('hash_task', 'author_task_id', 'nomer', 'number', 'author')
    logging.debug(len(task_list))
    for user in user_list:
        user.ref = 0
        user.hash_task_list = []
        user.user_task_list = 0
        user_task_list = tuple(((task['hash_task'], task['author_task_id'], task['nomer']) for task in task_list if
                                task['author_task_id'] == user.user.id))
        user.user_task_list = len([e for i, e in enumerate(user_task_list) if e not in user_task_list[:i]])
        logging.debug(len(user_task_list))
        for task in user_task_list:
            if task[0] not in user.hash_task_list:
                if get_relationship_count(task, user, all_task):
                    user.hash_task_list.append(task[0])

        logging.debug((user.ref, user))
    out = {
        'user_list': user_list
    }
    return direct_to_template(request, 'project/query/13.html', out)


@login_required
def query_attention_distribution(request):
    display_type = request.GET.get('type') or "table"
    period = request.GET.get('period') or "current_month"
    users0 = get_allowed_visible_users(request)
    users = []
    total = 0
    start_date = get_period_start_date(period)
    for user in users0:
        if user.get_profile().role == 1:
            user.time_in_history = UsersOnline.objects.filter(user=user,
                                                              time_in_history__range=[datetime.datetime.combine(start_date, datetime.time.min), datetime.datetime.now()]).count()
            total += user.time_in_history
            users.append(user)
    return direct_to_template(request, 'project/query/attention_distribution.html', {
        'users': users,
        'page_title': u"Распределение внимания руководителя",
        'total': total,
        'type': display_type,
        'period': period,
        'query_name': "query_attention_distribution",
        'is_test': is_test(),
    })


@login_required
def query_task_control(request):
    display_type = request.GET.get('type') or "table"
    period = request.GET.get('period') or "current_month"
    users0 = get_allowed_visible_users(request)
    users = []
    total = 0
    start_date = get_period_start_date(period)
    for user in users0:
        if user.get_profile().role == 1 or user.get_profile().role2 == 1:
            user.time_in_history = UsersOnline.objects.filter(user=user,
                                                              time_in_history__range=[datetime.datetime.combine(start_date, datetime.time.min), datetime.datetime.now()]).count()
            total += user.time_in_history
            users.append(user)
    return direct_to_template(request, 'project/query/attention_distribution.html', {
        'users': users,
        'page_title': u"Процент проконтролированных поручений руководителем управления/отдела",
        'total': total,
        'type': display_type,
        'period': period,
        'query_name': "query_task_control",
        'is_test': is_test(),
    })


@login_required
def query_attention_distribution_time(request):
    display_type = request.GET.get('type') or "table"
    period = request.GET.get('period') or "current_month"
    users0 = get_allowed_visible_users(request)
    # users0 = User.objects.filter(id=1)
    users = []
    total = 0
    start_date = get_period_start_date(period)
    for user in users0:
        if user.get_profile().role == 1:
            user.time_in_history = 0
            user_time_list = AttentionOnline.objects.filter(user=user, time__range=[
                datetime.datetime.combine(start_date, datetime.time.min), datetime.datetime.now()]).values_list(
                'time_in_history', flat=True)
            for x in user_time_list:
                user.time_in_history += x
            total += user.time_in_history
            user.time_in_history = str(datetime.timedelta(seconds=user.time_in_history))
            users.append(user)
    return direct_to_template(request, 'project/query/attention_distribution.html', {
        'users': users,
        'page_title': u"Распределение внимания руководителя",
        'total': str(datetime.timedelta(seconds=total)),
        'type': display_type,
        'period': period,
        'query_name': "query_attention_distribution",
        'is_test': is_test(),
    })


def get_lasts_months(start_month, count):
    months = []
    for month in xrange(count):
        month_id = start_month - month
        if month_id <= 0:
            month_id += 12
        months.append(month_id)
    return months


def next_month(month):
    if month == 12:
        return 1
    return month + 1


@login_required
def query_attention_distribution_time2(request):
    display_type = request.GET.get('type') or "table"
    period = request.GET.get('period') or "current_month"
    users0 = get_allowed_visible_users(request)
    users = []
    total = 0
    start_date = get_period_start_date(period)
    for user in users0:
        if user.get_profile().role == 1:
            current_month = datetime.datetime.now().month
            months = get_lasts_months(current_month, 4)
            now = datetime.datetime.now()
            months_names = [month_names[x - 1] for x in months]
            user.months = []
            for month in months:
                start_month = now.replace(day=1, month=month)
                end_month = now.replace(day=1, month=next_month(month))
                user_time_list = AttentionOnline.objects.filter(
                    user=user,
                    time__range=[start_month, end_month]
                ).values_list('time_in_history', flat=True)
                time_in_history = 0
                for x in user_time_list:
                    time_in_history += x
                user.months.append(str(datetime.timedelta(seconds=time_in_history)))
            user.time_in_history = 0
            user_time_list = AttentionOnline.objects.filter(user=user, time__range=[
                datetime.datetime.combine(start_date, datetime.time.min), datetime.datetime.now()]).values_list(
                'time_in_history', flat=True)
            for x in user_time_list:
                user.time_in_history += x
            total += user.time_in_history
            user.time_in_history = str(datetime.timedelta(seconds=user.time_in_history))
            users.append(user)
    return direct_to_template(request, 'project/query/attention_distribution2.html', {
        'users': users,
        'page_title': u"Распределение внимания руководителя",
        'total': str(datetime.timedelta(seconds=total)),
        'type': display_type,
        'period': period,
        'months_names': months_names,
        'query_name': "query_attention_distribution",
        'is_test': is_test(),
    })


def get_manager_task_count(user, period):
    start_date = get_period_start_date(period)
    task_count = Task.objects.filter(author_task=user.id,
                                     layer__range=[datetime.datetime.combine(start_date, datetime.time.min), datetime.datetime.now()]).select_related().values_list(
        'hash_task').distinct().count()
    return task_count


def query_tasks_distribution(request, query_name, page_title):
    display_type = request.GET.get('display_type') or "table"
    period = request.GET.get('period') or "current_month"

    date_start = datetime.datetime.now()
    date_end = date_start

    users0 = get_allowed_visible_users(request)
    users = []
    total = 0

    start_date = get_period_start_date(period)
    tasks0 = Task.objects.filter(layer__range=[datetime.datetime.combine(start_date, datetime.time.min), datetime.datetime.now()], nomer__isnull=False)

    tasks = []
    hashes = []

    for task in tasks0:
        if task.hash_task in hashes:
            pass
        else:
            hashes.append(task.hash_task)
            tasks.append(task)

    total = 0

    for user in users0:
        user.task_count = 0
        if query_name == "query_manager_tasks_distribution":
            if user.get_profile().role == 1:
                for task in tasks:
                    if task.author == user: #  and task.nomer.isdigit()
                        user.task_count += 1

                total += user.task_count
                users.append(user)

        elif query_name == "query_subordinate_tasks_distribution":
            for task in tasks:
                if task.author_task == user: #  and task.nomer.isdigit()
                    user.task_count += 1
            total += user.task_count
            users.append(user)

    query_template = {
        'users': users,
        'page_title': page_title,
        'total': total,
        'type': display_type,
        'period': period,
        'query_name': query_name,
        'is_test': is_test(),
    }

    return direct_to_template(request, 'project/query/attention_distribution.html', query_template)


def subordinate_tasks_distribution_graph_data(period, profile):
    if period == "current_month":
        delta = datetime.datetime.today().day
    elif period == "week":
        delta = 7
    elif period == "month":
        delta = 31
    elif period == "quarter":
        delta = 100
    elif period == "year":
        delta = 365
    else:
        raise Exception("unknown period " + str(period))
    dates = [datetime.datetime.date(datetime.datetime.now() - datetime.timedelta(days=i)) for i in reversed(range(delta))]
    tasks = Task.objects.filter(layer__in=dates, author_task__id=profile.user.id, author__isnull=False).order_by('author', 'layer')
    author_dict = {}
    for task in tasks:
        if not task.author.id in author_dict:
            author_dict[task.author.id] = {}
        if not task.layer in author_dict[task.author.id]:
            author_dict[task.author.id][task.layer] = []
        author_dict[task.author.id][task.layer].append(task)
    ys_dict = {}
    i = -1
    for author_id in author_dict:
        i += 1
        ys_dict[author_id] = {'ys': [], 'color': g_colors[i], 'author': User.objects.get(id=author_id)}
        for date in dates:
            if date in author_dict[author_id]:
                val = len(author_dict[author_id][date])
            else:
                val = 0
            ys_dict[author_id]['ys'].append(val)
    xs = [date.strftime('%d.%m') for date in dates]
    if len(tasks) == 0:
        ys_dict = {'_': {'ys': [0 for date in dates], 'color': g_colors[0], 'author': '_'}}
    return xs, ys_dict


@login_required
def graph_subordinate_tasks_distribution(request):
    if not request.user.is_superuser:
        if not request.user.has_perm('accounts.view_query_0'):
            raise PermissionDenied
    period = request.GET.get('period') or "current_month"
    profile = UserProfile.objects.get(id=request.GET.get('profiles'))
    chart = {}
    colors_line = mark_safe("['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']")
    out = {
        'values': '',
        'profiles': UserProfile.staff_objects.all(),
        'colors_line': colors_line,
        'is_test': is_test(),
    }
    chart['title'] = u'График распределения поручений у подчиненного'
    chart['desc'] = u'График распределения поручений у подчиненного'
    chart['line'], ys_dict = subordinate_tasks_distribution_graph_data(period, profile)
    chart['count'] = ''
    out = {
        'period': period,
        'values': '',
        'chart': chart,
        'ys_dict': ys_dict,
        'profiles': UserProfile.staff_objects.all(),
        'profile': profile,
        'colors_line': colors_line,
        'is_test': is_test(),
    }
    return direct_to_template(request, 'project/graph/subordinate_tasks_distribution.html', out)


@login_required
def query_subordinate_tasks_distribution(request):
    return query_tasks_distribution(request, "query_subordinate_tasks_distribution", u"График распределения поручений у подчиненного")


@login_required
def query_manager_tasks_distribution(request):
    return query_tasks_distribution(request, "query_manager_tasks_distribution", u"Распределение поручений руководителем")


@login_required
def query_average_control_term(request):
    query_name = 'query_average_control_term'
    page_title = u" Средний срок контроля поручений руководителем"

    display_type = request.GET.get('display_type') or "table"
    period = request.GET.get('period') or "current_month"

    date_start = datetime.datetime.now()
    date_end = date_start

    filters_form = PeriodFilters(initial={
        'period': 'today',
    })

    if request.GET:
        filters_form = PeriodFilters(request.GET)
        if filters_form.is_valid():
            values = filters_form.cleaned_data
            date_start = filters_form.start
            date_end = filters_form.end

    allowed_users = get_allowed_visible_users(request)

    users = []
    total = 0
    total_control_term = 0
    tasks = Task.objects.filter(layer__range=(date_start, date_end), nomer__isnull=False)

    total = 0

    for user in allowed_users:
        user.task_count = 0
        user.common_control_term = 0
        user.average_control_term = 0

        if user.get_profile().role == 1 or user.get_profile().role2 == 1:
            employers = user.get_profile().get_employers()
            #  Задачи на контроле
            user.user_conrol_tasks = []

            for task in tasks:
                if (task.author == user or (task.author_task.get_profile() in employers)) and task.author != task.author_task: #  and task.nomer.isdigit()
                    user.task_count += 1
                    user.user_conrol_tasks.append(task)
                    try:
                        visit_action = Action.objects.filter(user=user, task=task, action='visit').order_by('date_create')[:1].get()
                        difference = WorkTime.substraction(task.create_task, visit_action.date_create, user.get_profile().location)
                        user.common_control_term += difference
                    except Exception, e:
                        #Если руководитель не заходил сегодня в это поручение, считаем разницу от текущего момента
                        difference = WorkTime.substraction(task.create_task, datetime.datetime.now(), user.get_profile().location)
                        user.common_control_term += difference

            total += user.task_count
            total_control_term += user.common_control_term

            if user.task_count != 0:
                user.average_control_term = user.common_control_term / user.task_count
            users.append(user)

        if total != 0:
            total_average_control_term = total_control_term / total
        else:
            total_average_control_term = 0

    query_template = {
        'users': users,
        'page_title': page_title,
        'total': total,
        'type': display_type,
        'filters_form': filters_form,
        'period': period,
        'query_name': query_name,
        'is_test': is_test(),
        'total_average_control_term': total_average_control_term,
    }

    return direct_to_template(request, 'project/query/average_control_term.html', query_template)


@login_required
def query_tasks_changed_statement(request):
    """Процент поручений, по которым вносились изменения в формулировку"""
    def by_users():
        """Отчет по пользователям"""
        changed_task_revisions = TaskRevision.objects.filter(
            (Q(changed_fields__field_name='title') | Q(changed_fields__field_name='text')),
            date_add__range=(date_from, date_to),
            ).order_by('pk').distinct()

        tasks = Task.task_manager.filter(deleted=False, date_add__range=(date_from, date_to))

        if user:
            if change_initiator == 'manager':
                changed_task_revisions = changed_task_revisions.filter(author=user)
                tasks = tasks.filter(author=user)
            elif change_initiator == 'performer':
                changed_task_revisions = changed_task_revisions.filter(author_task=user)
                tasks = tasks.filter(author_task=user)
            else:
                changed_task_revisions = changed_task_revisions.filter(Q(author=user) | Q(author_task=user))
                tasks = tasks.filter(Q(author=user) | Q(author_task=user))

        if change_initiator:
            if change_initiator == 'manager':
                changed_task_revisions = changed_task_revisions.filter(
                    Q(changed_fields__field_name='title', changed_fields__field_editor=F('author')) |
                    Q(changed_fields__field_name='text', changed_fields__field_editor=F('author'))
                )
            elif change_initiator == 'performer':
                changed_task_revisions = changed_task_revisions.filter(
                    Q(changed_fields__field_name='title', changed_fields__field_editor=F('author_task')) |
                    Q(changed_fields__field_name='text', changed_fields__field_editor=F('author_task'))
                )
        changed_task_hashes = changed_task_revisions.values_list('hash_task', flat=True).distinct()
        changed_tasks = Task.task_manager.filter(deleted=False, hash_task__in=changed_task_hashes)

        tasks_total = tasks.count()
        if tasks_total:
            changed_statement_tasks_count = changed_task_revisions.values('hash_task').distinct().count()
            changed_statement_tasks_percent = float(changed_statement_tasks_count) / tasks_total * 100
            changed_statement_tasks_percent = round(changed_statement_tasks_percent, 2)
        else:
            changed_statement_tasks_percent = 0

        context = {
            'date_from': date_from,
            'date_to': date_to,
            'filter_form': filter_form,
            'task_revisions': changed_task_revisions,
            'changed_tasks': changed_tasks,
            'change_initiator': change_initiator,
            'tasks_total': tasks_total,
            'changed_statement_tasks_percent': changed_statement_tasks_percent,
            }
        template_name = 'project/query/tasks_changed_statement/table_users.html'
        return direct_to_template(request, template_name, context)

    def by_departments():
        """Отчет по подразделениям"""
        departments = TreeDepartment.objects.exclude(level=0)
        total = {
            'tasks_count': 0,
            'changed_by_manager_count': 0,
            'changed_by_performer_count': 0,
        }
        for department in departments:
            department_tasks = Task.task_manager.filter(
                date_add__range=(date_from, date_to), deleted=False, author_task__userprofile__tree=department
            )
            task_hashes = [task.hash_task for task in department_tasks]
            statement_changed_revisions = TaskRevision.objects.filter(
                (Q(changed_fields__field_name='title') | Q(changed_fields__field_name='text')),
                hash_task__in=task_hashes
            ).order_by('hash_task').values('hash_task').distinct()
            changed_by_manager = statement_changed_revisions.filter(
                (Q(changed_fields__field_name='title', changed_fields__field_editor=F('author')) |
                 Q(changed_fields__field_name='text', changed_fields__field_editor=F('author'))),
            )
            changed_by_performer = statement_changed_revisions.filter(
                (Q(changed_fields__field_name='title', changed_fields__field_editor=F('author_task')) |
                 Q(changed_fields__field_name='text', changed_fields__field_editor=F('author_task'))),
            )
            tasks_total = department_tasks.count()
            changed_by_manager_count = changed_by_manager.count()
            changed_by_performer_count = changed_by_performer.count()
            if tasks_total:
                changed_by_manager_percent = float(changed_by_manager_count) / tasks_total * 100
                changed_by_manager_percent = round(changed_by_manager_percent, 2)
                changed_by_performer_percent = float(changed_by_performer_count) / tasks_total * 100
                changed_by_performer_percent = round(changed_by_performer_percent, 2)
            else:
                changed_by_manager_percent = 0
                changed_by_performer_percent = 0
            department.tasks_total = tasks_total
            department.changed_by_manager_count = changed_by_manager_count
            department.changed_by_manager_percent = changed_by_manager_percent
            department.changed_by_performer_count = changed_by_performer_count
            department.changed_by_performer_percent = changed_by_performer_percent

            total['tasks_count'] += tasks_total
            total['changed_by_manager_count'] += changed_by_manager_count
            total['changed_by_performer_count'] += changed_by_performer_count

        if total['tasks_count']:
            total['changed_by_manager_percent'] = float(total['changed_by_manager_count']) / total['tasks_count'] * 100
            total['changed_by_manager_percent'] = round(total['changed_by_manager_percent'], 2)
            total['changed_by_performer_percent'] = float(total['changed_by_performer_count']) / total['tasks_count'] * 100
            total['changed_by_performer_percent'] = round(total['changed_by_performer_percent'], 2)
        else:
            total['changed_by_manager_percent'] = 0
            total['changed_by_performer_percent'] = 0

        context = {
            'date_from': date_from,
            'date_to': date_to,
            'filter_form': filter_form,
            'departments': departments,
            'total': total,
        }
        template_name = 'project/query/tasks_changed_statement/table_departments.html'
        return direct_to_template(request, template_name, context)

    date_from = None
    date_to = None
    change_initiator= None
    user = None
    display_type = 'users'


    if request.GET.get('period'):
        filter_form = QueryTasksChangedStatementFilterForm(request.GET, initial={'period': 'today'})
        if filter_form.is_valid():
            date_from = filter_form.start
            date_to = filter_form.end
            change_initiator= filter_form.cleaned_data['change_initiator']
            user = filter_form.cleaned_data['user']
            display_type = filter_form.cleaned_data['display_type']

    if not date_from or not date_to:
        filter_form = QueryTasksChangedStatementFilterForm(initial={'period': 'today'})
        date_from = datetime.date.today()
        date_to = datetime.date.today()

    if display_type == 'users':
        return by_users()
    elif display_type == 'departments':
        return by_departments()


@login_required
def query_change_task_completion_terms(request):
    """Изменение сроков исполнения поручения"""
    def table():
        all_users = get_allowed_visible_users(request)
        if filter_employees == 'division_managers':
            department_tree = TreeDepartment.objects.filter(userprofile__is_division_manager=True)
        elif filter_employees == 'department_managers':
            department_tree = TreeDepartment.objects.filter((Q(userprofile__role=True) | Q(userprofile__role2=True)),
                                                    userprofile__is_division_manager=False)
        else:
            department_tree = TreeDepartment.objects.all()
        department_tree = department_tree.distinct()

        for node in department_tree:
            node.tasks_count = 0
            node.users = []
            users = all_users.filter(userprofile__tree=node)
            if filter_employees == 'division_managers':
                users = users.filter(userprofile__is_division_manager=True)
            elif filter_employees == 'department_managers':
                users = users.filter(Q(userprofile__role=True) | Q(userprofile__role2=True))
            node.users = users
            for user in node.users:
                user.tasks_count = 0
                profile = user.get_profile()
                task_revisions = TaskRevision.objects.filter(
                    revision_number__gt=1, changed_fields__field_name__in=('date_add', 'date_end')
                )
                # Если это менеджер ищем изменения в поручениях, которые дал он
                if profile.is_division_manager or profile.role or profile.role2:
                    task_revisions = task_revisions.filter(author=user)
                else:
                    # Иначе ищем в назначенных поручениях
                    task_revisions = task_revisions.filter(author_task=user)
                task_revisions = task_revisions.order_by('hash_task')
                task_hashes = task_revisions.values('hash_task').distinct()
                tasks = Task.task_manager.filter(deleted=False, hash_task__in=task_hashes,
                                                 date_add__range=(date_from, date_to))
                user.changed_tasks = tasks
                for task in user.changed_tasks:
                    node.tasks_count += 1
                    user.tasks_count += 1
                    date_add_values = []
                    date_end_values = []
                    date_change_initiators = []
                    revisions = TaskRevision.objects.filter(
                        hash_task=task.hash_task, changed_fields__field_name__in=(('date_add', 'date_end'))
                    ).distinct()
                    for revision in revisions:
                        for changed_field in revision.changed_fields.all():
                            if changed_field.field_name in ['date_add', 'date_end']:
                                if changed_field.field_name == 'date_add':
                                    date_add_values.append(revision.date_add)
                                    date_end_values.append(None)
                                    initiator = None
                                    if changed_field.field_editor == revision.author:
                                        initiator = 'manager'
                                    elif changed_field.field_editor == revision.author_task:
                                        initiator = 'performer'
                                    date_change_initiators.append(initiator)
                                elif changed_field.field_name == 'date_end':
                                    date_add_values.append(None)
                                    date_end_values.append(revision.date_end)
                                    initiator = None
                                    if changed_field.field_editor == revision.author:
                                        initiator = 'manager'
                                    elif changed_field.field_editor == revision.author_task:
                                        initiator = 'performer'
                                    date_change_initiators.append(initiator)
                    task.date_add_values = date_add_values
                    task.date_end_values = date_end_values
                    task.date_change_initiators = date_change_initiators

        context['department_tree'] = department_tree

        template_name = 'project/query/change_task_completion_terms/table.html'
        return render(request, template_name, context)

    def pie_chart():
        """Круговая диаграмма. Отображает процент измененных поручений руководителя от общего числа измененных
        поручений"""
        task_revisions = TaskRevision.objects.filter(
            (Q(changed_fields__field_name='date_add') | Q(changed_fields__field_name='date_end')),
            date_add__range=(date_from, date_to)
        )
        if filter_employees == 'division_managers':
            task_revisions = task_revisions.filter(author__userprofile__is_division_manager=True)
        elif filter_employees == 'department_managers':
            task_revisions = task_revisions.filter(
                (Q(author__userprofile__role=True) | Q(author__userprofile__role2=True)),
                author__userprofile__is_division_manager=False
            )
        task_hashes = task_revisions.order_by('hash_task').values_list('hash_task', flat=True).distinct()

        query_results = {}
        changed_tasks_total = len(task_hashes)
        if changed_tasks_total:
            for hash_task in task_hashes:
                try:
                    task = Task.task_manager.get(hash_task=hash_task, deleted=False)
                    author_name = task.author.get_profile().get_names()
                    if author_name not in query_results:
                        query_results[author_name] = 1
                    else:
                        query_results[author_name] += 1
                except Task.DoesNotExist:
                    pass
            for key in query_results:
                query_results[key] = float(query_results[key]) / changed_tasks_total * 100
                query_results[key] = round(query_results[key], 2)

        context['query_data'] = query_results
        context['changed_tasks_total'] = changed_tasks_total
        template_name = 'project/query/change_task_completion_terms/pie_chart.html'
        return direct_to_template(request, template_name, context)

    def bar_chart():
        tasks = Task.task_manager.filter(deleted=False, date_add__range=(date_from, date_to))
        user_ids = tasks.values_list('author', flat=True).order_by('author').distinct()
        users = User.objects.filter(pk__in=user_ids, is_active=True, userprofile__dismissed=False)
        if filter_employees == 'division_managers':
            users = users.filter(userprofile__is_division_manager=True)
        elif filter_employees == 'department_managers':
            users = users.filter(
                (Q(userprofile__role=True) | Q(userprofile__role2=True)),
                userprofile__is_division_manager=False
            )

        for user in users:
            user_tasks = tasks.filter(author=user)
            user.tasks_total = user_tasks.count()
            task_hashes = [row.hash_task for row in user_tasks]
            task_revisions = TaskRevision.objects.filter(
                (Q(changed_fields__field_name='date_add') | Q(changed_fields__field_name='date_end')),
                hash_task__in=task_hashes
            ).values('hash_task').order_by('hash_task').distinct()
            user.changed_tasks_count = task_revisions.count()

        context['users'] = users
        template_name = 'project/query/change_task_completion_terms/bar_chart.html'
        return direct_to_template(request, template_name, context)

    date_from = datetime.date.today()
    date_to = datetime.date.today()
    display_type = 'table'
    filter_employees = 'all'

    if request.GET.get('period'):
        filter_form = QueryTasksChangedCompletionTermsFilterForm(request.GET, initial={'period': 'today'})
        if filter_form.is_valid():
            date_from = filter_form.start
            date_to = filter_form.end
            display_type = filter_form.cleaned_data['display_type']
            filter_employees = filter_form.cleaned_data['employees']
    else:
        filter_form = QueryTasksChangedCompletionTermsFilterForm(initial={'period': 'today'})

    context = {
        'date_from': date_from,
        'date_to': date_to,
        'filter_form': filter_form,
    }

    if display_type == 'bar_chart':
        return bar_chart()
    elif display_type == 'pie_chart':
        return pie_chart()
    else:
        return table()


@login_required
def query_task_complexity_distribution(request):
    """Распределение трудоемкости поручений по сотрудникам"""
    display_type = 'table'
    date_from = None
    date_to = None
    date_all_time = False

    if request.GET.get('period'):
        filter_form = QueryTaskComplexityDistributionFilterForm(request.GET, initial={'period': 'today'})
        if filter_form.is_valid():
            date_from = filter_form.start
            date_to = filter_form.end
            display_type = filter_form.cleaned_data['display_type']
            if filter_form.cleaned_data['period'] == 'all_time':
                date_all_time = True
    if not date_from or not date_to:
        filter_form = QueryTaskComplexityDistributionFilterForm(initial={'period': 'today'})
        date_from = datetime.date.today()
        date_to = datetime.date.today()

    users = get_allowed_visible_users(request)
    total = {
        'division_managers': {
            'tasks_total': 0,
            'simple_tasks_count': 0,
            'complex_tasks_count': 0,
            'task_iterations_total': 0,
        },
        'managers': {
            'tasks_total': 0,
            'simple_tasks_count': 0,
            'complex_tasks_count': 0,
            'task_iterations_total': 0,
        },
        'specialists': {
            'tasks_total': 0,
            'simple_tasks_count': 0,
            'complex_tasks_count': 0,
            'task_iterations_total': 0,

        }
    }
    for user in users:
        tasks = Task.task_manager.assigned_to_user(user).filter(deleted=False, date_add__range=(date_from, date_to))
        user.tasks_total = tasks.count()
        user.simple_tasks_count = 0
        user.complex_tasks_count = 0
        task_hashes = []
        for task in tasks:
            task_hashes.append(task.hash_task)
            # TODO: Рассмотреть необходимость добавить дополнительное поле к задачам, содержащее количество ревизий,
            # чтобы не генерировать подзапрос для каждой задачи
            revisions = TaskRevision.objects.filter(hash_task=task.hash_task)
            # Если задача завершена и у нее ревизий с изменениями 2 или меньше, считаем ее простой
            if task.progress == 100 and revisions.count() <= 2:
                user.simple_tasks_count += 1
            # Если у задачи, независимо от ее завершения, накопилось больше 2 ревизий с изменениями,
            # считаем ее трудоемкой
            elif revisions.count() > 2:
                user.complex_tasks_count += 1

        if user.tasks_total > 0:
            x = float(user.tasks_total) / 100
            user.simple_tasks_percent = float(user.simple_tasks_count) / x
            user.simple_tasks_percent = round(user.simple_tasks_percent, 2)
            user.complex_tasks_percent = float(user.complex_tasks_count) / x
            user.complex_tasks_percent = round(user.complex_tasks_percent, 2)
        else:
            user.simple_tasks_percent = 0
            user.complex_tasks_percent = 0
        # Здесь вообще нужно считать ревизии, но так как они не создаются для уже существующих задач,
        # считаем количество задач, плюс кол-во ревизий с порядковым номером больше 1
        task_iterations_total = tasks.count()
        task_iterations_total += TaskRevision.objects.filter(hash_task__in=task_hashes, revision_number__gt=1).count()
        user.task_iterations_total = task_iterations_total

        # Средние значения
        if user.get_profile().is_division_manager:
            role = 'division_managers'
        elif user.get_profile().role or user.get_profile().role2:
            role = 'managers'
        else:
            role = 'specialists'
        total[role]['tasks_total'] += user.tasks_total
        total[role]['simple_tasks_count'] += user.simple_tasks_count
        total[role]['complex_tasks_count'] += user.complex_tasks_count
        total[role]['task_iterations_total'] += user.task_iterations_total

    #Считаем процент поручений для средних значений
    for role in ('division_managers', 'managers', 'specialists'):
        if total[role]['tasks_total']:
            x = float(total[role]['tasks_total']) / 100
            simple_tasks_percent = float(total[role]['simple_tasks_count']) / x
            simple_tasks_percent = round(simple_tasks_percent, 2)
            total[role]['simple_tasks_percent'] = simple_tasks_percent
            complex_tasks_percent = float(total[role]['complex_tasks_count']) / x
            complex_tasks_percent = round(complex_tasks_percent, 2)
            total[role]['complex_tasks_percent'] = complex_tasks_percent
        else:
            total[role]['simple_tasks_percent'] = 0
            total[role]['complex_tasks_percent'] = 0

    context = {
        'filter_form': filter_form,
        'date_from': date_from,
        'date_to': date_to,
        'users': users,
        'total': total,
        'date_all_time': date_all_time,
    }
    if display_type == 'bar_chart':
        template_name = 'project/query/task_complexity_distribution/bar_chart.html'
    else:
        template_name = 'project/query/task_complexity_distribution/table.html'
    return direct_to_template(request, template_name, context)


@login_required
def query_low_active_employees(request):
    """Список малоактивных сотрудников"""
    display_type = 'table'
    date_to = datetime.datetime.today()
    date_from = date_to - datetime.timedelta(days=6)
    employees = get_allowed_visible_users(request)

    if request.GET.get('period'):
        # TODO: Переделать эту форму, после мержа с веткой, в которой ее переделал Дмитрий
        filter_form = LowActiveEmployeesFiltersForm(request.GET, initial={'period': 'today',})
        if filter_form.is_valid():
            """
            if filter_form.cleaned_data['user']:
                employees = employees.filter(pk=filter_form.cleaned_data['user'].pk)
            if filter_form.cleaned_data['job']:
                employees = employees.filter(userprofile__job__icontains=filter_form.cleaned_data['job'])"""
            if filter_form.cleaned_data['period']:
                date_from = filter_form.start
                date_to = filter_form.end
            display_type = filter_form.cleaned_data['display_type']
        else:
            print "test"
            print filter_form.errors()
            filter_form = LowActiveEmployeesFiltersForm(initial={'period': 'today',})
    else:
        filter_form = LowActiveEmployeesFiltersForm(initial={'period': 'today',})

    dates_range = WorkTime.get_working_days_range(date_from, date_to)
    date_range_len = len(dates_range)  # Количество рабочих дней в диапазоне(для подсчета средней активности)

    total_attendance = 0
    exclude_employees = []
    # Исключаем сотрудников, у которых нет пропущенных дней
    for user in employees:
        user_attendance = UsersAttendDays.objects.filter(user=user, date__in=dates_range)
        if user_attendance.count() == date_range_len:
            exclude_employees.append(user.pk)
            total_attendance += user_attendance.count()
    employees = employees.exclude(pk__in=exclude_employees)

    #Подсчитываем у оставшихся процент посещаемости
    for user in employees:
        user_attendance = UsersAttendDays.objects.filter(user=user, date__in=dates_range)
        user.attendance_total = user_attendance.count()
        user.attendance_base = user_attendance.count()
        total_attendance += user.attendance_base
        user.attendance_of = date_range_len
        common_denominator = find_max_common_denominator(user.attendance_base, user.attendance_of)
        if common_denominator > 1:
            user.attendance_base = user.attendance_base / common_denominator
            user.attendance_of = user.attendance_of / common_denominator

    total_average_attendance_base = float(total_attendance) / employees.count()
    total_average_attendance_base = round(total_average_attendance_base, 2)
    total_average_attendance_of = date_range_len
    common_denominator = find_max_common_denominator(total_average_attendance_base, total_average_attendance_of)
    if common_denominator > 1:
        total_average_attendance_base = float(total_average_attendance_base) / common_denominator
        total_average_attendance_base = round(total_average_attendance_base, 2)
        total_average_attendance_of = total_average_attendance_of / common_denominator

    context = {
        'date_to': date_to,
        'date_from': date_from,
        'employees': employees,
        'dates_range': dates_range,
        'filter_form': filter_form,
        'total_average_attendance_base': total_average_attendance_base,
        'total_average_attendance_of': total_average_attendance_of,
        'display_type': display_type,
    }
    if display_type == 'bar_chart':
        template_name = 'project/query/low_active_employees/bar_chart.html'
    else:
        template_name = 'project/query/low_active_employees/table.html'
    return direct_to_template(request, template_name, context)


@login_required
def query_manager_tasks_control_percent(request):
    query_name = 'query_manager_tasks_control_percent'
    page_title = u"Процент проконтролированных поручений руководителем управления/отдела"

    # Набор доступных видом отчета (только таблица и круги)
    custom_choices = (('table', u'Таблица'),
                      ('pie', u'Круги'),
    )

    display_type = request.GET.get('display_type') or "table"
    period = request.GET.get('period') or "cur_month"

    date_start = datetime.datetime.now()
    date_end = date_start

    # Форма фильтров - даты + вид
    filters_form = DatePeriodsDisplayTypeFilters(
        custom_choices = custom_choices,
        initial = {
            'period': 'today',
            'display_type': 'table'
        })

    if request.GET:
        filters_form = DatePeriodsDisplayTypeFilters(custom_choices = custom_choices, initial=request.GET)
        if filters_form.is_valid():
            values = filters_form.cleaned_data
            date_start = filters_form.start
            date_end = filters_form.end
            display_type = values['display_type']

    users0 = get_allowed_visible_users(request)
    users = []
    total = 0
    tasks0 = Task.objects.filter(layer__range=(date_start, date_end), nomer__isnull=False)
    tasks_look = Task.objects.filter(layer__range=(date_start, date_end), nomer__isnull=False, author_look=True)

    tasks = []
    hashes = []

    for task in tasks0:
        if task.hash_task in hashes:
            pass
        else:
            hashes.append(task.hash_task)
            tasks.append(task)

    total = 0

    for user in users0:
        user.task_count = 0
        user.task_look_count = 0
        if user.get_profile().role == 1 or user.get_profile().role2 == 1:
            #  Все задачи
            for task in tasks:
                if task.author == user and task.author != task.author_task: #  and task.nomer.isdigit()
                    user.task_count += 1

            # Задачи просмотренные пставщиком
            for task in tasks_look:
                if task.author == user and task.author != task.author_task: #  and task.nomer.isdigit()
                    user.task_look_count += 1

            user.tasks_look_percent = 0
            user.tasks_unlook_percent = 100
            if user.task_look_count != 0:
                user.tasks_look_percent = findPercent(user.task_count, user.task_look_count)
                user.tasks_unlook_percent = 100 - user.tasks_look_percent

            total += user.task_count

            users.append(user)

        for user in users:
            if user.task_count != 0:
                user.all_tasks_percent = findPercent(total, user.task_count)
                if user.task_look_count != 0:
                    user.all_tasks_look_percent = findPercent(total, user.task_look_count)
                else:
                    user.all_tasks_look_percent = 0
                user.all_tasks_unlook_percent = user.all_tasks_percent - user.all_tasks_look_percent

    query_template = {
        'users': users,
        'page_title': page_title,
        'total': total,
        'type': display_type,
        'filters_form': filters_form,
        'period': period,
        'period_display': dict(constants.PERIOD_CHOICES)[period],
        'query_name': query_name,
        'is_test': is_test(),
    }

    # Кидаем на темплейты или для таблицы или для диаграмм
    if display_type in ["bar", "pie"]:
        return direct_to_template(request, 'project/graph/manager_tasks_control_percent.html', query_template)
    else:
        return direct_to_template(request, 'project/query/manager_tasks_control_percent.html', query_template)


def get_task_parent(arg_task, tasks):
    for task in tasks:
        if arg_task.nomer.isdigit():
            if str(task.number) == str(arg_task.nomer) and not (task.id == arg_task.id):
                return task
    return None


@login_required
def query_maximal_chain(request):
    users0 = get_allowed_visible_users(request)
    users = []
    today = datetime.datetime.today()
    tasks0 = Task.objects.filter(layer__range=[datetime.datetime.combine(today, datetime.time.min), datetime.datetime.now()], nomer__isnull=False)
    tasks = []
    processed_task_ids = []
    last_task_ids = []
    chains = []
    for task in tasks0:
        if task.id in processed_task_ids:
            continue
        chain = [task.id]
        processed_task_ids.append(task.id)
        parent = task
        while 1:
            parent = get_task_parent(parent, tasks0)
            if parent is None:
                chains.append(chain)
                last_task_ids.append(chain[-1])
                break
            if parent.id in last_task_ids:
                last_task_ids.remove(parent.id)
                last_task_ids.append(chain[-1])
                for i in range(len(chains)):
                    if chains[i][-1] == parent.id:
                        chains[i] = chains[i] + chain
                        break
            processed_task_ids.append(parent.id)
            chain.insert(0, parent.id)
    chains.sort(key=lambda chain: -len(chain))
    res = []
    for chain in chains[:10]:
        elt = {"task": Task.objects.get(id=chain[0]), "length": len(chain) - 1}
        subtasks = []
        for task_id in chain:
            subtasks.append(Task.objects.get(id=task_id))
        elt["subtasks"] = subtasks
        res.append(elt)
    return direct_to_template(request, 'project/query/maximal_chain.html', {
        'res': res,
        'page_title': u"Максимальные цепочки делегирования поручений",
        'query_name': "query_maximal_chain",
        'is_test': is_test(),
    })


@login_required
def query_task_fulfillment_report(request):
    """Динамика заполнения СКАИП сотрудниками"""
    display_type = 'table'
    date_from = date_to = datetime.datetime.today()
    employees = get_allowed_visible_users(request)

    if request.GET.get('period'):
        filter_form = TaskFulfillmentReportFilterForm(request.GET, initial={'period': 'today',})
        if filter_form.is_valid():
            display_type = filter_form.cleaned_data['display_type']
            date_from = filter_form.start
            date_to = filter_form.end
        else:
            filter_form = TaskFulfillmentReportFilterForm(initial={'period': 'today',})
    else:
        filter_form = TaskFulfillmentReportFilterForm(initial={'period': 'today',})

    for user in employees:
        # Общий список задач пользователя
        user_tasks = Task.task_manager.assigned_to_user(user).filter(
            deleted=False,
            date_add__range=(date_from, date_to)
        )
        user_tasks_count = user_tasks.count()
        user.tasks_total = user_tasks_count
        # Задачи, срок исполнения которых больше 1 дня
        user_tasks_duration_more_than_day = []
        for task in user_tasks:
            # Вычисляем рабочие дни в диапазоне начала и окончания задачи.
            work_days_range = WorkTime.get_working_days_range(task.date_add, task.date_end)
            # Если на выполнение задачи дано более одного дня(например, поставлена 1, дедлайн - 3 числа)
            if len(work_days_range) > 2:
                user_tasks_duration_more_than_day.append(task)
        user_tasks_count = user_tasks.count()
        # Процент задач со сроком исполнения больше 1 дня от общего числа задач
        user_tasks_duration_more_than_day_count = len(user_tasks_duration_more_than_day)
        user.tasks_more_than_day_count = user_tasks_duration_more_than_day_count
        if user_tasks.count() > 0:
            user.tasks_more_than_day_percent = int(float(user_tasks_duration_more_than_day_count) / (float(user_tasks_count) / 100))
        else:
            user.tasks_more_than_day_percent = 0
        #Задачи больше 1 дня, которые сразу были переведены в 100%
        user_tasks_duration_more_than_day_pks = [x.pk for x in user_tasks_duration_more_than_day]
        completed_tasks_more_than_day = user_tasks.filter(pk__in=user_tasks_duration_more_than_day_pks, progress=100)
        instantly_completed_count = 0
        for task in completed_tasks_more_than_day:
            task_revisions = task.get_task_revisions()
            # Если в истории изменений задачи есть только 2 разных progress(например, 0 и 100), считаем эту задачу
            # сразу переведенной из стартового значения progress в 100%
            if not task_revisions.values('progress').distinct().count() > 2:
                instantly_completed_count += 1
        instantly_completed_percent = 0
        if instantly_completed_count > 0:
            instantly_completed_percent = float(instantly_completed_count) / \
                                                (float(user.tasks_more_than_day_count) / 100)
            instantly_completed_percent = int(instantly_completed_percent)
        user.instantly_completed_count = instantly_completed_count
        user.instantly_completed_percent = instantly_completed_percent


    context = {
        'date_from': date_from,
        'date_to': date_to,
        'filter_form': filter_form,
        'employees': employees,
    }
    if display_type == 'bar_chart':
        template_name = 'project/query/query_task_fulfillment_report/bar_chart.html'
    else:
        template_name = 'project/query/query_task_fulfillment_report/table.html'
    return direct_to_template(request, template_name, context)


def get_relationship_count(task, user, task_list):
    # у поручения есть связь
    try:
        nomer_task = int(task[2])
        author_task = task[1]
    except TypeError:
        nomer_task = int(task['nomer'])
        author_task = task['author']
    except KeyError:
        nomer_task = int(task['nomer'])
        author_task = task['author']
    except ValueError:
        return
    if nomer_task:
        # получаем связанное поручение
        try:
            # link_user_task = Task.objects.filter(author_task=author_task, number=nomer_task)
            link_user_task = (task for task in task_list if
                              task['author_task_id'] == author_task and task['number'] == nomer_task)
        except UnicodeEncodeError:
            return
        except Task.DoesNotExist:
            return
        except ValueError:
            return
        if not link_user_task:
            return
        try:
            link_user_task = link_user_task.next()
        except StopIteration:
            return
            # если у связанного поручения есть связь, повторяем еще
        if link_user_task['nomer']:
            return get_relationship_count(link_user_task, user, task_list)
        else:
            # связи нет и функция запущена не с корневого элемента
            user.ref += 1
            return 1


@login_required
def search(request):
    get_user_task = [u.user.id for u in request.user.get_profile().get_user_profiles()]
    if request.GET.get('get_user'):
        get_user_task = [request.GET.get('get_user')]

    if request.GET.get('query_type') == 'project':
        search = Task.objects.filter(
            Q(title__icontains=request.GET.get('search-query')) | \
            Q(comment__icontains=request.GET.get('search-query')) | \
            Q(text__icontains=request.GET.get('search-query'))).filter(author_task__in=get_user_task, layer=datetime.datetime.now().today(), deleted=0)
    else:
        search = Task.objects.filter(
            Q(title__icontains=request.GET.get('search-query')) | \
            Q(comment__icontains=request.GET.get('search-query')) | \
            Q(text__icontains=request.GET.get('search-query'))).filter(author_task__in=get_user_task)

    return direct_to_template(request, 'project/search.html', {
        'date': datetime.datetime.date(datetime.datetime.now()),
        'query': request.GET.get('search-query'),
        'project': search,
        'is_test': is_test(),
    })


@login_required
# @require_http_methods(['POST'])
def tasks_bulk_action(request):
    '''
    Массовое изменение поручений.
    К сожалению не получилось использовать обычный сабмит формы
    (вместо этого js-скриптом приходится аяксом отправлять значения всех полей формы),
    потому что вставлять форму массовых операций приходится внутрь других форм (такие места шаблонов).
    '''
    action = request.POST.get('action', 'change_task_performer')
    if not action:
        return HttpResponseRedirect('/')

    try:
        action_view_function = eval('tasks_bulk_action_' + action)
        ids_str = request.POST.get('ids', '229090,229089')
        ids = ids_str.split(',')
        return action_view_function(ids, request);
    except:
        return HttpResponse('error')


def tasks_bulk_action_change_task_performer(ids, request):
    author_task_id = request.POST.get('author_task', 21)
    if not author_task_id:
        return HttpResponse('error')

    for pk in ids:
        task = Task.objects.get(pk=pk)
        task.author_task = User.objects.get(pk=author_task_id)
        task.save()
    else:
        return HttpResponse('ok')

    return HttpResponse('error')


def findPercent(all_value, part_value):
    return int(100 * float(part_value) / float(all_value))


class WorkTime(object):
    # Разница между датами с учетом рабочих часов и дней (а рабчасах)
    @classmethod
    def substraction(cls, start_date, end_date, city=u"Москва"):
        delta = timedelta(days=1)
        work_start_date = cls.date_to_worktime(start_date, city)
        work_end_date = cls.date_to_worktime(end_date, city)
        substraction = 0

        if work_start_date.day != work_end_date.day:
            substraction += cls.hours_to_end_work_day(work_start_date, city)
            substraction += cls.hours_from_start_work_day(work_end_date, city)

            for day in cls.date_span(work_start_date, work_end_date):
                if day.isoweekday() not in [6, 7]:
                    substraction += 8
        else:
            substraction = cls.hours_to_end_work_day(work_start_date) - cls.hours_to_end_work_day(work_end_date)

        return substraction

    # Привод к дате с учетом рабочих часов
    @staticmethod
    def date_to_worktime(date, city=u"Москва"):
        if city == u"Москва":
            if datetime.time(00, 00) <= date.time() <= datetime.time(10, 00):
                date = date.replace(hour=10, minute=00)
            if datetime.time(14, 00) <= date.time() <= datetime.time(15, 00):
                date = date.replace(hour=15, minute=00)
            if datetime.time(19, 00) <= date.time() <= datetime.time(23, 59):
                date = date.replace(hour=19, minute=00)
        elif city == u"Воронеж":
            if datetime.time(00, 00) <= date.time() <= datetime.time(9, 00):
                date = date.replace(hour=9, minute=00)
            if datetime.time(13, 00) <= date.time() <= datetime.time(14, 00):
                date = date.replace(hour=15, minute=00)
            if datetime.time(18, 00) <= date.time() <= datetime.time(23, 59):
                date = date.replace(hour=19, minute=00)
        return date

    #  Количество рабчасов от указанного времени до окончания рабдня
    @staticmethod
    def hours_to_end_work_day(date, city=u"Москва"):
        hours = 0
        if city == u"Москва":
            hours = date.replace(hour=19, minute=00) - date
            if datetime.time(10, 00) <= date.time() <= datetime.time(14, 00):
                hours = hours - timedelta(hours=1)
        elif city == u"Воронеж":
            hours = date.replace(hour=18, minute=00) - date
            if datetime.time(9, 00) <= date.time() <= datetime.time(13, 00):
                hours = hours - timedelta(hours=1)
        return hours.total_seconds() / 3600

    #  Количество рабчасов от начала рабдня до указанного времени
    @staticmethod
    def hours_from_start_work_day(date, city=u"Москва"):
        hours = 0
        if city == u"Москва":
            hours = date - date.replace(hour=10, minute=00)
            if datetime.time(15, 00) <= date.time() <= datetime.time(19, 00):
                hours = hours - timedelta(hours=1)
        if city == u"Воронеж":
            hours = date - date.replace(hour=19, minute=00)
            if datetime.time(14, 00) <= date.time() <= datetime.time(18, 00):
                hours = hours - timedelta(hours=1)
        return hours.total_seconds() / 3600

    # Подневный список дней от начально до конечно даты, без учета этих двух дат
    @staticmethod
    def date_span(start_date, end_date, delta=timedelta(days=1)):
        current_date = start_date.replace(hour=00, minute=00) + delta
        while current_date < end_date - delta:
            yield current_date
            current_date += delta

    @staticmethod
    def is_working_day(date):
        """Принимает дату, возвращает True, если это рабочий день или False, если выходной"""
        #TODO: Проверяются только субботы и воскресенья. Сделать поддержку праздников
        if date.weekday() == 5 or date.weekday() == 6:
            return False
        else:
            return True

    @staticmethod
    def get_working_days_range(date_from, date_to):
        """Принимает начальную и конечную даты, возвращает последовательность дней между этими датами, отбрасывая выходные дни"""
        result = []
        day_delta = datetime.timedelta(days=1)
        date = date_from
        while date <= date_to:
            if WorkTime.is_working_day(date):
                result.append(date)
            date = date + day_delta
        return result


def title_or_text_changed_tasks(request):
    '''
    Процент поручений, по которым вносились изменения в формулировку, после их просмотра Поручающим.
    '''
    # фильтр по периоду
    today = datetime.datetime.today()
    date_start = datetime.datetime(1900, 1, 1)
    date_end   = datetime.datetime(2900, 1, 1)
    filters_form = UsersStatFilters(initial = {
        'period': 'all_time',
    })
    if request.GET.get('period', None):
        filters_form = UsersStatFilters(request.GET)
        if filters_form.is_valid():
            date_start = filters_form.start
            date_end   = filters_form.end


    # получаем словарь 
    # { "id исполнителя": "количество поручений, по которым вносились изменения в формулировку по инициативе ПОРУЧАЮЩЕГО, после просмотра поручающим"}
    with_author_editor_hashes = Task.objects.select_related().filter(
        date_add__range = (date_start, date_end),
    ).filter(
        task_revision_task__changed_fields__field_name__in = ('title', 'text',),
        task_revision_task__revision_created__gt = F('action_task__date_create'),
        action_task__action = 'visit',
        action_task__user = F('author'),
    ).values_list('hash_task').distinct()#.annotate(count_tasks = Count('author'))
    with_author_editor_hashes = [m[0] for m in with_author_editor_hashes]
    with_author_editor = Task.objects.filter(
        hash_task__in = with_author_editor_hashes,
    ).values_list('author').annotate(count_tasks = Count('author_task'))
    common_with_author_editor_count = 0
    with_author_editor_count = {}
    for m in with_author_editor:
        with_author_editor_count.update({
            m[0]: m[1],
        })
        common_with_author_editor_count += m[1]

    # получаем словарь 
    # { "id исполнителя": "количество поручений, по которым вносились изменения в формулировку по инициативе ИСПОЛНИТЕЛЯ, после просмотра поручающим"}
    with_performer_editor_hashes = Task.objects.select_related().filter(
        date_add__range = (date_start, date_end),
    ).filter(
        task_revision_task__changed_fields__field_name__in = ('title', 'text',),
        task_revision_task__revision_created__gt = F('action_task__date_create'),
        action_task__action = 'visit',
        action_task__user = F('author_task'),
    ).values_list('hash_task').distinct()#.annotate(count_tasks = Count('author'))
    with_performer_editor_hashes = [m[0] for m in with_performer_editor_hashes]
    with_performer_editor = Task.objects.filter(
        hash_task__in = with_performer_editor_hashes,
    ).values_list('author_task').annotate(count_tasks = Count('author_task'))
    common_with_performer_editor_count = 0
    with_performer_editor_count = {}
    for m in with_performer_editor:
        with_performer_editor_count.update({
            m[0]: m[1],
        })
        common_with_performer_editor_count += m[1]

    # получаем словарь 
    # { "id исполнителя": "общее количество поручений"}
    all_tasks_hashes = Task.objects.filter(
        date_add__range = (date_start, date_end),
    ).values_list('hash_task').distinct()
    all_tasks_hashes = [m[0] for m in all_tasks_hashes]
    all_tasks_count_by_author = Task.objects.filter(
        hash_task__in = all_tasks_hashes,
    ).values_list('author_task').annotate(count_tasks = Count('author_task'))
    common_all_tasks_count = 0
    all_tasks_count = {}
    for m in all_tasks_count_by_author:
        all_tasks_count.update({
            m[0]: m[1],
        })
        common_all_tasks_count += m[1]

    user = request.user
    user_profile = request.user.get_profile()

    employers = user_profile.get_employers()

    node = TreeDepartment.objects.all()
    for tree in node:
        tree.with_author_editor_count = 0
        tree.with_performer_editor_count = 0
        tree.all_tasks_count = 0
        tree.users = []

        # список идентификаторов всех сотрудников данного отдела
        tree_users_ids = [u.user.id for u in tree.get_user_profiles()]

        # есть ли в этом отделе подчиненные для текущего пользователя
        tree.can_show_users = False
        for u in tree.get_user_profiles():
            # если данный пользователь является подчиненным для того, кто смотрит отчет, 
            # то показываем ему и сотрудников отдела и детальную инфу по данному сотруднику
            if u in employers:
                # можно показывать детальную информацию, например, ссылки на открытые поручения данного пользователя
                u.can_show_detail = True
                # можно показывать список всех сотрудников данного отдела
                tree.can_show_users = True
            else:
                # для самого себя показываем детальную информацию
                if u.user.id == request.user.id:
                    u.can_show_detail = True
                else:
                    u.can_show_detail = False
                if request.user.id in tree_users_ids or request.user.is_superuser:
                    tree.can_show_users = True
                else:
                    tree.can_show_users = False

            u.with_author_editor_count = with_author_editor_count.get(u.user.id, 0)
            u.with_performer_editor_count = with_performer_editor_count.get(u.user.id, 0)
            u.all_tasks_count = all_tasks_count.get(u.user.id, 0)

            u.with_author_editor_percent = 0 if u.all_tasks_count == 0 else ((u.with_author_editor_count * 100 / u.all_tasks_count) )
            u.with_performer_editor_percent = 0 if u.all_tasks_count == 0 else ((u.with_performer_editor_count * 100 / u.all_tasks_count))

            tree.with_author_editor_count += u.with_author_editor_count
            tree.with_performer_editor_count += u.with_performer_editor_count
            tree.all_tasks_count += u.all_tasks_count

            tree.users.append(u)
            # if u.user.id == 79:
            #     RRR_with_author_editor_count = u.with_author_editor_count
            #     RRR_with_author_editor_percent = u.with_author_editor_percent
            #     RRR_with_performer_editor_count = u.with_performer_editor_count
            #     RRR_with_performer_editor_percent = u.with_performer_editor_percent
            #     RRR_all_tasks_count = u.all_tasks_count
            #     assert False

        tree.with_author_editor_percent = 0 if tree.all_tasks_count == 0 else ((tree.with_author_editor_count * 100 / tree.all_tasks_count))
        tree.with_performer_editor_percent = 0 if tree.all_tasks_count == 0 else ((tree.with_performer_editor_count * 100 / tree.all_tasks_count))

    common_with_performer_editor_percent = 0 if common_all_tasks_count == 0 else ((common_with_performer_editor_count * 100 / common_all_tasks_count))
    common_with_author_editor_percent = 0 if common_all_tasks_count == 0 else ((common_with_author_editor_count * 100 / common_all_tasks_count))

    return direct_to_template(request, 'project/title_or_text_changed_tasks.html', {
        'filters_form': filters_form,
        'node': node,
        'common_with_performer_editor_percent': common_with_performer_editor_percent,
        'common_with_author_editor_percent': common_with_author_editor_percent,
        'common_all_tasks_count': common_all_tasks_count,
    })
