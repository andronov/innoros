#!/usr/bin/python
# -*- coding: utf-8 -*-
import csv
import os
import tempfile
import datetime
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseRedirect
from django.http import HttpResponse
import time
import xlwt
from django.utils.translation import ungettext, ugettext, ugettext_lazy as _

def export_as_xls(request, queryset):
    wb = xlwt.Workbook()
    ws0 = wb.add_sheet('0',  cell_overwrite_ok=True)
    col = 0
    borders = xlwt.Borders()
    borders.left = xlwt.Borders.DASHED
    borders.right = xlwt.Borders.DASHED
    borders.top = xlwt.Borders.DASHED
    borders.bottom = xlwt.Borders.DASHED
    borders.left_colour = 0x40
    borders.right_colour = 0x40
    borders.top_colour = 0x40
    borders.bottom_colour = 0x40
    style = xlwt.XFStyle() # Create Style
    style.borders = borders # Add Borders to Style

    ws0.write(0, col, u"Номер связи", style)
    ws0.write(0, col+1, u"№", style)
    ws0.write(0, col+2, u"Дата поручения", style)
    ws0.write(0, col+3, u"Название поручения", style)
    ws0.write(0, col+4, u"Подробности", style)
    ws0.write(0, col+5, u"Приоритет", style)
    ws0.write(0, col+6, u"% выполнения", style)
    ws0.write(0, col+7, u"Срок исполнения", style)
    ws0.write(0, col+8, u"Кем дано", style)
    ws0.write(0, col+9, u"Кому поручено", style)
    ws0.write(0, col+10, u"Оценка", style)
    ws0.write(0, col+11, u"Комментарий", style)
    ws0.col(0).width = 2000
    ws0.col(1).width = 3000
    ws0.col(2).width = 9000
    ws0.col(3).width = 10000
    ws0.col(4).width = 6000
    ws0.col(5).width = 6000
    ws0.col(6).width = 4000
    ws0.col(7).width = 4000
    ws0.col(8).width = 4000
    ws0.col(9).width = 4000
    ws0.col(10).width = 4000
    ws0.col(11).width = 4000
    col = 0
    row = 1
    for obj in queryset:
            ws0.write(row, col, obj.nomer, style)
            ws0.write(row, col+1, obj.number, style)
            ws0.write(row, col+2, unicode(obj.date_add), style)
            ws0.write(row, col+3, obj.title, style)
            ws0.write(row, col+4, obj.text, style)
            ws0.write(row, col+5, unicode(obj.get_priority_display()), style)
            ws0.write(row, col+6, unicode(obj.get_progress()), style)
            ws0.write(row, col+7, unicode(obj.date_end), style)
            if obj.author:
               ws0.write(row, col+8,  unicode(obj.author.get_profile().get_names()), style)
            else:
                ws0.write(row, col+8,  unicode(obj.author_task.get_profile().get_names()), style)
            if obj.performer:
               ws0.write(row, col+9,  unicode(obj.performer.get_profile().get_names()), style)
            else:
                ws0.write(row, col+9,  unicode(obj.author_task.get_profile().get_names()), style)
            ws0.write(row, col+10, unicode(obj.evaluation), style)
            ws0.write(row, col+11, unicode(obj.comment), style)
            row = row + 1

    fd, fn = tempfile.mkstemp()
    os.close(fd)
    wb.save(fn)
    fh = open(fn, 'rb')
    resp = fh.read()
    fh.close()
    response = HttpResponse(resp, mimetype='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=task.xls'
    return response

def export_as_csv_action(description="Export selected objects as CSV file",  fields=None, exclude=None, header=True):

    def export_as_csv(modeladmin, request, queryset):

        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % unicode(opts).replace('.', '_')

        writer = csv.writer(response)
        if header:
            writer.writerow(list(field_names))
        for obj in queryset:
            writer.writerow([unicode(getattr(obj, field)).encode("utf-8","replace") for field in field_names])
        return response
    export_as_csv.short_description = description
    return export_as_csv


def export_as_pdf(modeladmin, request, queryset):
    from reportlab.pdfgen import canvas
    from django.utils.text import truncate_words
    from reportlab.lib.pagesizes import A4
    from reportlab.pdfbase import pdfmetrics
    from reportlab.pdfbase.ttfonts import TTFont

    if not request.user.is_staff:
        raise PermissionDenied
    opts = modeladmin.model._meta
    response = HttpResponse(mimetype="application/pdf")
    response['Content-Disposition'] = 'attachment; filename=%s.pdf' % (unicode(opts).replace('.', '_'),)
    p = canvas.Canvas(response, pagesize=A4)
    pdfmetrics.registerFont(TTFont('Arial', 'Arial.ttf'))
    p.setFont('Arial', 8)
    x = 10
    y = 800
    p.drawString(x, y, u'Кем поручено:')
    p.drawString(x+60, y, u'Кому поручено:')
    p.drawString(x+120, y, u'Время завершения:')
    p.drawString(x+220, y, u'Краткое описание поручения:')
    for obj in queryset:
        y = y - 15
        p.drawString(x, y, unicode(obj.author))
        p.drawString(x+60, y, unicode(obj.performer))
        p.drawString(x+120, y, unicode(obj.date_end))
        p.drawString(x+220, y, truncate_words(obj.text, 10))
    p.showPage()
    p.save()
    return response

export_as_pdf.short_description = _('Export selected objects as PDF')
