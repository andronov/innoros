# -*- coding: utf-8 -*-
from django import template
from django.shortcuts import render_to_response, render
from django.template.loader import get_template, render_to_string
from django.template.context import Context
from project.forms import TasksBulkForm
import re


register = template.Library()

@register.simple_tag
def active(request, pattern):
    import re
    if re.search(pattern, request.path):
        return 'active'
    return ''

@register.filter
def lookup(l, key):
    return l[key]

@register.simple_tag
def tasks_bulk_form(request, *args):
    '''
    Выводит форму для массовых операций с поручениями
    '''

    form = TasksBulkForm(request = request)
    if not form.has_actions:
        return ''

    tpl = get_template('project/tasks_bulk_form.html')
    context = Context({
        'form' : form,
    })
    html = tpl.render(context)
    return html
    # return render(request, 'project/tasks_bulk_form.html', {
    #     'form': form,
    # })

@register.filter
def replace ( string, args ):
    search  = args.split(args[0])[1]
    replace = args.split(args[0])[2]

    return re.sub( search, replace, string )

