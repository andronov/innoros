# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from django.contrib.auth.models import User
from dateutil.relativedelta import *
from project.models import Task


def UserStatistic(request):
    try:
        a = Task.objects.select_related().filter(author_task = request.user, layer = datetime.datetime.now().today(),deleted = 0).count()
        b = Task.objects.select_related().filter(author_task = request.user, layer = datetime.datetime.now().today(), date_end = datetime.datetime.now().today(), deleted = 0).count()
        c = Task.objects.select_related().filter(author_task = request.user, layer = datetime.datetime.now().today(), date_end__lt = datetime.datetime.now().today(), deleted = 0).count()
        type_search = request.GET.get('query_type') or None
        return {
            'STAT_ALL': a,
            'STAT_TO_DAY': b,
            'STAT_LAST': c,
            "TYPE_search": type_search,
            }
    except :
        return {

        }
