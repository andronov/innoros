from django.conf.urls.defaults import *
from utils import *

urlpatterns = patterns('',
    # Task
    url(r'^debug', 'project.views.debug'),
    url(r'^$', 'project.views.front_page', name="front_page"),
    url(r'^my-dashboard/$', 'project.views.my_dashboard', name="my-dashboard-view"),
    url(r'^user-dashboard/(?P<user_id>\d+)/$', 'project.views.user_dashboard', name="user-dashboard-view"),
    url(r'^show_task/$', 'project.views.show_task', name="show-task"),
    url(r'^action_task/$', 'project.views.action_task', name="action_task"),
    url(r'^task/quick_edit/$', 'project.views.quick_edit', name="quick_edit"),
    url(r'^task/delete_task/$', 'project.views.delete_task', name="delete_task"),
    url(r'task/file_upload/$', 'project.views.upload_file', name='uploadify_upload'),
    url(r'task/file_delete/$', 'project.views.delete_file', name='uploadify_delete'),
    url(r'task/print/(?P<object_id>\d+)/$', 'project.views.print_page', name="print_page"),
    url(r'^tasks-bulk-action/$', 'project.views.tasks_bulk_action', name="tasks_bulk_action"),
    
    # History Uri
    url(r'^my-history/$', 'project.views.my_history', name="my-history-view"),
    url(r'^history/(?P<user_id>\d+)/$', 'project.views.user_history', name="user-history-view"),
    url(r'^save_attention_time/$', 'project.views.save_history_time'),
    url(r'^my-history_standalone/$', 'project.views.my_history', {"is_standalone": 1}, name="my-history_standalone" ),
    url(r'^history_standalone/$', 'project.views.user_history', {"is_standalone": 1}, name="user-history_standalone" ),
    url(r'^history/history_list/$', 'project.views.history_list', name="history_list"),
    url(r'^history/history_task_view/$', 'project.views.history_task_view', name="history_task_view"),
    url(r'^history-object/(?P<hash_task>\d+)/$', 'project.views.history_object', name="history_object"),
    
    # Reports Url
    url(r'^reports/$', 'project.views.reports', name="reports-view"),
    url(r'^reports/stat/$', 'project.views.report_stat', name="report_stat"),
    url(r'^reports/stat/open_tasks/$', 'project.views.report_stat_opened', name="report-open-tasks"),
    url(r'^reports/stat/employees/$', 'project.views.report_employees', name="report-employees"),
    url(r'^reports/stat/print_open_tasks/$', 'project.views.report_print_open_tasks', name="report-print-open-tasks"),
    
    # Query
    #url(r'query/0/$', 'project.views.query_tab_0', name="query_tab_0"),
    url(r'query/canceled_tasks/$', 'project.views.query_canceled_tasks', name="query_canceled_tasks"),
    url(r'query/closing_tasks/$', 'project.views.query_closing_tasks', name="query_closing_tasks"),
    url(r'query/completion_date_changes/$', 'project.views.query_completion_date_changes', name="query_completion_date_changes"),
    url(r'query/deleted_tasks/$', 'project.views.query_deleted_tasks', name="query_deleted_tasks"),
    url(r'query/load_distribution/$', 'project.views.query_load_distribution', name="query_load_distribution"),
	url(r'query/load_distribution/(\d+)$', 'project.views.query_load_distribution', name="query_load_distribution"),
    url(r'query/most_least_variable_task/$', 'project.views.query_most_least_variable_task', name="query_most_least_variable_task"),
    url(r'query/overdue_tasks/$', 'project.views.query_overdue_tasks', name="query_overdue_tasks"),
    url(r'query/overdue_tasks/view/$', 'project.views.query_overdue_tasks_view', name="query_overdue_tasks_view"),
    url(r'query/planning_errors/$', 'project.views.query_planning_errors', name="query_planning_errors"),
    url(r'query/start_date_changes/$', 'project.views.query_start_date_changes', name="query_start_date_changes"),
    url(r'query/task_count_dynamics/$', 'project.views.query_task_count_dynamics', name="query_task_count_dynamics"),
    url(r'query/tasks_delegation_level/$', 'project.views.query_tasks_delegation_level', name="query_tasks_delegation_level"),
    url(r'query/tasks_status_review/$', 'project.views.query_tasks_status_review', name="query_tasks_status_review"),
    url(r'query/tasks_value/$', 'project.views.query_tasks_value', name="query_tasks_value"),
	url(r'query/tasks_value/(\d+)$', 'project.views.query_tasks_value', name="query_tasks_value"),
    url(r'query/attention_distribution/$', 'project.views.query_attention_distribution', name="query_attention_distribution"),
    url(r'query/average_control_term/$', 'project.views.query_average_control_term', name="query_average_control_term"),
    url(r'query/query_task_complexity_distribution/$', 'project.views.query_task_complexity_distribution', name="query_task_complexity_distribution"),
    url(r'query/query_change_task_completion_terms/$', 'project.views.query_change_task_completion_terms', name="query_change_task_completion_terms"),
    url(r'query/low_active_employees/$', 'project.views.query_low_active_employees', name="query_low_active_employees"),
    url(r'query/query_tasks_changed_statement/$', 'project.views.query_tasks_changed_statement', name="query_tasks_changed_statement"),

    url(r'query/attention_distribution_time/$', 'project.views.query_attention_distribution_time', name="query_attention_distribution_time"),
    url(r'query/attention_distribution_time2/$', 'project.views.query_attention_distribution_time2', name="query_attention_distribution_time2"),
    url(r'query/manager_tasks_distribution/$', 'project.views.query_manager_tasks_distribution', name="query_manager_tasks_distribution"),
    url(r'query/manager_tasks_control_percent/$', 'project.views.query_manager_tasks_control_percent', name="query_manager_tasks_control_percent"),
    url(r'query/query_task_fulfillment_report/$', 'project.views.query_task_fulfillment_report', name="query_task_fulfillment_report"),

    url(r'query/subordinate_tasks_distribution/$', 'project.views.query_subordinate_tasks_distribution', name="query_subordinate_tasks_distribution"),
    url(r'query/maximal_chain/$', 'project.views.query_maximal_chain', name="query_maximal_chain"),
    url(r'query/timeliness/$', 'project.views.query_timeliness', name="query_timeliness"),
    url(r'query/timeliness/(\w+)$', 'project.views.query_timeliness', name="query_timeliness"),
    url(r'query/title_or_text_changed_tasks/$', 'project.views.title_or_text_changed_tasks', name="title_or_text_changed_tasks"),


    #Graph
    url(r'^graph/$', 'project.views.graph', name="graph"),
    url(r'^graph/1/$', 'project.views.graph_1', name="graph_1"),
    url(r'^graph/2/$', 'project.views.graph_2', name="graph_2"),
    url(r'^graph/subordinate_tasks_distribution/$', 'project.views.graph_subordinate_tasks_distribution', name="graph_subordinate_tasks_distribution"),
    
    # Search
    url(r'^search/$', 'project.views.search', name="project_search"),


)

if is_test():
    urlpatterns += (
        # Query
        url(r'query/13/$', 'project.views.query_tab_13', name="query_tab_13"),

        #Graph
        url(r'^graph/3/$', 'project.views.graph_3', name="graph_3"),
        url(r'^graph/4/$', 'project.views.graph_4', name="graph_4"),
        #url(r'^graph/5/$', 'project.views.graph_5', name="graph_5"),
    )
