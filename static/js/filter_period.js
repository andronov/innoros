
$(document).ready(function () {
    var $period = $('.form-filters__period'),
        $start = $('.form-filters__start-date'),
        $end = $('.form-filters__end-date'),
        DATE_FORMAT = 'yy-mm-dd';

    
    $start.default_datepicker();
    $end.default_datepicker();

    // скрываем поля дат, если выбран стандартный период
    if ($period.find(':selected').val() !== 'custom') {
        $start.parent().hide();
        $end.parent().hide();
    }

    // Если выбирают период "произвольный период", то показываем поля начала и конца периода
    $period.bind('change', function(event) {
        if ($period.find(':selected').val() !== 'custom') {
            $start.parent().hide();
            $end.parent().hide();
        }
        else {
            $start.parent().show();
            $end.parent().show();
        }
    });
});
