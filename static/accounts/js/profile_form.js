
$(document).ready(function () {
    // добавляем попап-календарь
    $(".profile-form__field.field-birthday input").no_future_datepicker();
    
    // темизируем чекбоксы
    $('.profile-form input[type="checkbox"]').customInput();

    // валидация
    $(".profile-form").validate({
        rules: {
            'user-first_name': {
                required: true
            },
            'profile-middle_name': {
                required: true
            },
            'user-last_name': {
                required: true
            },
            'profile-birthday': {
                required: true
            },
            'profile-job': {
                required: true
            },
            'profile-tree': {
                required: true
            },
        }
    });
});
