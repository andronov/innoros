/**
 * Массовые операции над задачами
 */
$(document).ready(function () {
    var $bulk_checkbox       = $('.bulk-checkbox'),
        $bulk_all_checkboxes = $('.bulk-checkbox_all'),
        processed_class      = 'tasks-bult-operations-processed',
        $bulk_form           = $('.tasks-bulk-form'),
        $action              = $bulk_form.find('select[name="action"]'),
        $bulk_form_ids_field = $bulk_form.find('.tasks-bulk-form__ids'),
        $submit              = $bulk_form.find('.bulk-form__submit'),
        ajax_url             = '/tasks-bulk-action/';

    bind_events();

    // скрываем поля, используемые специально для определенных видов действий
    hide_additional_fields();

    function bind_events() {
        $submit.bind('click', function(event) {
            var ids     = get_ids_of_all_checked(),
                ids_str = ids.join(',');
            if (ids_str === '') {
                alert("Поставьте галочки напротив задач, над которыми хотите произвести действие.");
            }
            $bulk_form_ids_field.val(ids_str);
            submit_bulk_form();
        });

        $action.bind('change', function(event) {
            // показываем поля формы, которые нужны именно для выбранного действия
            show_additional_field();
        });

        $bulk_all_checkboxes.bind('click', function() {
            if ($(this).attr('checked') === 'checked') {
                $bulk_checkbox.attr('checked', 'checked');
            }
            else {
                $bulk_checkbox.removeAttr('checked');
            }
        });
    };

    /**
     * Скрывает те поля формы, которые нужны лишь для определенного действия
     */
    function hide_additional_fields() {
      $bulk_form.find('.for-action').parent().hide();
    }

    /**
     * Показывает те поля формы, которые нужны лишь для выбранного действия
     */
    function show_additional_field() {
        // скрываем все поля, используемые специально для определенных видов действий
        hide_additional_fields();
        // и показываем лишь для выбранного действия
        var class_name = 'for-' + get_action();
        $bulk_form.find('.' + class_name).parent().show();
    }

    function get_action() {
        return $action.find(':selected').val();
    }

    function submit_bulk_form() {
        var action      = get_action(),
            ids         = $bulk_form_ids_field.val(),
            author_task = $bulk_form.find('select[name="author_task"] :selected').val();
        if (!action) {
            alert('Выберите действие.')
        }
        if (action === 'change_task_performer' && author_task === '') {
            alert('Выберите исполнителя.')
        }
        $.ajax({
            type: 'POST',
            url: ajax_url,
            data: {
                action      : action, 
                ids         : ids, 
                author_task : author_task,
            },
            success: function(data) {
                if (data === 'ok') {
                  alert('Действие выполнено успешно.')
                  location.reload();
                }
            },
        })
    }

    function get_object_id(elem) {
        return $(elem).attr('value');
    }

    function get_ids_of_all_checked() {
        var ids = [];
        $bulk_checkbox.filter(':checked').each(function(i, elem) {
            var obj_id = get_object_id(elem);
            if (obj_id) {
                ids.push(obj_id);
            }
        });
        return ids;
    }
});
