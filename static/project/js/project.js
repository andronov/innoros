function searchTable(inputVal) {
    var table = $('#stat_table tbody');
    table.find('tr').each(function (index, row) {
        var allCells = $(row).find('td');
        if (allCells.length > 0) {
            var found = false;
            allCells.each(function (index, td) {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text())) {
                    found = true;
                    return false;
                }
            });
            if (found == true)$(row).show(); else $(row).hide();
        }
    });
}

$(document).ready(function () {


    var DATE_FORMAT = 'yy-mm-dd';
    var MONTHNAME = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
    var DAY_NAMES = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
    // $('#scrollbar1').tinyscrollbar();
    $('a[data-toggle="tab"]').bind('shown.bs.tab', function() {
        // $('#scrollbar1').tinyscrollbar();
      $('.scrollbar-container').tinyscrollbar();
    });

    $('input[name=alls]').click(function () {
        if ($('td input.checkbox:checked').val()) {
            $('td input.checkbox').removeAttr("checked");
            $('input[name=alls]').removeAttr("checked");
        } else {
            $('td input.checkbox').attr("checked", "checked");
            $('input[name=alls]').attr("checked", "checked");
        }
    });

    $('#select_employer select').change(function () {
        // $('#select_employer').submit();
        var $selected = $(this).find(':selected');
        url = $selected.attr('data-url');
        console.log(url);
        document.location = url;
    });

    $.validator.addMethod('not_equal_title', function (value, element) {
        return value !== $(element).parents('form').eq(0).find('#id_title').val();
    }, 'Поле "Подробности" не должно содержать только название поручения! Добавьте подробное описание.');
    $.validator.addMethod('not_equal_edit_title', function (value, element) {
        return value !== $(element).parents('form').eq(0).find('input#id_edit-title').val();
    }, 'Поле "Подробности" не должно содержать только название поручения! Добавьте подробное описание.');

    $.validator.addMethod('required_if_title_changed', function (value, element) {
        var $title = $(element).parents('form').eq(0).find('#id_edit-title');
        var title_initial = $title.attr('data-initial');
        var title_value = $title.val();
        if (title_value === title_initial) {
          return true;
        }
        return value !== '';
    }, 'Поле "Инициатор изменения" обязательно при изменении названия.');

    $.validator.addMethod('required_if_text_changed', function (value, element) {
        var $text = $(element).parents('form').eq(0).find('#id_edit-text');
        var text_initial = $text.attr('data-initial');
        var text_value = $text.val();
        if (text_value === text_initial) {
          return true;
        }
        return value !== '';
    }, 'Поле "Инициатор изменения" обязательно при изменении описания задачи.');

    $.validator.addMethod('required_if_date_end_changed', function (value, element) {
        var $date_end = $(element).parents('form').eq(0).find('input#id_edit-date_end');
        var date_end_initial = $date_end.attr('data-initial');
        var date_end_value = $date_end.val();
        if (date_end_value === date_end_initial) {
          return true;
        }
        return value !== '';
    }, 'Поле "Инициатор изменения" обязательно при изменении срока исполнения.');


//    Prevent multiple submishion of Add and Edit Task Form
    $("#AddTaskForm, form.show_task").submit(function () {
        var task_form = $(this);
        if (task_form.valid()) {
            task_form.submit(function () {
                return false;
            });
        }
        return true;
    });

//    Add and Show Task Forms Validation rules
    $("#AddTaskForm").validate({
        rules: {
            title: {
                required: true
            },
            date_add: {
                required: true
            },
            date_end: {
                required: true
            },
            text: {
                required: true,
                not_equal_title: true,
            },
            performer: {
                required: true
            },
            author: {
                required: true
            }
        }
    });

    $('#Add').live("click", function (event) {
        event.preventDefault();
        event.stopPropagation();

        $('#AddTask').modal({
            backdrop: false
        });
    });

    $('#AddTask #id_author').change(function () {
        if ($(this).val()) {
            $('#id_performer').attr('disabled', 'disabled');
        } else {
            $('#id_performer').removeAttr('disabled');
        }
    });
    $('#AddTask #id_performer').change(function () {
        if ($(this).val()) {
            $('#id_author').attr('disabled', 'disabled');
        } else {
            $('#id_author').removeAttr('disabled');
        }
    });

    $("#AddTask #id_date_add").default_datepicker();
    $("#AddTask #id_date_end").default_datepicker();

    $('#task_detail #id_author').live('change', function () {
        if ($(this).val()) {
            $('#task_detail #id_performer').attr('disabled', 'disabled');
        } else {
            $('#task_detail #id_performer').removeAttr('disabled');
        }
    });
    $('#task_detail #id_performer').live('change', function () {
        if ($(this).val()) {
            $('#task_detail #id_author').attr('disabled', 'disabled');
        } else {
            $('#task_detail #id_author').removeAttr('disabled');
        }
    });
    $(".alert").alert();

    $("#datepicker").datepicker(
        {
            dateFormat: DATE_FORMAT,
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            changeMonth: true,
            changeYear: true,
            firstDay: 1,
            maxDate: '0',

            onSelect: function (dateText, inst) {
                $('div.loader').show("slow");
                var GET_USER = $("#datepicker").attr('var');
                console.log('0');
                $.get("/history/history_list/", { date: dateText, get_user: GET_USER },
                    function (data) {
                        $('div.loader').hide("slow");
                        $('#history_list').html(data);
                        $('.scrollbar-container').tinyscrollbar();
                        // если не добавлять запуск после паузы, то низ контейнера не отображается.
                        setTimeout(function() {
                            $('.scrollbar-container').tinyscrollbar();
                        }, 600);
                    });
            }
        }
    );

    function validate_task_edit_form($elem) {
        $elem.validate({
            rules: {
                'edit-title': {
                    required: true
                },
                'edit-date_add': {
                    required: true
                },
                'edit-date_end': {
                    required: true
                },
                'edit-text': {
                    required: true,
                    not_equal_edit_title: true,
                },
                'edit-performer': {
                    required: true
                },
                'edit-author': {
                    required: true
                },
                'edit-title_editor': {
                    required_if_title_changed: true,
                },
                'edit-text_editor': {
                    required_if_text_changed: true,
                },
                'edit-date_end_editor': {
                    required_if_date_end_changed: true,
                },
            }
        });
    }

    $('.task_list tr td.number').live("click", function () {
        $('#task_detail').html("<center><h1>Загрузка...</h1></center>");
        var task_id = $(this).parents().attr('rel');
        if ($(this).parents().attr('role') == 'history') {
            var URL_GET = '/history/history_task_view/';
        } else {
            var URL_GET = '/show_task/';
        }
        var GET_USER = $(this).parent().attr('var');
        $(this).parent().parent().find('.hovers').removeClass('hovers');
        $(this).parent().addClass('hovers');

        $.get(URL_GET, { obj_id: task_id, get_user: GET_USER},
            function (data) {
                $('#task_detail').html(data);

                if (URL_GET == '/show_task/') {

                    var author = $('#task_detail #id_edit-author');
                    var performer = $('#task_detail #id_edit-performer');

                    if (performer.val()) {
                        author.attr('disabled', 'disabled');
                    } else {
                        performer.attr('disabled', 'disabled');
                    }


                    $("#task_detail #id_edit-date_add").default_datepicker();
                    $("#task_detail #id_edit-date_end").default_datepicker();
                    validate_task_edit_form($(".show_task"));

                    // при изменении некоторых полей делаем обязательным поле "инициатор изменения" для соответствующего поля
                    bind_editor_field('title');
                    bind_editor_field('text');
                    bind_editor_field('date_end');

                    function bind_editor_field(field_name) {
                      $('#id_edit-' + field_name).bind('change keypress', function(event) {
                        var $form = $(this).parents('.show_task');
                        var $field_editor = $form.find('.task-form__field.field-' + field_name + '_editor');
                        var $label_editor = $field_editor.find('label');
                        if ($label_editor.find('.required').length === 0) {
                          $field_editor.addClass('field-required');
                          $label_editor.append('<span class="required">*</span>');
                        }
                      });
                    }

                    $('.task-history__items').tinyscrollbar();


                    $('.file_list tr:even td').css('background-color', '#fff');

                    if ($('input[name=edit-terminated]:checked').val() == 'on') {
                        $('#task_detail tr#terminated_text').show();
                    } else {
                        $('#task_detail tr#terminated_text').hide();
                    }


                    $('input[name=edit-terminated]').change(function () {
                        if ($('#task_detail td.terminated input:checked').val() == 'on') {
                            $('#task_detail tr#terminated_text').show();
                        } else {
                            $('#task_detail tr#terminated_text').hide();
                        }
                    });
                }
            });
    });
    /*
     // Print selected tasks. Bug in Chrome - opens only one tab.
     $('input[name=print]').click(function(){
     $('input.checkbox:checked').each(function(){
     var task_id = $(this).val();
     window.open("/task/print/"+ task_id, "_blank");
     });
     });*/

    $(".task_list").tablesorter({
        dateFormat: 'ddmmyyyy'
    });


    $('label[for=id_department-manager]').append('<font style=color:red>*</font>');

    $('#search_table').keyup(function () {
        searchTable($(this).val());
    });


    $('.edit_feedback').live("click", function () {
        $('.feedback_area').html('<textarea name="feedback_area" id="id_feedback_area"></textarea><br><input class="btn btn-small" id="id_feedback_submit" type="submit" value="Сохранить">');
        $('.edit_feedback').hide();
    });
    $('#id_feedback_submit').live("click", function () {
        $('.edit_feedback').show();
        var val = $('#id_feedback_area').val();
        $('.feedback_area').html(val);
        $.get("/ajax_edit/", { feedback_edit: val});
    });

    $('.edit_kadr').live("click", function () {
        $('.kadr_area').html('<textarea name="kadr_area" id="id_kadr_area"></textarea><br><input class="btn btn-small" id="id_kadr_submit" type="submit" value="Сохранить">');
        $('.edit_kadr').hide();
    });
    $('#id_kadr_submit').live("click", function () {
        $('.edit_kadr').show();
        var val = $('#id_kadr_area').val();
        $('.kadr_area').html(val);
        $.get("/ajax_edit/", { kadr_edit: val, kadr_edit_id: $('.kadr_area').attr('rel')});
    });

    $("#date_query_7").datepicker(
        {
            dateFormat: DATE_FORMAT,
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            changeMonth: true,
            changeYear: true,
            firstDay: 1,

            onSelect: function (dateText, inst) {
                var GET_USER = $("#otd").val();
                $('div.loader').show("slow");
                $.get("/query/closing_tasks/", { date: dateText, get_user: GET_USER},
                    function (data) {
                        $('div.loader').hide("slow");
                        $('#history_list').html(data);
                        $('.scrollbar-container').tinyscrollbar();
                        // если не добавлять запуск после паузы, то низ контейнера не отображается.
                        setTimeout(function() {
                            $('.scrollbar-container').tinyscrollbar();
                        }, 600);
                    });
            }
        }
    );


    $('.get_proc_task').live("click", function () {
        var USER = $(this).data('user');
        $.get("/query/8/view/", { year: $(this).data('year'), type: $(this).data('month'), user: USER}).done(function (data) {
            $('.proc_task_' + USER).html(' <a href="javascript:void()"  class="proc_task_close">закрыть</a>');
            $('.proc_task_' + USER).show();
            $('.proc_task_' + USER).append(data);
        });
    });
    $('.proc_task_close').live("click", function () {
        $(this).parents('.proc_task').hide();
    });

    $("#stat_table").chromatable({
        width: "100%",
        height: "600px",
        scrolling: "yes"
    });

    $('.scrollbar-container').tinyscrollbar();
    // если не добавлять запуск после паузы, то низ контейнера не отображается.
    setTimeout(function() {
        $('.scrollbar-container').tinyscrollbar();
    }, 500);
})
;
