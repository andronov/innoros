// Powerd by JavaScript Print System - Codigy (info@codigy.ru)
jQuery.fn.print = function(){
    if (this.size() > 1){
        this.eq( 0 ).print();
        return;
    } else if (!this.size()){
        return;
    }
    var strFrameName = ("printer-" + (new Date()).getTime());
    var jFrame = $( "<iframe name='" + strFrameName + "'>" );
    jFrame
        .css("width", "1px")
        .css("height", "1px")
        .css("position", "absolute")
        .css("left", "-9999px")
        .appendTo( $( "body:first" ) )
    ;
    var objFrame = window.frames[ strFrameName ];
    var objDoc = objFrame.document;
    var jStyleDiv = $( "<div>" ).append($( "style" ).clone());
    var jLink = $( "<div>" ).append($( "link" ).clone());


    objDoc.open();
    objDoc.write( "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" );
    objDoc.write( "<html>" );
    objDoc.write( "<head>" );
    objDoc.write( "<title>" );

    objDoc.write( document.title );
    objDoc.write( "</title>" );
	objDoc.write( "<style> ._thead{display:none !important} img {display:none !important} .print-braek {page-break-before:always; } </style>" );
    objDoc.write( jLink.html() );

    objDoc.write( jStyleDiv.html() );
    objDoc.write( "</head>" );
    objDoc.write( "<body id='print'>" );
	objDoc.write( "<div>"+this.html() + "</div>");
    objDoc.write( "</body>" );
    objDoc.write( "</html>" );
    objDoc.close();
    objFrame.focus();
    objFrame.print();
    setTimeout(
        function(){
            jFrame.remove();
        },
        (10 * 1000)
    );
}