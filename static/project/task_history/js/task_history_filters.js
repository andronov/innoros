
$(document).ready(function () {
    var $filters_containers = $('.task-history__filter-container');
    var filter_checkbox = '.task-history__filter';
    var $filters_checkboxes = $('.task-history__filter');
    var $history_items = $('.task-history__item');

    /**
     * Отображает лишь те ревизии, которые содержат правки согласно выставленным галочкам.
     */
    function filter_history_items() {
        $filters_checkboxes.each(function(i, elem) {
            var target_class = $(elem).attr('target');
            if ($(elem).attr('checked')) {
                $history_items.filter('.'+target_class).show();
            }
            else {
                $history_items.filter('.'+target_class).hide();
            }
        });
    }

    $filters_containers.bind('click', function() {
        // фильтруем историю, согласно выставленным галочкам фильтров.
        filter_history_items();
    });

    // темизируем чекбоксы
    $filters_checkboxes.customInput();
});
