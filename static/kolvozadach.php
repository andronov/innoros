<?
//phpinfo();
$bd_host = "localhost";
$bd_user = "system_user";
$bd_password = "GK1DyBUe";
$bd_base = "system";
//$url = "http://127.0.0.1";
$con = mysql_connect($bd_host, $bd_user, $bd_password); mysql_select_db($bd_base, $con);$mysql_queries++;
if (!$con) {
    die('Ошибка соединения: ' . mysql_error());
}

//mysql_query("set names 'cp1251'");
//mysql_query ("set character_set_client='cp1251'");
//mysql_query ("set character_set_results='cp1251'");
//mysql_query ("set collation_connection='cp1251_general_ci'");
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Электронный учет поручений</title>
    <meta name="author" content="Codi">
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link href="/static/project/css/bootstrap.css" rel="stylesheet">
    <link href="/static/project/css/project.css" rel="stylesheet">
    <link href="/static/project/css/bootstrap-responsive.css" rel="stylesheet">
     <style type="text/css" media="print">
      .pagebreak
      {
        page-break-after: always;
        page-break-inside: avoid;
       height: 330px;

      }
    </style>
    <link href="/static/project/jui/css/hot-sneaks/jquery-ui-1.8.17.custom.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="/static/project/jui/jquery-ui-1.8.17.custom.min.js"></script>
    <script src="/static/project/js/jquery.tinyscrollbar.min.js"></script>
    <script src="/static/project/js/bootstrap.min.js"></script>
    <script src="/static/project/js/jquery.validate.js"></script>
    <script src="/static/project/js/jquery.formset.js"></script>
    <script src="/static/project/js/jquery.tablesorter.js" type="text/javascript"></script>
    <script src="/static/project/js/jquery.chromatable.js" type="text/javascript"></script>
	<script src="/static/project/js/print.js"></script>
    <script src="/static/project/js/project.js"></script>
	<script type="text/javascript">
	 $(document).ready(function() {
	    $(".print").click(function () {
              $("#itogiwrapper").print();
             return (false);
         });
        });
	</script>
</head>
<body>
<header>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse">

                <nav>
                <ul class="nav">
                         <li class=""><a href="/">Поручения</a></li>
                         <li class=""><a href="/history/">Истории</a></li>

                             <li class=""><a href="/reports/stat/">Отчет по сотрудникам</a>
                                 </li>


                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown"href="#">Запросы <b class="caret"></b></a>
                            <ul class="dropdown-menu" style="max-width: 400px;">
                                <li><a href="/reports/stat/graph/" > <i class="icon-signal"></i> Графики</a></li>
                                <li class="divider"></li>
                                <li><a href="/static/kolvozadach.php" ><i class="icon-th-large"></i> Степень загруженности сотрудников и подразделений</a></li>
                                <li><a href="/static/chislootmen.php" ><i class="icon-th-large"></i> Учет отмененных поручений</a></li>
                                <li><a href="/static/deletstat.php" ><i class="icon-th-large"></i> Учет удаленных поручений</a></li>
                                <li><a href="/reports/stat/tab/wave/" ><i class="icon-th-large"></i> Уровень делегирования поручений</a></li>

                                <li><a href="/reports/stat/tab/pr/" ><i class="icon-th-large"></i> Изменения сроков начала исполнения поручений</a></li>
                                 <li><a href="/reports/stat/tab/pr/on/" ><i class="icon-th-large"></i> Изменения сроков завершения исполнения поручений</a></li>
                                <li><a href="/reports/stat/tab/end/" ><i class="icon-th-large"></i> Обзор состояния поручений сотрудников</a></li>

                            </ul>

                </ul>

                    <form class="navbar-search pull-left" action="/search/">
                        <input type="text" class="search-query" placeholder="Поиск по названию" name="search-query">


                    </form>

                <ul class="nav pull-right">
                    <li><a href="/users/profile/root/">Моя карточка</a></li>
                    <li class="divider-vertical"></li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">root <b class="caret"></b></a>
                        <ul class="dropdown-menu">

                            <li><a href="/users/add_invaite/">Отправить приглашение</a></li>
                            <li><a href="/users/employers/">Управление сотрудниками</a></li>
                            <li><a href="/users/departments/">Подразделения и отделы</a></li>
                            <li><a href="/admin/">Панель управления</a></li>

                            <li><a href="/users/edit/">Настройки профиля</a></li>
                            <li><a href="/users/logout/">Выход</a></li>
                        </ul>
                    </li>
                </ul>

                </nav>

            </div>
        </div>
    </div>
</div>
</header>
<section>
<div class="container-fluid">

</div>
</section>
<section>
<!--
    <div class="container-fluid">
        <h3 style="color:#ce0000;float: left;">Рутовский Алексей Геннадьевич</h3>

            <div style="float: right;display: table;">
                <h4 style="float: left; margin-right: 10px;">Всего: 3</h4>
                <h4 style="float: left; margin-right: 10px;">На сегодня: 0</h4>
                <h4 style="float: left; margin-right: 10px;">Просрочено: 2</h4>
            </div>

    </div>
    -->
</section>

    <script type="text/javascript">
    $(document).ready(function() {

        $('#type').change(function() {
            $('#zp').submit();
             $('#zp1').submit();
        });
         $('#yers').change(function() {
            $('#zp1').submit();
            $('#zp').submit();
        });
    });
    </script>
    <div class="container-fluid">
        <div class="page-header">

        <?

        /*
        SELECT *
FROM `project_task`
WHERE `layer` = '2012-12-20'
AND `deleted` =0
AND `terminated` =0
ORDER BY `project_task`.`id` DESC
LIMIT 0 , 30

SELECT COUNT(layer) AS AllCountTask
FROM `project_task`
WHERE `layer` = '2012-12-20'
AND `deleted` =0
AND `terminated` =0

        */
        if($_GET['type']||$_GET['yers'])
        {
         if($_GET['yers']==date('Y',time())&& $_GET['type']==date('m',time()))
          $now_time=date('Y-m-d',time()-86400);//текущее время
          else
           $now_time=''.$_GET['yers'].'-'.$_GET['type'].'-30';
        }
        else
         $now_time=date('Y-m-d',time()-86400);//текущее время


   // echo  $now_time;
$sql="SELECT COUNT(layer) AS AllCountTask FROM `project_task` WHERE `layer` = '$now_time' AND `deleted` =0 AND `terminated` =0";
$res=mysql_query($sql);
$res=mysql_fetch_array($res);

$AllCountTask=$res['AllCountTask'];//все задачи на данный момент на сегодня




function task_user_id($author_task_id)
{

if($_GET['type']||$_GET['yers'])
        {
         if($_GET['yers']==date('Y',time())&& $_GET['type']==date('m',time()))
          $now_time=date('Y-m-d',time()-86400);//текущее время
          else
           $now_time=''.$_GET['yers'].'-'.$_GET['type'].'-30';
        }
        else
         $now_time=date('Y-m-d',time()-86400);//текущее время
$sql="SELECT COUNT(layer) AS TaskId
FROM `project_task`
WHERE `author_task_id` =$author_task_id
AND `layer` = '$now_time' AND `deleted` =0 AND `terminated` =0
ORDER BY `project_task`.`number` ASC";
$res=mysql_query($sql);
$res=mysql_fetch_array($res);
$Task_User_Id=$res['TaskId'];
return $Task_User_Id;
}

function task_some_id($some){$Task_User_Id=0;

$some_new=explode(",", $some);
//$some_new=$some;
if($_GET['type']||$_GET['yers'])
        {
         if($_GET['yers']==date('Y',time())&& $_GET['type']==date('m',time()))
          $now_time=date('Y-m-d',time()-86400);//текущее время
          else
           $now_time=''.$_GET['yers'].'-'.$_GET['type'].'-30';
        }
        else
         $now_time=date('Y-m-d',time()-86400);//текущее время
//echo $some;
foreach ($some_new as $author_task_id)
{
//echo $author_task_id;
$sql="SELECT COUNT(layer) AS TaskId
FROM `project_task`
WHERE `author_task_id` =$author_task_id
AND `layer` = '$now_time'AND `deleted` =0 AND `terminated` =0
ORDER BY `project_task`.`number` ASC";
$res=mysql_query($sql);
$res=mysql_fetch_array($res);
$Task_User_Id+=$res['TaskId'];

}

return $Task_User_Id;}









function task_some_id_min($some){$Task_User_Id=0;

$some_new=explode(",", $some);
//$some_new=$some;
if($_GET['type']||$_GET['yers'])
        {
         if($_GET['yers']==date('Y',time())&& $_GET['type']==date('m',time()))
          $now_time=date('Y-m-d',time()-86400);//текущее время
          else
           $now_time=''.$_GET['yers'].'-'.$_GET['type'].'-30';
        }
        else
         $now_time=date('Y-m-d',time()-86400);//текущее время
//echo $some;
foreach ($some_new as $author_task_id)
{
//echo $author_task_id;
$sql="SELECT COUNT(layer) AS TaskId
FROM `project_task`
WHERE `author_task_id` =$author_task_id
AND `layer` = '$now_time'AND `deleted` =0 AND `terminated` =0
ORDER BY `project_task`.`number` ASC";
$res=mysql_query($sql);
$res=mysql_fetch_array($res);
$Task_User_Id_ar[]=$res['TaskId'];
//echo $res['TaskId'].'<br>';
}
return min($Task_User_Id_ar);
}


function task_some_id_max($some){
$Task_User_Id=0;

$some_new=explode(",", $some);
//$some_new=$some;
if($_GET['type']||$_GET['yers'])
        {
         if($_GET['yers']==date('Y',time())&& $_GET['type']==date('m',time()))
          $now_time=date('Y-m-d',time()-86400);//текущее время
          else
           $now_time=''.$_GET['yers'].'-'.$_GET['type'].'-30';
        }
        else
         $now_time=date('Y-m-d',time()-86400);//текущее время
//echo $some;
foreach ($some_new as $author_task_id)
{
//echo $author_task_id;
$sql="SELECT COUNT(layer) AS TaskId
FROM `project_task`
WHERE `author_task_id` =$author_task_id
AND `layer` = '$now_time'AND `deleted` =0 AND `terminated` =0
ORDER BY `project_task`.`number` ASC";
$res=mysql_query($sql);
$res=mysql_fetch_array($res);
$Task_User_Id_ar[]=$res['TaskId'];
}
return max($Task_User_Id_ar);
}





$otd_010101_min=task_some_id_min('84,35');
$otd_010102_min=task_some_id_min('89,81');
$otd_010103_min=task_some_id_min('31,85');
$otd_010104_min=task_some_id_min('21,30,27,25');
$otd_0102_min=task_some_id_min('51,52,56,55,53,54');
$otd_0103_min=task_some_id_min('64');
$otd_0201_min=task_some_id_min('83,82,66,67,87');
$otd_0301_min=task_some_id_min('57,58,62,61,59,60,63');
$otd_0302_min=task_some_id_min('68,69,28,70,71');
$otd_0401_min=task_some_id_min('39,40,79');
$otd_0402_min=task_some_id_min('41,43,42');
$otd_05_min=task_some_id_min('47,65,48,50,49');
$otd_06_min=task_some_id_min('86,72,37,74,36,73');
$otd_07_min=task_some_id_min('20,15,7,6,16');
$otd_08_min=task_some_id_min('44,46,78,45');



$otd_010101_max=task_some_id_max('84,35');
$otd_010102_max=task_some_id_max('89,81');
$otd_010103_max=task_some_id_max('31,85');
$otd_010104_max=task_some_id_max('21,30,27,25');
$otd_0102_max=task_some_id_max('51,52,56,55,53,54');
$otd_0103_max=task_some_id_max('64');
$otd_0201_max=task_some_id_max('83,82,66,67,87');
$otd_0301_max=task_some_id_max('57,58,62,61,59,60,63');
$otd_0302_max=task_some_id_max('68,69,28,70,71');
$otd_0401_max=task_some_id_max('39,40,79');
$otd_0402_max=task_some_id_max('41,43,42');
$otd_05_max=task_some_id_max('47,65,48,50,49');
$otd_06_max=task_some_id_max('86,72,37,74,36,73');
$otd_07_max=task_some_id_max('20,15,7,6,16');
$otd_08_max=task_some_id_max('44,46,78,45');



$pod_0101_min=task_some_id_min('84,35,89,81,31,85,21,30,27,25,80');
$pod_01_min=task_some_id_min('84,35,89,81,31,85,21,30,27,25,80,11,51,52,56,55,53,54,64');
$pod_02_min=task_some_id_min('33,83,82,66,67,87');
$pod_03_min=task_some_id_min('32,68,69,28,70,71,57,58,62,61,59,60,63');
$pod_04_min=task_some_id_min('38,41,43,42,39,40,79');
$pod_05_min=$otd_05_min;
$pod_06_min=$otd_06_min;
$pod_07_min=$otd_07_min;
$pod_08_min=$otd_08_min;

$pod_0101_max=task_some_id_max('84,35,89,81,31,85,21,30,27,25,80');
$pod_01_max=task_some_id_max('84,35,89,81,31,85,21,30,27,25,80,11,51,52,56,55,53,54,64');
$pod_02_max=task_some_id_max('33,83,82,66,67,87');
$pod_03_max=task_some_id_max('32,68,69,28,70,71,57,58,62,61,59,60,63');
$pod_04_max=task_some_id_max('38,41,43,42,39,40,79');
$pod_05_max=$otd_05_max;
$pod_06_max=$otd_06_max;
$pod_07_max=$otd_07_max;
$pod_08_max=$otd_08_max;

$pod_0_min=task_some_id_min('84,35,89,81,31,85,21,30,27,25,80,84,35,89,81,31,85,21,30,27,25,80,11,51,52,56,55,53,54,64.33,83,82,66,67,87,32,68,69,28,70,71,38,41,43,42,39,40,79');
$pod_0_max=task_some_id_max('84,35,89,81,31,85,21,30,27,25,80,84,35,89,81,31,85,21,30,27,25,80,11,51,52,56,55,53,54,64.33,83,82,66,67,87,32,68,69,28,70,71,38,41,43,42,39,40,79');

//echo  $pod_0_max;

//echo $pod_01_min;



$otd_010101=task_some_id('84,35');
$otd_010102=task_some_id('89,81');
$otd_010103=task_some_id('31,85');
$otd_010104=task_some_id('21,30,27,25');
$otd_0102=task_some_id('51,52,56,55,53,54');
$otd_0103=task_some_id('64');
$otd_0201=task_some_id('83,82,66,67,87');
$otd_0301=task_some_id('57,58,62,61,59,60,63');
$otd_0302=task_some_id('68,69,28,70,71');
$otd_0401=task_some_id('39,40,79');
$otd_0402=task_some_id('41,43,42');
$otd_05=task_some_id('47,65,48,50,49');
$otd_06=task_some_id('86,72,37,74,36,73');
$otd_07=task_some_id('20,15,7,6,16');
$otd_08=task_some_id('44,46,78,45');





$AllCountTask_proverka=$otd_010101+$otd_010102+$otd_010103+$otd_010104+$otd_0102+$otd_0103+$otd_0201+$otd_0301+$otd_0302+$otd_0401+$otd_0402+$otd_05+$otd_06+$otd_07+$otd_08; //сложили все отделы
$AllCountTask_proverka=$AllCountTask_proverka+task_some_id('11')+task_some_id('80')+task_some_id('33')+task_some_id('32')+task_some_id('38');

$pod_0101=$otd_010101+ $otd_010102 +$otd_010103 +$otd_010104+task_some_id('80');
$pod_01=$pod_0101+task_some_id('11')+$otd_0102+$otd_0103;
$pod_02=task_some_id('33')+$otd_0201;
$pod_03=task_some_id('32')+$otd_0301+$otd_0302;
$pod_04=task_some_id('38')+$otd_0401+$otd_0402;
$pod_05=$otd_05;
$pod_06=$otd_06;
$pod_07=$otd_07;
$pod_08=$otd_08;
$AllCountTask_proverka_pod=$pod_01+$pod_02+$pod_03+$pod_04+$pod_05+$pod_06+$pod_07+$pod_08;


$otd_010101_vkl=round($otd_010101*100/ $AllCountTask_proverka,1);
$otd_010102_vkl=round($otd_010102*100/ $AllCountTask_proverka,1);
$otd_010103_vkl=round($otd_010103*100/ $AllCountTask_proverka,1);
$otd_010104_vkl=round($otd_010104*100/ $AllCountTask_proverka,1);
$otd_0102_vkl=round($otd_0102*100/ $AllCountTask_proverka,1);
$otd_0103_vkl=round($otd_0103*100/ $AllCountTask_proverka,1);
$otd_0201_vkl=round($otd_0201*100/ $AllCountTask_proverka,1);
$otd_0301_vkl=round($otd_0301*100/ $AllCountTask_proverka,1);
$otd_0302_vkl=round($otd_0302*100/ $AllCountTask_proverka,1);
$otd_0401_vkl=round($otd_0401*100/ $AllCountTask_proverka,1);
$otd_0402_vkl=round($otd_0402*100/ $AllCountTask_proverka,1);
$pod_0101_vkl=round($pod_0101*100/ $AllCountTask_proverka,1);
$pod_01_vkl=round($pod_01*100/ $AllCountTask_proverka,1);
$pod_02_vkl=round($pod_02*100/ $AllCountTask_proverka,1);
$pod_03_vkl=round($pod_03*100/ $AllCountTask_proverka,1);
$pod_04_vkl=round($pod_04*100/ $AllCountTask_proverka,1);
$pod_05_vkl=round($pod_05*100/ $AllCountTask_proverka,1);
$pod_06_vkl=round($pod_06*100/ $AllCountTask_proverka,1);
$pod_07_vkl=round($pod_07*100/ $AllCountTask_proverka,1);
$pod_08_vkl=round($pod_08*100/ $AllCountTask_proverka,1);

$kolvo0=65;
$kolvo1=19;
$kolvo2=11;
$kolvo3=2;
$kolvo4=2;
$kolvo5=2;
$kolvo6=4;
$kolvo7=6;
$kolvo8=1;
$kolvo9=6;
$kolvo10=5;
$kolvo11=14;
$kolvo12=7;
$kolvo13=5;
$kolvo14=7;
$kolvo15=3;
$kolvo16=3;
$kolvo17=5;
$kolvo18=6;
$kolvo19=5;
$kolvo20=4;














//$restask=mysql_query("select count(*) as kolvo, sum(amount) as paid from tb_taskstats where status='1' and user='$user'");
//$restask1=mysql_fetch_array($restask);
//$tasksok2=$restask1["kolvo"];

        ?>
            <h2>Степень загруженности сотрудников и подразделений: </h2> <a href="#" class="print">печатать</a>
        <hr>
            <h4 style="margin-right: 10px; margin-top: 10px;">Выбрать дату:</h4>
            <form style="padding-top:6px;" id="zp">
                <select id="type" name="type" style="float: left; margin-right: 5px; width: 100px;">
                    <option value="1" <?if($_GET['type']==1||$_GET['type']=='') echo 'selected="selected"';?>>Январь</option>
                    <option value="2" >Февраль</option>
                    <option value="3" >Март</option>
                    <option value="4" >Апрель</option>
                    <option value="5" >Май</option>
                    <option value="6" >Июнь</option>
                    <option value="7" >Июль</option>
                    <option value="8" >Август</option>
                    <option value="9" >Сентябрь</option>
                    <option value="10" <?if($_GET['type']==10) echo 'selected="selected"';?>>Октябрь</option>
                    <option value="11" <?if($_GET['type']==11) echo 'selected="selected"';?>>Ноябрь</option>
                    <option value="12" <?if($_GET['type']==12) echo 'selected="selected"';?>>Декабрь</option>
                </select>
                 <select id="yers" name="yers" style="float: left; margin-right: 5px; width: 100px;">
                    <option value="2012" <?if($_GET['yers']==2012) echo 'selected="selected"';?>>2012</option>
                    <option value="2013" <?if($_GET['yers']==2013||$_GET['type']=='') echo 'selected="selected"';?>>2013</option>

                </select>
                <!--<a href="/reports/stat/tab/?type=all" style="float: right;" class="btn btn-warning">за все время</a>-->
            </form>


        </div>

        <table class="table table-striped  tablesorter" id="itogi" >
            <thead>
            <tr style="
    height: 122px;">
                <th>№ п.п.</th>
                <th style="
    width: 240px;
">Сотрудник</th>
                <th style="
    width: 160px;
">Должность</th>
                <th style="
    width: 160px;
">Подразделение</th>
                <th>Количество поручений</th>
                <th>Среднее количество поручений в подразделении на сотрудника</th>
                <th>Минимальное количество поручений у сотрудника в подразделении</th>
                <th>Максимальное количество поручений у сотрудника</th>
                <th>% Вклад подразделения в общий объем поручений агентства / сотрудника в подразделение</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                    <td></td>
                    <td colspan="2" style="text-align: center;"><b>ОКУ "АИР"</b></td>

                    <td></td>
                    <td><b><? echo $AllCountTask_proverka;?></b></td>
                    <td><b><?echo round($AllCountTask_proverka/$kolvo0,1);?></b></td>
                    <td><b><?echo $pod_0_min;?></b></td>
                    <td><b><?echo $pod_0_max;?></b></td>
                    <td><b>100%</b></td>
                </tr>


                 <tr style="background: #D1D1D1;">
                    <td>01</td>
                    <td colspan="2" style="text-align: center;"><b>Заместитель директора – руководитель филиала</b></td>

                    <td></td>
                    <td><b><?echo $pod_01;?></b></td>
                    <td><b><?echo round($pod_01/$kolvo1,1);?></b></td>
                    <td><b><?echo $pod_01_min;?></b></td>
                    <td><b><?echo $pod_01_max;?></b></td>
                    <td><b><?echo $pod_01_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>01.00.00.01</td>
                        <td>


                                    <img src="/media/cache/3e/21/3e21c77cdb1eb267b78314ef81b6b23c.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=11">Нафтс Олег Владимирович</a></b
                        </td>
                        <td>заместитель директора – руководитель филиала</td>
                        <td>--</td>
                        <td><?echo task_user_id(11);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

<tr style="background: #E5E5E5;">
                    <td>01.01</td>
                    <td colspan="2" style="text-align: center;"><b>Управление по организации предоставления Государственных и Муниципальных услуг</b></td>

                    <td></td>
                    <td><b><?echo $pod_0101;?></b></td>
                    <td><b><?echo round($pod_0101/$kolvo2,1);?></b></td>
                    <td><b><?echo $pod_0101_min;?></b></td>
                    <td><b><?echo $pod_0101_max;?></b></td>
                    <td><b><?echo $pod_0101_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>01.01.00.01</td>
                        <td>


                                    <img src="/media/cache/07/88/07881a10cd44dff0f596d1fcde859ac8.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=80">Образцов Александр Михайлович</a></b
                        </td>
                        <td>Начальник управления</td>
                        <td>Управление по организации предоставления Государственных и Муниципальных услуг</td>
                        <td><?echo task_user_id(80);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(80)*100/$pod_0101,1);?>%</td>
                    </tr>


                 <tr style="background: #F2F2F2;">
                    <td>01.01.01</td>
                    <td colspan="2" style="text-align: center;"><b>Отдел информационного обеспечения продвижения универсальных электронных карт</b></td>
<td></td>
                    <td><b><? echo $otd_010101;?></b></td>
                    <td><b><?echo round($otd_010101/$kolvo3,1);?></b></td>
                    <td><b><?echo $otd_010101_min;?></b></td>
                    <td><b><?echo $otd_010101_max;?></b></td>
                    <td><b><?echo $otd_010101_vkl;?>%</b></td>
                </tr>


<tr>
                        <td>01.01.01.01</td>
                        <td>


                                    <img src="/media/cache/c8/14/c814038cc0b9b25fb23e733a4b3a1f3b.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=84">Колесникова Юлия Ивановна</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Отдел информационного обеспечения продвижения универсальных электронных карт</td>
                        <td><?echo task_user_id(84);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(84)*100/$otd_010101,1);?>%</td>
                    </tr>
                    <tr>
                        <td>01.01.01.02</td>
                        <td>


                                    <img src="/media/cache/5c/1d/5c1d747399f53abddc4ae43cf0f5545d.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=35">Колесникова Виктория Владимировна</a></b
                        </td>
                        <td>Заместитель начальника отдела</td>
                        <td>Отдел информационного обеспечения продвижения универсальных электронных карт</td>
                        <td><?echo task_user_id(35);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(35)*100/$otd_010101,1);?>%</td>
                    </tr>

                <tr style="background-color: #F2F2F2;">
                    <td style="background-color: #F2F2F2;">01.01.02</td>
                    <td style="background-color: #F2F2F2;" colspan="2" style="text-align: center;"><b>Отдел технического и программного обеспечения работ с универсальными картами</b></td>

                    <td style="background-color: #F2F2F2;"></td>
                    <td style="background-color: #F2F2F2;"><b><?echo $otd_010102;?></b></td>
                    <td style="background-color: #F2F2F2;"><b><?echo round($otd_010102/$kolvo4,1);?></b></td>
                    <td style="background-color: #F2F2F2;"><b><?echo $otd_010102_min;?></b></td>
                    <td style="background-color: #F2F2F2;"><b><?echo $otd_010102_max;?></b></td>
                    <td style="background-color: #F2F2F2;"><b><?echo $otd_010102_vkl?>%</b></td>
                </tr>

                    <tr>
                        <td>01.01.02.01</td>
                        <td>


                                    <img src="/media/cache/1f/85/1f85840ac7c6fa803c1e8463f8ffaf9e.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=89">Базарнов Александр Валерьевич</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Отдел технического и программного обеспечения работ с универсальными картами</td>
                        <td><?echo task_user_id(89);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(89)*100/$otd_010102,1);?>%</td>
                    </tr>

                    <tr  class="pagebreak">
                        <td>01.01.02.02</td>
                        <td>


                                    <img src="/media/cache/44/9d/449d892dc0cb978967df0dc5f3226d69.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=81">Белов Алексей Александрович</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Отдел технического и программного обеспечения работ с универсальными картами</td>
                        <td><?echo task_user_id(81);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(81)*100/$otd_010102,1);?>%</td>
                    </tr>
                 <tr style="background: #F2F2F2;"  class="print-braek">
                    <td>01.01.03</td>
                    <td colspan="2" style="text-align: center;"><b>Отдел информационно-технологического взаимодействия государственных и муниципальных информационных систем</b></td>

                    <td></td>
                    <td><b><?echo $otd_010103;?></b></td>
                    <td><b><?echo round($otd_010103/$kolvo5,1);?></b></td>
                    <td><b><?echo $otd_010103_min;?></b></td>
                    <td><b><?echo $otd_010103_max;?></b></td>
                    <td><b><?echo $otd_010103_vkl?>%</b></td>
                </tr>


<tr>
                        <td>01.01.03.01</td>
                        <td>


                                    <img src="/media/cache/f5/1a/f51a5f33b043a9ed8fc73c807ac44000.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=31">Подрезов  Алексей  Николаевич</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Отдел информационно-технологического взаимодействия государственных и муниципальных информационных систем</td>
                        <td><?echo task_user_id(31);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(31)*100/$otd_010103,1);?>%</td>
                    </tr>

                    <tr>
                        <td>01.01.03.02</td>
                        <td>


                                    <img src="/media/cache/bc/9a/bc9a45c900ca55d72373ad4f1ca0ec32.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=85">Котляров Михаил Юрьевич</a></b
                        </td>
                        <td>Заместитель начальника отдела</td>
                        <td>Отдел информационно-технологического взаимодействия государственных и муниципальных информационных систем</td>
                        <td><?echo task_user_id(85);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(85)*100/$otd_010103,1);?>%</td>
                    </tr>

                <tr style="background: #F2F2F2;">
                    <td style="background: #F2F2F2;">01.01.04</td>
                    <td  style="background: #F2F2F2;"colspan="2" style="text-align: center;"><b>Отдел методического, нормативного и организационного обеспечения, хранения и ведения реестра универсальных электронных карт</b></td>

                    <td style="background: #F2F2F2;"></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_010104;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo round($otd_010104/$kolvo6,1);?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_010104_min;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_010104_max;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_010104_vkl?>%</b></td>
                </tr>

                <tr>
                        <td>01.01.04.01</td>
                        <td>


                                    <img src="/media/cache/97/b1/97b14d3d66de0d842e668270a9adb92d.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=21">Самойленко Елена Николаевна</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Отдел методического, нормативного и организационного обеспечения, хранения и ведения реестра универсальных электронных карт</td>
                        <td><?echo task_user_id(21);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(21)*100/$otd_010104,1);?>%</td>
                    </tr>
                     <tr>
                        <td>01.01.04.02</td>
                        <td>


                                    <img src="/media/cache/39/88/3988080c9750714fea17c79847ae33da.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=30">Полякова Ирина  Раисовна</a></b
                        </td>
                        <td>Заместитель начальника отдела</td>
                        <td>Отдел методического, нормативного и организационного обеспечения, хранения и ведения реестра универсальных электронных карт</td>
                        <td><?echo task_user_id(30);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(30)*100/$otd_010104,1);?>%</td>
                    </tr>

                    <tr   class="pagebreak">
                        <td>01.01.04.03</td>
                        <td>


                                    <img src="/media/cache/6e/d3/6ed377faad7e12f1a9f6566c49ea65b5.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=27">Сумская Ирина Васильевна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Отдел методического, нормативного и организационного обеспечения, хранения и ведения реестра универсальных электронных карт</td>
                        <td><?echo task_user_id(27);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(27)*100/$otd_010104,1);?>%</td>
                    </tr>
                    <tr>
                        <td>01.01.04.04</td>
                        <td>


                                    <img src="/media/cache/85/b6/85b6e22c2f2dea33b20660e8c20e7a91.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=25">Литвинчук Александр Сергеевич</a></b
                        </td>
                        <td>Специалист</td>
                        <td>Отдел методического, нормативного и организационного обеспечения, хранения и ведения реестра универсальных электронных карт</td>
                        <td><?echo task_user_id(25);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(25)*100/$otd_010104,1);?>%</td>
                    </tr>


               <tr style="background: #F2F2F2;">
                    <td>01.02</td>
                    <td colspan="2" style="text-align: center;"><b>Отдел бухгалтерского учета</b></td>

                    <td></td>
                    <td><b><?echo $otd_0102;?></b></td>
                    <td><b><?echo round($otd_0102/$kolvo7,1);?></b></td>
                    <td><b><?echo $otd_0102_min;?></b></td>
                    <td><b><?echo $otd_0102_max;?></b></td>
                    <td><b><?echo $otd_0102_vkl;?>%</b></td>
                </tr>

                <tr>
                        <td>01.02.00.01</td>
                        <td>


                                    <img src="/media/cache/8d/a8/8da8d2c8c80f3af256320f951bfe5a3a.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=51">Чеснакова Тамара Михайловна</a></b
                        </td>
                        <td>Главный бухгалтер - начальник отдела</td>
                        <td>Отдел бухгалтерского учета</td>
                        <td><?echo task_user_id(51);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(51)*100/$otd_0102,1);?>%</td>
                    </tr>
                    <tr>
                        <td>01.02.00.02</td>
                        <td>


                                    <img src="/media/cache/bb/d3/bbd3b6403613467a574563521ef7accd.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=52">Мигачева Анна Михайловна</a></b
                        </td>
                        <td>Заместитель главного бухгалтера</td>
                        <td>Отдел бухгалтерского учета</td>
                        <td><?echo task_user_id(52);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(52)*100/$otd_0102,1);?>%</td>
                    </tr>

                    <tr>
                        <td>01.02.00.03</td>
                        <td>


                                    <img src="/media/cache/04/f2/04f282e17e2b6a9b61884c22ed0a1593.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=56">Богданова Мария Анатольевна</a></b
                        </td>
                        <td>Ведущий экономист</td>
                        <td>Отдел бухгалтерского учета</td>
                        <td><?echo task_user_id(56);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(56)*100/$otd_0102,1);?>%</td>
                    </tr>

                    <tr>
                        <td>01.02.00.04</td>
                        <td>


                                    <img src="/media/cache/df/35/df3534f2d66f287dbd824b16bb2c7b7d.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=55">Болгова Валентина Ивановна</a></b
                        </td>
                        <td>Ведущий бухгалтер</td>
                        <td>Отдел бухгалтерского учета</td>
                        <td><?echo task_user_id(55);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(55)*100/$otd_0102,1);?>%</td>
                    </tr>

                    <tr>
                        <td>01.02.00.05</td>
                        <td>


                                    <img src="/media/cache/49/b1/49b13e6c5d78fe8b77e4059d96f3f1dc.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=53">Воробьева Ольга Александровна</a></b
                        </td>
                        <td>Ведущий бухгалтер</td>
                        <td>Отдел бухгалтерского учета</td>
                        <td><?echo task_user_id(53);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(53)*100/$otd_0102,1);?>%</td>
                    </tr>





                    <tr>
                        <td>01.02.00.06</td>
                        <td>


                                    <img src="/media/cache/ef/72/ef72646b27c862c5ddf8acf1554aa217.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=54">Шумбасова Наталья Ивановна</a></b
                        </td>
                        <td>Ведущий бухгалтер</td>
                        <td>Отдел бухгалтерского учета</td>
                        <td><?echo task_user_id(54);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(54)*100/$otd_0102,1);?>%</td>
                    </tr>

                <tr style="background: #F2F2F2;">
                    <td style="background: #F2F2F2;">01.03</td>
                    <td style="background: #F2F2F2;" colspan="2" style="text-align: center;"><b>Отдел хозяйственного обеспечения</b></td>

                    <td style="background: #F2F2F2;"></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_0103;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo round($otd_0103/$kolvo8,1);?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_0103_min;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_0103_max;?></b></td>
                    <td style="background: #F2F2F2;"><b><? echo $otd_0103_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>01.03.00.01</td>
                        <td>


                                    <img src="/media/cache/e2/8e/e28e090355772474d44b65eb919507ca.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=64">Овсянникова Ольга Михайловна</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Отдел хозяйственного обеспечения</td>
                        <td><?echo task_user_id(64);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(64)*100/$otd_0103,1);?>%</td>
                    </tr>

               <tr style="background: #D1D1D1;">
                    <td style="background: #D1D1D1;">02</td>
                    <td style="background: #D1D1D1;"><b>Заместитель директора</b></td>
                    <td style="background: #D1D1D1;"></td>
                    <td style="background: #D1D1D1;"></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_02;?></b></td>
                   <td style="background: #D1D1D1;"><b><?echo round($pod_02/$kolvo9,1);?></b></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_02_min;?></b></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_02_max;?></b></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_02_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>02.00.00.01</td>
                        <td>


                                    <img src="/media/cache/bb/60/bb60f26ddc44773418841f23d99a9b1e.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=33">Кочетов Руслан Львович</a></b
                        </td>
                        <td>Заместитель директора</td>
                        <td>--</td>
                        <td><?echo task_user_id(33);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(33)*100/$pod_02,1);?>%</td>
                    </tr>




                <tr style="background: #E5E5E5;"  class="print-braek">
                    <td  style="background: #E5E5E5;">02.01</td>
                    <td  style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Управление по обеспечению деятельности бизнес-инкубаторов и технопарков</b></td>

                    <td  style="background: #E5E5E5;"></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0201;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo round($otd_0201/$kolvo10,1);?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0201_min;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0201_max;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0201_vkl;?>%</b></td>
                </tr>


<tr>
                        <td>02.01.00.01</td>
                        <td>


                                    <img src="/media/cache/e8/71/e87144cb4ca0985d8a5c6293e42721a5.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=83">Жигайлов Сергей Николаевич</a></b
                        </td>
                        <td>Начальник управления</td>
                        <td>Управление по обеспечению деятельности бизнес-инкубаторов и технопарков</td>
                        <td><?echo task_user_id(83);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(83)*100/$otd_0201,1);?>%</td>
                    </tr>

                    <tr  class="pagebreak">
                        <td>02.01.00.02</td>
                        <td>


                                    <img src="/media/cache/0e/01/0e0106cac0d463e009cf0af681ee816c.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=82">Свиридов Андрей Анатольевич</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по обеспечению деятельности бизнес-инкубаторов и технопарков</td>
                        <td><?echo task_user_id(82);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(82)*100/$otd_0201,1);?>%</td>
                    </tr>

                    <tr>
                        <td>02.01.00.03</td>
                        <td>


                                    <img src="/media/cache/f9/cc/f9cc97956556267a1f6e27abe39af034.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=66">Чеботарев Вячеслав Иванович</a></b
                        </td>
                        <td>Ведущий экономист</td>
                        <td>Управление по обеспечению деятельности бизнес-инкубаторов и технопарков</td>
                        <td><?echo task_user_id(66);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(66)*100/$otd_0201,1);?>%</td>
                    </tr>

                    <tr>
                        <td>02.01.00.04</td>
                        <td>


                                    <img src="/media/cache/df/7f/df7f03844c3672ccc85c3dd2ab97af87.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=67">Романова Светлана Васильевна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Управление по обеспечению деятельности бизнес-инкубаторов и технопарков</td>
                        <td><?echo task_user_id(67);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(67)*100/$otd_0201,1);?>%</td>
                    </tr>

                    <tr>
                        <td>02.01.00.05</td>
                        <td>


                                    <img src="/media/cache/24/26/24266e5e522fdefc8b2cd08f4ae8376b.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=87">Букреев Сергей Анатольевич</a></b
                        </td>
                        <td>Специалист</td>
                        <td>Управление по обеспечению деятельности бизнес-инкубаторов и технопарков</td>
                        <td><?echo task_user_id(87);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(87)*100/$otd_0201,1);?>%</td>
                    </tr>


                <tr style="background: #D1D1D1;">
                    <td style="background: #D1D1D1;">03</td>
                    <td style="background: #D1D1D1;" colspan="2" style="text-align: center;"><b>Заместитель директора </b></td>

                    <td style="background: #D1D1D1;"></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_03;?></b></td>
                    <td style="background: #D1D1D1;"><b><?echo round($pod_03/$kolvo11,1);?></b></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_03_min;?></b></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_03_max;?></b></td>
                    <td style="background: #D1D1D1;"><b><?echo $pod_03_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>03.00.00.01</td>
                        <td>


                                    <img src="/media/cache/7a/a5/7aa5d790e1dd07e02d4e5099aff73b0b.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=32">Антиликаторов Алексей Александрович</a></b
                        </td>
                        <td>Заместитель директора</td>
                        <td>--</td>
                        <td><?echo task_user_id(32);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(32)*100/$pod_03,1);?>%</td>
                    </tr>




                <tr style="background: #E5E5E5;">
                    <td style="background: #E5E5E5;">03.01</td>
                    <td  style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Управление по работе с инновационными проектами и программами развития</b></td>

                    <td style="background: #E5E5E5;"></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0301;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo round($otd_0301/$kolvo12,1);?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0301_min;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0301_max;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_0301_vkl;?>%</b></td>
                </tr>


                <tr>
                        <td>03.01.00.01</td>
                        <td>


                                    <img src="/media/cache/c7/e3/c7e330013be6fb30fd870368acd21451.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=57">Харченко Валерий Павлович</a></b
                        </td>
                        <td>Начальник управления</td>
                        <td>Управление по работе с инновационными проектами и программами развития</td>
                        <td><?echo task_user_id(57);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(57)*100/$otd_0301,1);?>%</td>
                    </tr>

                <tr>
                        <td>03.01.00.02</td>
                        <td>


                                    <img src="/media/cache/8a/00/8a006f3e799b44ad300d933d0199f890.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=58">Требунских Сергей Юрьевич</a></b
                        </td>
                        <td>Заместитель начальника управления</td>
                        <td>Управление по работе с инновационными проектами и программами развития</td>
                        <td><?echo task_user_id(58);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(58)*100/$otd_0301,1);?>%</td>
                    </tr>

                    <tr>
                        <td>03.01.00.03</td>
                        <td>


                                    <img src="/media/cache/13/56/1356a2bb98e5722fca52a65ec361225c.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=62">Козлова Инна Ивановна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Управление по работе с инновационными проектами и программами развития</td>
                        <td><?echo task_user_id(62);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(62)*100/$otd_0301,1);?>%</td>
                    </tr>


                    <tr>
                        <td>03.01.00.04</td>
                        <td>


                                    <img src="/media/cache/a3/b9/a3b974712af82b4bf34e8a5b5a8ecbaf.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=61">Золотарев Евгений Юрьевич</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по работе с инновационными проектами и программами развития</td>
                        <td><?echo task_user_id(61);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(61)*100/$otd_0301,1);?>%</td>
                    </tr>


                    <tr  class="pagebreak">
                        <td>03.01.00.05</td>
                        <td>


                                    <img src="/media/cache/a0/3c/a03c4454951910751f92e91b3496bc0e.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=59">Соляник Вероника Игоревна</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по работе с инновационными проектами и программами развития</td>
                        <td><?echo task_user_id(59);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(59)*100/$otd_0301,1);?>%</td>
                    </tr>


                     <tr>
                        <td>03.01.00.06</td>
                        <td>

                                <img src="/static/project/img/default.png" width="40" height="40" style="float: left; vertical-align: top; margin-right: 5px;">

                            <b><a href="/history//?get_user=60">Филатова Екатерина Викторовна</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по работе с инновационными проектами и программами развития</td>
                        <td><?echo task_user_id(60);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(60)*100/$otd_0301,1);?>%</td>
                    </tr>


                    <tr>
                        <td>03.01.00.07</td>
                        <td>


                                    <img src="/media/cache/8c/c6/8cc6b213ff843b6c86a1fc7ea15f0414.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=63">Иванников Сергей Алексеевич</a></b
                        </td>
                        <td>специалист</td>
                        <td>Управление по работе с инновационными проектами и программами развития</td>
                        <td><?echo task_user_id(63);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(63)*100/$otd_0301,1);?>%</td>
                    </tr>

                <tr  style="background: #E5E5E5;">
                    <td  style="background: #E5E5E5;">03.02</td>
                    <td  style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Управление по развитию</b></td>

                    <td  style="background: #E5E5E5;"></td>
                    <td  style="background: #E5E5E5;"><b><?echo $otd_0302;?></b></td>
                    <td  style="background: #E5E5E5;"><b><?echo round($otd_0302/$kolvo13,1);?></b></td>
                    <td  style="background: #E5E5E5;"><b><?echo $otd_0302_min;?></b></td>
                    <td  style="background: #E5E5E5;"><b><?echo $otd_0302_max;?></b></td>
                    <td  style="background: #E5E5E5;"><b><?echo $otd_0302_vkl;?>%</b></td>
                </tr>


                <tr>
                        <td>03.02.00.01</td>
                        <td>


                                    <img src="/media/cache/ef/50/ef509b5f3b0bea8e5e6ab6c050a023be.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=68">Торко Ярослав Олегович</a></b
                        </td>
                        <td>Начальник управления</td>
                        <td>Управление по развитию</td>
                        <td><?echo task_user_id(68);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(68)*100/$otd_0302,1);?>%</td>
                    </tr>

                    <tr>
                        <td>03.02.00.02</td>
                        <td>


                                    <img src="/media/cache/0c/05/0c051b2cb2711564d3e8c18f2f7ac677.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=69">Шевченко Тарас Владимирович</a></b
                        </td>
                        <td>Заместитель начальника управления</td>
                        <td>Управление по развитию</td>
                        <td><?echo task_user_id(69);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(69)*100/$otd_0302,1);?>%</td>
                    </tr>


                    <tr>
                        <td>03.02.00.03</td>
                        <td>


                                    <img src="/media/cache/c3/8a/c38a7d930ea6ce0e6be84c9ccf411d50.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=28">Рябоконь Виктория Александровна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Управление по развитию</td>
                        <td><?echo task_user_id(28);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(28)*100/$otd_0302,1);?>%</td>
                    </tr>



                    <tr>
                        <td>03.02.00.04</td>
                        <td>


                                    <img src="/media/cache/18/6d/186d9b2c2671b1022fc3a5002baa69e1.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=70">Малаханов Евгений Сергеевич</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по развитию</td>
                        <td><?echo task_user_id(70);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(70)*100/$otd_0302,1);?>%</td>
                    </tr>

                    <tr>
                        <td>03.02.00.05</td>
                        <td>


                                    <img src="/media/cache/60/fb/60fbcf40b33a3d4c46b8cfe3314a528b.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=71">Перфильев Никита Витальевич</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по развитию</td>
                        <td><?echo task_user_id(71);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(71)*100/$otd_0302,1);?>%</td>
                    </tr>









            </tbody>

















            </tbody>














            </tbody>

                <tr  style="background: #E5E5E5;">
                    <td  style="background: #E5E5E5;">04</td>
                    <td  style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Управление правового обеспечения</b></td>

                    <td  style="background: #E5E5E5;"></td>
                    <td  style="background: #E5E5E5;"><b><?echo $pod_04;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo round($pod_04/$kolvo14,1);?></b></td>
                    <td  style="background: #E5E5E5;"><b><?echo $pod_04_min;?></b></td>
                    <td  style="background: #E5E5E5;"><b><?echo $pod_04_max;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_04_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>04.00.00.01</td>
                        <td>


                                    <img src="/media/cache/e8/d0/e8d0fab4f9e6d8748233da5806b89b03.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=38">Рощин Алексей Вячеславович</a></b
                        </td>
                        <td>Начальник управления</td>
                        <td>Управление правового обеспечения</td>
                        <td><?echo task_user_id(38);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(38)*100/$pod_04,1);?>%</td>
                    </tr>




                <tr class="print-braek">
                    <td>04.01</td>
                    <td  colspan="2" style="text-align: center;"><b>Отдел  договорной работы</b></td>

                    <td></td>
                    <td><b><?echo $otd_0401;?></b></td>
                    <td><b><?echo round($otd_0401/$kolvo15,1);?></b></td>
                    <td><b><?echo $otd_0401_min;?></b></td>
                    <td><b><?echo $otd_0401_max;?></b></td>
                    <td><b><?echo $otd_0401_vkl;?>%</b></td>
                </tr>

                <tr>
                        <td>04.01.00.01</td>
                        <td>


                                    <img src="/media/cache/31/f4/31f403d515e31c1f6cf9c86b7f10e71c.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=39">Бордюг Юлия Валериевна</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Отдел  договорной работы</td>
                        <td><?echo task_user_id(39);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(39)*100/$otd_0401,1);?>%</td>
                    </tr>

                    <tr>
                        <td>04.01.00.02</td>
                        <td>


                                    <img src="/media/cache/c9/70/c970f3de8fe844a6e068c5bcf785f7ca.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=40">Прохорович Рената Равильевна</a></b
                        </td>
                        <td>Заместитель начальника отдела</td>
                        <td>Отдел  договорной работы</td>
                        <td><?echo task_user_id(40);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(40)*100/$otd_0401,1);?>%</td>
                    </tr>

                    <tr>
                        <td>04.01.00.03</td>
                        <td>


                                    <img src="/media/cache/25/36/2536ea47ce423189499531b956d7e0c4.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=79">Антонова Мария Юрьевна</a></b
                        </td>
                        <td>ведущий юрисконсульт</td>
                        <td>Отдел  договорной работы</td>
                        <td><?echo task_user_id(79);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(79)*100/$otd_0401,1);?>%</td>
                    </tr>






                <tr style="background: #F2F2F2;"  class="pagebreak">
                    <td style="background: #F2F2F2;">04.02</td>
                    <td style="background: #F2F2F2;" colspan="2" style="text-align: center;"><b>Отдел нормативно-аналитического обеспечения и судебно-претензионной работы</b></td>

                    <td style="background: #F2F2F2;"></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_0402;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo round($otd_0402/$kolvo16,1);?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_0402_min;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_0402_max;?></b></td>
                    <td style="background: #F2F2F2;"><b><?echo $otd_0402_vkl;?>%</b></td>
                </tr>

                <tr>
                        <td>04.02.00.01</td>
                        <td>


                                    <img src="/media/cache/4a/f7/4af7963f4915187e87e8cf86bb76cde1.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=41">Шибкова Наталья Владимировна</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Отдел нормативно-аналитического обеспечения и судебно-претензионной работы</td>
                        <td><?echo task_user_id(41);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(41)*100/$otd_0402,1);?>%</td>
                    </tr>

<tr>
                        <td>04.02.00.02</td>
                        <td>


                                    <img src="/media/cache/8c/81/8c816d251fa03d4b3fe4a77ad453526c.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=43">Клемешов Алексей Николаевич</a></b
                        </td>
                        <td>ведущий юрисконсульт</td>
                        <td>Отдел нормативно-аналитического обеспечения и судебно-претензионной работы</td>
                        <td><?echo task_user_id(43);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(43)*100/$otd_0402,1);?>%</td>
                    </tr>

                    <tr>
                        <td>04.02.00.03</td>
                        <td>


                                    <img src="/media/cache/08/c4/08c4af7a0e46f0e653b879c8a39b59fb.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=42">Винникова Анна Ивановна</a></b
                        </td>
                        <td>Главный юрисконсульт</td>
                        <td>Отдел нормативно-аналитического обеспечения и судебно-претензионной работы</td>
                        <td><?echo task_user_id(42);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(42)*100/$otd_0402,1);?>%</td>
                    </tr>








            </tbody>

                <tr style="background: #E5E5E5;">
                    <td style="background: #E5E5E5;">05</td>
                    <td style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Управление по продвижению проектов</b></td>

                    <td style="background: #E5E5E5;"></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_05;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo round($pod_05/$kolvo17,1);?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_05_min;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_05_max;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_05_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>05.00.00.01</td>
                        <td>

                                <img src="/static/project/img/default.png" width="40" height="40" style="float: left; vertical-align: top; margin-right: 5px;">

                            <b><a href="/history//?get_user=47">Рущенко Ирина Васильевна</a></b
                        </td>
                        <td>Начальник управления</td>
                        <td>Управление по продвижению проектов</td>
                        <td><?echo task_user_id(47);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(47)*100/$pod_05,1);?>%</td>
                    </tr>

<tr>
                        <td>05.00.00.02</td>
                        <td>


                                    <img src="/media/cache/a0/3d/a03dea98bd4aa3be539a3cc75e15d3bc.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=65">Иванов Илья Александрович</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по продвижению проектов</td>
                        <td><?echo task_user_id(65);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(65)*100/$pod_05,1);?>%</td>
                    </tr>
                     <tr>
                        <td>05.00.00.03</td>
                        <td>


                                    <img src="/media/cache/df/4c/df4c902194dc7ac2414ec6d677270c2f.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=48">Лосева Людмила Анатольевна</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление по продвижению проектов</td>
                        <td><?echo task_user_id(48);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(48)*100/$pod_05,1);?>%</td>
                    </tr>

                     <tr>
                        <td>05.00.00.04</td>
                        <td>


                                    <img src="/media/cache/fd/9f/fd9f964196a020a38349e245d2fe51a1.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=50">Волкова Тамара Геннадьевна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Управление по продвижению проектов</td>
                        <td><?echo task_user_id(50);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(50)*100/$pod_05,1);?>%</td>
                    </tr>




                    <tr>
                        <td>05.00.00.05</td>
                        <td>


                                    <img src="/media/cache/ff/47/ff475dce5dad06f665de47d6e8ab08d1.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=49">Тюрина Нина Геннадьевна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Управление по продвижению проектов</td>
                        <td><?echo task_user_id(49);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(49)*100/$pod_05,1);?>%</td>
                    </tr>


                <tr style="background: #E5E5E5;">
                    <td style="background: #E5E5E5;">06</td>
                    <td style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Секретариат   </b></td>

                    <td style="background: #E5E5E5;"></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_06;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo round($pod_06/$kolvo18,1);?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_06_min;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_06_max;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_06_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>06.00.00.01</td>
                        <td>


                                    <img src="/media/cache/4c/ff/4cffa57980856328383cbefb4fcd7b2b.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=86">Иванов Дмитрий Алексеевич</a></b
                        </td>
                        <td>Начальник секретариата</td>
                        <td>Секретариат</td>
                        <td><?echo task_user_id(86);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(86)*100/$pod_06,1);?>%</td>
                    </tr>

     <tr>
                        <td>06.00.00.02</td>
                        <td>


                                    <img src="/media/cache/d9/a1/d9a172014fe49b91466014b333dbecd0.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=72">Барляк Оксана Викторовна</a></b
                        </td>
                        <td>Ведущий инспектор по контролю за исполнением поручений</td>
                        <td>Секретариат</td>
                        <td><?echo task_user_id(72);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(72)*100/$pod_06,1);?>%</td>
                    </tr>

                    <tr  class="pagebreak">
                        <td>06.00.00.03</td>
                        <td>


                                    <img src="/media/cache/0b/97/0b97813093a5a764d632040d84abc222.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=37">Гаврилова Светлана Александровна</a></b
                        </td>
                        <td>Ведущий инспектор по контролю за исполнением поручений</td>
                        <td>Секретариат</td>
                        <td><?echo task_user_id(37);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(37)*100/$pod_06,1);?>%</td>
                    </tr>



                    <tr>
                        <td>06.00.00.04</td>
                        <td>


                                    <img src="/media/cache/2a/d0/2ad063f8ecee166690a506fb8931346e.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=74">Пенина Мария Олеговна</a></b
                        </td>
                        <td>Ведущий инспектор по контролю за исполнением поручений</td>
                        <td>Секретариат</td>
                        <td><?echo task_user_id(74);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(74)*100/$pod_06,1);?>%</td>
                    </tr>

                    <tr>
                        <td>06.00.00.05</td>
                        <td>


                                    <img src="/media/cache/c2/ec/c2ecc76b0446aa1134213d2971e6dc0a.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=36">Столповская Светлана Михайловна</a></b
                        </td>
                        <td>Ведущий инспектор по контролю за исполнением поручений</td>
                        <td>Секретариат</td>
                        <td><?echo task_user_id(36);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(36)*100/$pod_06,1);?>%</td>
                    </tr>

                    <tr>
                        <td>06.00.00.06</td>
                        <td>


                                    <img src="/media/cache/5e/fe/5efe838ef62666236c43fc5e96f9051d.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=73">Шестакова Екатерина Сергеевна</a></b
                        </td>
                        <td>Ведущий инспектор по контролю за исполнением поручений</td>
                        <td>Секретариат</td>
                        <td><?echo task_user_id(73);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(73)*100/$pod_06,1);?>%</td>
                    </tr>




            </tbody>


























            </tbody>

                <tr style="background: #E5E5E5;">
                    <td style="background: #E5E5E5;">07</td>
                    <td style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Отдел информационно-аналитического обеспечения</b></td>

                    <td style="background: #E5E5E5;"></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_07;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo round($otd_07/$kolvo19,1);?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_07_min;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_07_max;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_07_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>07.00.00.01</td>
                        <td>


                                    <img src="/media/cache/7e/74/7e747dd76f93c0accd71d8ae451c031c.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=20">Тригуб Борис Владимирович</a></b
                        </td>
                        <td>Начальник отдела</td>
                        <td>Управление информационно-аналитической работы</td>
                        <td><?echo task_user_id(20);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(20)*100/$otd_07,1);?>%</td>
                    </tr>

                    <tr>
                        <td>07.00.00.02</td>
                        <td>


                                    <img src="/media/cache/46/a9/46a9e69c867a2a2459020542e598544f.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=15">Сушков Андрей Витальевич</a></b
                        </td>
                        <td>Заместитель начальника отдела</td>
                        <td>Управление информационно-аналитической работы</td>
                        <td><?echo task_user_id(15);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(15)*100/$otd_07,1);?>%</td>
                    </tr>

                    <tr>
                        <td>07.00.00.03</td>
                        <td>


                                    <img src="/media/cache/6f/92/6f92c55a9a100444be31f37eaec5482b.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=7">Алятина Юлия Юрьевна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Управление информационно-аналитической работы</td>
                        <td><?echo task_user_id(7);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(7)*100/$otd_07,1);?>%</td>
                    </tr>

                    <tr>
                        <td>07.00.00.04</td>
                        <td>


                                    <img src="/media/cache/ec/dc/ecdcae121411b2605f5bb8a1d9b9bd71.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=6">Волощик Денис Валерьевич</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Управление информационно-аналитической работы</td>
                        <td><?echo task_user_id(6);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(6)*100/$otd_07,1);?>%</td>
                    </tr>

                    <tr>
                        <td>07.00.00.06</td>
                        <td>


                                    <img src="/media/cache/23/fb/23fb9b22fda68f74f298fade751857d1.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=16">Рудаков Александр Александрович</a></b
                        </td>
                        <td>Специалист</td>
                        <td>Управление информационно-аналитической работы</td>
                        <td><?echo task_user_id(16);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(16)*100/$otd_07,1);?>%</td>
                    </tr>





            </tbody>






            </tbody>

                <tr style="background: #E5E5E5;">
                    <td style="background: #E5E5E5;">08</td>
                    <td style="background: #E5E5E5;" colspan="2" style="text-align: center;"><b>Отдел кадров  </b></td>

                    <td style="background: #E5E5E5;"></td>
                    <td style="background: #E5E5E5;"><b><?echo $otd_08;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo round($otd_08/$kolvo19,1);?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_08_min;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_08_max;?></b></td>
                    <td style="background: #E5E5E5;"><b><?echo $pod_08_vkl;?>%</b></td>
                </tr>

                    <tr>
                        <td>08.00.00.02</td>
                        <td>


                                    <img src="/media/cache/f0/e7/f0e74b51273a59d295dabd7bc6029c50.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=44">Еремина Наталья Викторовна</a></b
                        </td>
                        <td>Заместитель начальника отдела</td>
                        <td>Отдел кадров</td>
                        <td><?echo task_user_id(44);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(44)*100/$otd_08,1);?>%</td>
                    </tr>





                    <tr>
                        <td>08.00.00.03</td>
                        <td>


                                    <img src="/media/cache/82/fd/82fd65aa739c5b82ffaa10dfaa381c1d.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=46">Бондарева Наталия Александровна</a></b
                        </td>
                        <td>Ведущий специалист</td>
                        <td>Отдел кадров</td>
                        <td><?echo task_user_id(46);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(46)*100/$otd_08,1);?>%</td>
                    </tr>

                    <tr>
                        <td>08.00.00.04</td>
                        <td>


                                    <img src="/media/cache/04/42/04421bd13763755dce334441a4c2e18e.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=78">Худолей Елена Ивановна</a></b
                        </td>
                        <td>Ведущий специалист 0,5 ставки</td>
                        <td>Отдел кадров</td>
                        <td><?echo task_user_id(78);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(78)*100/$otd_08,1);?>%</td>
                    </tr>

                    <tr>
                        <td>08.00.00.05</td>
                        <td>


                                    <img src="/media/cache/4c/f0/4cf0e478405677548b95dc7d8412894b.jpg" style="float: left; vertical-align: top; margin-right: 5px;">


                            <b><a href="/history//?get_user=45">Просвирнина Наталья Викторовна</a></b
                        </td>
                        <td>Главный специалист</td>
                        <td>Отдел кадров</td>
                        <td><?echo task_user_id(45);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?echo round(task_user_id(45)*100/$otd_08,1);?>%</td>
                    </tr>






            </tbody>

        </table>

    </div>

</body>
</html>
