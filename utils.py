def is_sep(): #
    return __file__.startswith("/var/www/sysproject/data/django/sysproject")

def is_test():
    return not is_sep()
